<?php
$config = array(
				  'attributes' => array(
                                    	array(
                                            'field' => '1',
                                            'label' => 'Age',
                                            'rules' => 'integer|trim|xss_clean|required'
                                         ),
										array(
                                            'field' => '2',
                                            'label' => 'Life stage',
                                            'rules' => 'integer|trim|xss_clean|required'
                                         ),
										 array(
                                            'field' => '3',
                                            'label' => 'Home stage',
                                            'rules' => 'integer|trim|xss_clean|required'
                                         ),
										 array(
                                            'field' => '4',
                                            'label' => 'Monthly Income',
                                            'rules' => 'integer|trim|xss_clean|required'
                                         ),
										 array(
                                            'field' => '5',
                                            'label' => 'Loans',
                                            'rules' => 'integer|trim|xss_clean|required'
                                         )),
			     'ask_query' => array(
                                    	array(
                                            'field' => 'q',
                                            'label' => 'Query',
                                            'rules' => 'trim|required|xss_clean'
                                         ),
										array(
                                            'field' => 'f',
                                            'label' => 'Uploaded Files',
                                            'rules' => 'callback_validate_uploaded_files'
                                         )),				
				 'insert_user_group' => array(
                                    	array(
                                            'field' => 'group_name',
                                            'label' => 'Group Name',
                                            'rules' => 'trim|required|xss_clean'
                                         ),
										array(
                                            'field' => 'group_admin',
                                            'label' => 'Admin Status',
                                            'rules' => 'integer|xss_clean'
                                         )),
                 'insert_privilege' =>  array(
                                    	array(
                                            'field' => 'upriv_name',
                                            'label' => 'Privilege Name',
                                            'rules' => 'trim|required|xss_clean'
                                         )),
				   'admin_register_user' =>  array(
                                    	array(
                                            'field' => 'uacc_username',
                                            'label' => 'Name',
                                            'rules' => 'trim|required|xss_clean'
                                         ),
										 array(
                                            'field' => 'uacc_email',
                                            'label' => 'Email',
                                            'rules' => 'trim|required|xss_clean|alpha_numeric'
                                         ),
										 array(
                                            'field' => 'uacc_group_fk',
                                            'label' => 'New Password Again',
                                            'rules' => 'trim|required'
                                         )),                   
                 'rc_user_save' => array(
                                    array(
                                            'field' => 'uacc_username',
                                            'label' => 'Name',
                                            'rules' => 'trim|required|xss_clean'
                                         )),
				'rc_user_photo_upload' => array(
                                    array(
                                            'field' => 'profile_photo',
                                            'label' => 'Photo',
                                            'rules' => 'trim|required|xss_clean'
                                         )),                                         
                 'user_change_password' => array(
                                    array(
                                            'field' => 'current_password',
                                            'label' => 'Current Password',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'new_password',
                                            'label' => 'New Password',
                                            'rules' => 'required|callback_validate_password|matches[confirm_new_password]'
                                         ),
                                    array(
                                            'field' => 'confirm_new_password',
                                            'label' => 'Confirm Password',
                                            'rules' => 'required'
                                         )),
               				'user_login' =>array(
                                        array(
                                        	'field' => 'login_identity',
                                            'label' => 'Email',
                                            'rules' => 'required|valid_email'
                                         ),
                                         array(
                                    	     'field' => 'login_password',
                                            'label' => 'Password',
                                            'rules' => 'required'
                                         )),
                               'forgot_password' =>array(
                                          array(
                                        	'field' => 'forgot_password_identity',
                                            'label' => 'Email',
                                            'rules' => 'required|valid_email'
                                         )),
                                'user_account_create' =>array(
                                          array(
                                        	'field' => 'uacc_username',
                                            'label' => 'Name',
                                            'rules' => 'trim|required|xss_clean'
                                         ),
                                         array(
                                        	'field' => 'uacc_email',
                                            'label' => 'Email',
                                            'rules' => 'trim|required|xss_clean|valid_email'
                                         ),
                                          array(
                                        	'field' => 'uacc_password',
                                            'label' => 'Password',
                                            'rules' => 'trim|required|xss_clean|matches[uacc_password_match]|callback_validate_password'
                                         ),
                                          array(
                                        	'field' => 'uacc_password_match',
                                            'label' => 'confirm Password',
                                            'rules' => 'trim|required|xss_clean'
                                         ),
                                         array(
                                        	'field' => 'profile_dob',
                                            'label' => 'Date Of Birth',
                                            'rules' => 'trim|required|xss_clean'
                                         ),
                                         array(
                                        	'field' => 'profile_location',
                                            'label' => 'Location',
                                            'rules' => 'trim|required|xss_clean'
                                         ),
                                         array(
                                        	'field' => 'profile_gender',
                                            'label' => 'Gender',
                                            'rules' => 'trim|required'
                                         )
                                         ),                 
                                         );