<?php

class EmailSend
{
	public $email_settings;
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->config('email_settings', TRUE);
		$this->email_settings = $this->CI->config->item('email'); 
	}
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// UTILITY METHODS
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
		
	/**
	 * send_email
	 *
	 * @return bool
	 */
	public function send_email($email_to = NULL, $email_title = NULL, $template = NULL,$data = NULL)
	{
		
		if (empty($email_to) || empty($data) || empty($template))
		{
			return FALSE;
		}
		
		$template = "email/" . $template; 
		
		$message = $this->CI->load->view($template, $data, TRUE);
		$this->CI->load->library('email');
		$this->CI->email->clear();
		$this->CI->email->initialize(array('mailtype' => $this->email_settings['email_type']));
		$this->CI->email->set_newline("\r\n");
		$this->CI->email->from($this->email_settings['reply_email'], $this->email_settings['site_title']);
		$this->CI->email->to($email_to);
		$this->CI->email->subject($this->email_settings['site_title'] ." ". $email_title);
		$this->CI->email->message($message);
		echo $message;
		return $message;
		//return $this->CI->email->send();
	}
	
	public function send_account_activation_email($email_to,$user_data)
	{
		// Set email data.
		return ($this->send_email($email_to, ' - Account Activation', 'activate_account.tpl.php', $user_data));
	}
	
	
}