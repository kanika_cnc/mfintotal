<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 * Enter description here ...
 * @author fintotal
 *
 */
class Querylib
{
	public $message = array();
	public $attachement_path = "./uploads/query/";
	
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->model('qm_model');
	}
	/*********************************************************************************************
	 * Message Handling
	**********************************************************************************************/
	public function get_messages()
	{
		
		$this->message["erros"] =$this->CI->qm_model->error_messages();
		$this->message["status"] =$this->CI->qm_model->status_messages();
		return $this->message;
	}
	public function get_errors()
	{
		return $this->CI->qm_model->error_messages();
	}
	public function get_status()
	{
		return $this->CI->qm_model->status_messages();
	}
	
	/*********************************************************************************************
	 * Get Methods
	**********************************************************************************************/
	/**
	 * 
	 * Returns the list of messages for the given user
	 * @param int $user_id 
	 */
	public function get_user_messages($member_id)
	{
		$messages =  $this->CI->qm_model->get_user_messages($member_id);
		return $this->get_message_object($messages);
	}
	/**
	 * 
	 * Funtion to return the message object for the given message
	 * @param int $message_id
	 */
	public function get_message_by_id($message_id)
	{
		//get message
		$message =  $this->CI->qm_model->get_message_by_id($message_id);
		$messages = $this->get_message_object($message);
		if(is_array($messages))
			return $messages[0];
		return false;
	}
	/**
	 * 
	 * Return the list of unanswered queries for the given user
	 * if user is admin it will return all unanswered queries
	 * if user is sm then queries of his users and assigned to him
	 * if smm return all queries pending for his approval
	 * @param unknown_type $user_id
	 */
	public function get_unanswered_messages($user_id,$user_type)
	{
		$messages =  $this->CI->qm_model->get_unanswered_messages($user_id,$user_type);
		return $this->get_message_object($messages);	
		
	}
		
	public function get_answered_messages($user_id,$user_type)
	{
		$messages =  $this->CI->qm_model->get_answered_messages($user_id,$user_type);
		return $this->get_message_object($messages);
	}
	public function get_unapproved_messages($user_id,$user_type)
	{
		$messages =  $this->CI->qm_model->get_unapproved_messages($user_id,$user_type);
		return $this->get_message_object($messages);
	}
	public function get_approved_messages($user_id,$user_type)
	{
		$messages =  $this->CI->qm_model->get_approved_messages($user_id,$user_type);
		return $this->get_message_object($messages);
	}
	
	private function get_message_object($messages)
	{
		if(!empty($messages))
		{
			if(!is_array($messages))
			{
				$temp_msg = array();
				$temp_msg[] = $messages;
				$messages = $temp_msg;
			}
			$message_id = array();
			foreach ($messages as $message)
			{
				$message_id[] = $message->qm_id;
			}
			$messages_attributes =  $this->CI->qm_model->get_message_attributes($message_id);
			$messages_related_articles =  $this->CI->qm_model->get_message_articles($message_id);
			foreach ($messages as $message)
			{
				$message->qm_attached_files = array();
				if( $message->qm_has_attachment)
				{
					$message->qm_attached_files = array();
					//get the attached files
					$path =  $this->attachement_path.$message->qm_id;
					if(file_exists($path))
					{
						$attachments =  get_filenames($path);
						foreach ($attachments as $files)
						{
							$message->qm_attached_files[] = $files;
						}
					}
				}
				$message->qm_attributes = array();
				//get if any attributes are there
				if(!empty($messages_attributes))
				{
					foreach ($messages_attributes as $attr)
					{
						$attributes = new QAttributes();
						$attr = $attributes->to_object($attr);
						if($attr->qa_qm_id ==  $message->qm_id)
							$message->qm_attributes[] =  $attr;
					}
				}
				//get if any article suggections are there
				$message->qm_related_articles = array();
				if(!empty($messages_related_articles))
				{
					foreach ($messages_related_articles as $article)
					{
						$article = objectToObject($article, 'QRelatedArticle')  ;
						if($article->qa_qm_id ==  $message->qm_id)
							$message->qm_related_articles[] =  $article;
					}	
				}
				$message_temp =  $message;
			}
			return $messages;
		}
		return false;
	}
	/*********************************************************************************************
	 * Save Methods
	**********************************************************************************************/
	/**
	 * 
	 * Enter description here ...
	 * @param $message_array with two fields : description, has_attachement
	 * @param $logged_in_user_id : logged in user _id
	 * @param $sm_id : logged in member sm_id
	 * @param $attaibutes_array with two fileds : attribute_id , attribute_value
	 */
	public function create_message_by_member($message_array,$logged_in_user_id,$sm_id)
	{
		$message = new QMessage_Enity();
		$message->qm_description = $message_array["description"];
		$message->qm_created_by_id = $logged_in_user_id;
		$message->qm_modified_by_id = $logged_in_user_id;
		$message->qm_has_attachment =$message_array["has_attachment"];
		$message->qm_sm_uacct_id =  $sm_id;
		$message->qm_status_id = QStatus::OPEN;
		$message->qm_uacct_id = $logged_in_user_id;
		if(!$qm_id = $this->CI->qm_model->insert_message($message))
			$this->CI->qm_model->set_status_message('qm_saved_failed');  
		
		
		return $qm_id;	
	}
	/**
	*
	 * Save the attributes answered by the user 
	 * return the auto created message id
	 * @param $user_id
	 * @param $attaibutes_array array of key value pair of attribute id and its value
	 */
	public  function save_user_message_answered_attributes($user_id,$attaibutes_array)
	{
		//Save attributes
		if(!empty($attaibutes_array))
		{
			
			$this->CI->load->model('attribute_model');
			if(!$this->CI->attribute_model->save_attribute_collection($user_id,$attaibutes_array))
			{
				$this->CI->attribute_model->set_error_message('qm_saved_attributes_failed');  
				return false;
			}
			$this->CI->attribute_model->set_status_message('qm_saved_attributes_sucessful');
			return true;	
		}			
			
	}
	
	/**
	 * 
	 * Enter description here ...
	 * @param int $member_id
	 * @param int $logged_in_user_id
	 * @param string $description
	 * @param bool $has_attachments
	 * @param array $attributes_array = array of integer id array(1,2,3,4..)
	 * @param array $articles_array =  array("$title"=>"$url","$title"=>"$url","$title"=>"$url")
	 * @param unknown_type $archival_array = array("title"=>$title,"category_id"=>$qa_category_id)
	 */
	public function create_message_by_admin($member_id,$logged_in_user_id,$description,$has_attachments=0,$attributes_array='',$articles_array='',$archival_array='',$status_id='')
	{
		$message = new QMessage_Enity();
		$message->qm_description = $description;
		$message->qm_created_by_id = $logged_in_user_id;
		$message->qm_modified_by_id = $logged_in_user_id;
		$message->qm_has_attachment =$has_attachments;
		$message->qm_sm_uacct_id =  $logged_in_user_id;
		$message->qm_status_id = $status_id == ''? QStatus::ANSWERED:$status_id;
		$message->qm_uacct_id = $member_id;
		if(!empty($archival_array))
			$message->qm_is_archived = true;
		if(!$qm_id = $this->CI->qm_model->insert_message($message))
		{
			$this->CI->qm_model->set_status_message('qm_saved_failed');
			return false;
		}  
		
		if(!empty($attributes_array))
		{
			$qattributes = array();
			foreach ($attributes_array as $attr_id)
			{
				$qattr_temp = new stdClass();
				$qattr_temp->qa_qm_id =  $qm_id;
				$qattr_temp->qa_attr_id =  $attr_id;
				$qattributes[] = (array)$qattr_temp;
			}
			if(!$this->CI->qm_model->insert_message_attributes($qattributes))
				$this->CI->qm_model->set_error_message('qm_saved_attributes_failed');
		}	
		if(!empty($articles_array))
		{
			if(!$this->CI->qm_model->insert_message_articles($articles_array,$qm_id))
				$this->CI->qm_model->set_error_message("qm_saved_article_suggestions_failed");
		}
		if(!empty($archival_array))
		{
			if(!$this->CI->qm_model->archive_message($archival_array["title"],$archival_array["category_id"],$qm_id,$logged_in_user_id))
				$this->CI->qm_model->set_error_message("qm_archive_message_failed");
		}
		return $qm_id;	
	}
	
	/**
	 * 
	 * Enter description here ...
	 * @param unknown_type $title
	 * @param unknown_type $category_id
	 * @param unknown_type $message_id
	 * @param unknown_type $logged_user_id
	 * @todo : if already archived don;t do any action
	 */
	public function archive_message($title,$category_id,$message_id,$logged_user_id)
	{
		if(!$this->CI->qm_model->archive_message($title,$category_id,$message_id,$logged_user_id))
		{
				$this->CI->qm_model->set_error_message("qm_archive_message_failed");
				return false;
		}
		return true;
	}
	
	public function transfer_message($message_id,$new_sm_id,$logged_user_id)
	{
		$entity =  new stdClass();
		$entity->qm_is_transferred = 1;
		$entity->qm_id =  $message_id;
		$entity->qm_sm_uacct_id =  $new_sm_id;
		$entity->qm_modified_by_id =  $logged_user_id;
		return $this->update_message($entity);
	}
	
	public function approve_message($message_id,$smm_id)
	{
		$entity =  new stdClass();
		$entity->qm_approved_by_id = $smm_id;
		$entity->qm_id =  $message_id;
		$entity->qm_modified_by_id =  $smm_id;
		$this->update_message($entity);
	}
	
	public function ignore_message($message_id,$logged_user_id)
	{
		$entity =  new stdClass();
		$entity->qm_approved_by_id = $logged_user_id;
		$entity->qm_id =  $message_id;
		$entity->qm_status_id =  QStatus::IGNORED;
		$entity->qm_modified_by_id =  $logged_user_id;
		return $this->update_message($entity);
	}
	public function get_archived_messages_title_list($category_id='')
	{
		return $this->CI->qm_model->get_archived_messages_title_list($category_id);
	}

	public function get_archived_messages($category_id='',$title='',$message_id='')
	{
		$messages =  $this->CI->qm_model->get_archived_messages($category_id,$title,$message_id);
		$archival_list = array();
		foreach ($messages as $message)
		{
			$mess = new QArchival();
			$mess =  $message;
			if(!empty($mess))
			 {
			 	$mess->qa_message =  $this->get_message_by_id($message->qa_qm_id);
			 	$archival_list[] =  $mess;
			 }
		}
		return $archival_list;
	}
	
	public function edit_message($description,$message_id,$logged_user_id)
	{
		$entity =  stdClass();
		$entity->qm_description = $description;
		$entity->qm_id =  $message_id;
		$entity->qm_modified_by_id =  $logged_user_id;
		return $this->update_message($entity);
	}
	
	/**
	 * 
	 * Enter description here ...
	 * @param stdClass $entity : should contains only those properties which is to be updated
	 * with modified by
	 */
	private function update_message($entity)
	{
		if(!$this->CI->qm_model->update_message($entity))
		{
			$this->CI->qm_model->set_error_message("qm_message_update_failed");
			return false;
		}
		return true;	
	}
}