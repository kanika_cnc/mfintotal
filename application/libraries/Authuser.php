<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AuthUser 
{
	public $message ='';
	public $session_name = 'logged_in';
	//@todo : enable it later
	private $validate_login_onload = false; 	
	
	
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->model('auth_model');
	}

	public function errors()
	{
		return $this->CI->auth_model->error_messages();
	}
	public function messages()
	{
		return  $this->CI->auth_model->status_messages();
	}
	/**
	 * insert_user
	 * Inserts user account and profile data, returning the users new id.
	 *
	 * @return void
	 */
	public function insert_user($email, $username = FALSE, $password,$manager_id, $user_data, $group_id = FALSE, $activate = FALSE) 
	{
		$user_id = $this->CI->auth_model->insert_user($email, $username, $password,$manager_id, $user_data, $group_id);
		
		if ($user_id)
		{
			// Check whether to auto activate the user.
			if ($activate)
			{
				// If an account activation time limit is set by the config file, retain activation token.
				$clear_token = TRUE;
				$this->CI->auth_model->activate_user($user_id, FALSE, FALSE, $clear_token);
			}
			$user = $this->CI->auth_model->get_user_by_user_id($user_id); 
			
			if (!is_object($user))
			{
				$this->CI->auth_model->set_error_message('account_creation_unsuccessful', 'config');
				return FALSE;
			}
			$identity = $user->uacc_email;
			$activation_token = $user->uacc_activation_token;
			
			// Prepare account activation email.
			// If the $activation_token is not empty, the account must be activated via email before the user can login.
			if (!empty($activation_token))
			{
				// Set email data.
				$this->CI->load->library('emailsend');
				$user_data = array(
					'user_id' => $user_id,
					'identity' => $identity,
					'activation_token' => $activation_token
				);
				if ($this->CI->emailsend->send_email($email, ' - Account Activation', 'activate_account.tpl.php', $user_data))
				{
					$this->CI->auth_model->set_status_message('activation_email_successful', 'config');
					return $user_id;
				}
				else 
				{
					$this->CI->auth_model->set_error_message('activation_email_unsuccessful', 'config');
					return FALSE;
				}
			}

			$this->CI->auth_model->set_status_message('account_creation_successful', 'config');
			
			return $user_id;
		}
		else
		{
			$this->CI->auth_model->set_error_message('account_creation_unsuccessful', 'config');
			return FALSE;
		}
	}
	
	/**
	 * update_user_account
	 * Updates the account and profile data of a specific user.
	 * Note: The user profile table ('demo_user_profiles') is used in this demo as an example of relating additional user data to the auth libraries account tables. 
	 */
	function update_user_account($user_id)
	{
		if ($this->form_validation->run())
		{
			// If we were only updating profile data (i.e. no email, username or group included), we could use the 'update_custom_user_data()' function instead.
			$this->flexi_auth->update_user($user_id, $profile_data);
				
			// Save any public or admin status or error messages to CI's flash session data.
			$this->session->set_flashdata('message', $this->flexi_auth->get_messages());
			
			// Redirect user.
			redirect('auth_admin/manage_user_accounts');			
		}
		
		return FALSE;
	}

	public function get_messages()
	{
		if(!$this->CI->auth_model->error_messages())
			return $this->CI->auth_model->status_messages();
		return $this->CI->auth_model->error_messages();
	}
	
	/**
	 * ip_login_attempts_exceeded
	 * Validates whether the number of failed login attempts from a unique IP address has exceeded a defined limit.
	 * The function can be used in conjunction with showing a Captcha for users repeatedly failing login attempts.
	 *
	 * @return bool
	 * @author fintotal
	 */
	public function ip_login_attempts_exceeded($ip_address)
	{
		return $this->CI->auth_model->ip_login_attempts_exceeded($ip_address);
	}
	
	/**
	 * recaptcha
	 * Generates the html for Google reCAPTCHA.
	 * Note: If the reCAPTCHA is located on an SSL secured page (https), set $ssl = TRUE.
	 *
	 * @return string
	 * @author Rob Hussey
	 */
	public function recaptcha($ssl = FALSE)
	{
		return $this->CI->flexi_auth_model->recaptcha($ssl);
	}
		/**
     * Generate a random password. 
     * 
     * get_random_password() will return a random password with length 6-8 of lowercase letters only.
     *
     * @access    public
     * @param    $chars_min the minimum length of password (optional, default 6)
     * @param    $chars_max the maximum length of password (optional, default 8)
     * @param    $use_upper_case boolean use upper case for letters, means stronger password (optional, default false)
     * @param    $include_numbers boolean include numbers, means stronger password (optional, default false)
     * @param    $include_special_chars include special characters, means stronger password (optional, default false)
     *
     * @return    string containing a random password 
     */    
    function get_random_password($chars_min=6, $chars_max=8, $use_upper_case=false, $include_numbers=false, $include_special_chars=false)
    {
        $length = rand($chars_min, $chars_max);
        $selection = 'aeuoyibcdfghjklmnpqrstvwxz';
        if($include_numbers) {
            $selection .= "1234567890";
        }
        if($include_special_chars) {
            $selection .= "!@\"#$%&[]{}?|";
        }
                                
        $password = "";
        for($i=0; $i<$length; $i++) {
            $current_letter = $use_upper_case ? (rand(0,1) ? strtoupper($selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))];            
            $password .=  $current_letter;
        }                
	 return $password;
    }
	
    public function change_password($user_id,$old_password,$new_password,$autogenerate=false)
    {
    	if(!is_numeric($user_id))
		{
			$this->message = "Invalid User ID"; 
			return false; 
		}
		$user = $this->CI->auth_model->get_user_by_user_id($user_id);
		if($autogenerate)
		{
			$new_password =  $this->get_random_password();
			if(is_object($user))
				$old_password = $user->uacc_password;
		}
		
		if(is_object($user))
		{
			if(!$this->CI->auth_model->change_password($user->uacc_email, $old_password, $new_password))
			{
				$this->message = "password not verified";
				return false;
			}
			
			$this->CI->load->library('emailsend');
			$email_data = array('identity' => $user->uacc_email,'new_password'=>$new_password);
			if($this->CI->emailsend->send_email($user->uacc_email, 'Welcome', 'new_password.tpl.php', $email_data))
			{
				$this->message = "password generated. An email has been sent to user";
				return true;
			}
			else
			{
				$this->message= "password generated. But there is problem sending email.Please try after sometime";
				return false;
			}	
		}
		else
		{
			$this->message =  "Invalid User";
			return false;
		}
    }

	/**
	 * login
	 * Validate the submitted login details and attempt to log the user into their account.
	 */
	function login($identity, $password, $remember_user)
	{
		$user = $this->CI->auth_model->login($identity, $password, $remember_user);
		
		if (is_object($user))
		{
			//Set user login sessions.
			if ($this->set_login_sessions($user, TRUE))
			{
				// Set 'Remember me' cookie and database record if checked by user.
				if ($remember_user)
				{
					//@todo later
					//$this->remember_user($user->{$this->auth->database_config['user_acc']['columns']['id']});
				}
				// Else, ensure any existing 'Remember me' cookies are deleted.
				// This can occur if the user logs in via password, whilst already logged in via a "Remember me" cookie. 
				else
				{
					//@todo later
					//$this->flexi_auth_lite_model->delete_remember_me_cookies();
				}
				return TRUE;
			}
			$this->CI->auth_model->set_status_message('login_successful', 'config');
			return TRUE;
		}
		// If no specific error message has been set, set a generic error.
		if (!$this->CI->auth_model->error_messages())
		{
			$this->CI->auth_model->set_error_message('login_unsuccessful', 'config');
		}
		return FALSE;
	}
	
	/**
	 * set_login_sessions
	 * Set all login session and database data.
	 *
	 * @return bool
	 * @author Rob Hussey / Filou Tschiemer
	 */
	private function set_login_sessions($user, $logged_in_via_password = FALSE)
	{
		if (!$user)
		{
			return FALSE;
		}
		
		$user_id = $user->uacc_id;
		
		// Regenerate CI session_id on successful login.
		$this->regenerate_ci_session_id();
		
		// Set database and login session token if defined by config file.
		//todo : implement later
		//if ($this->auth->auth_security['validate_login_onload'] && ! $this->insert_database_login_session($user_id))
		if($this->validate_login_onload)
		{
			return FALSE;
		}
		$logged_in_user = new stdClass(); 		
		// Set verified login session if user logged in via Password rather than 'Remember me'.
		$logged_in_user->logged_in_via_password = $logged_in_via_password;
		$logged_in_user->user_id = $user_id;
		$logged_in_user->user_identifier = $user->uacc_email;
		$logged_in_user->logged_in_via_password = $logged_in_via_password;
		$logged_in_user->is_admin =  $user->ugrp_admin == 1;
		$logged_in_user->group = array("ugrp_id"=>$user->ugrp_id,"ugrp_name"=> $user->ugrp_name);
		$logged_in_user->uacc_username = $user->uacc_username;
		$logged_in_user->manager_id = $user->uacc_manager_acc_id;

		$privileges = array();
		$this->CI->load->model('group_privilage_model','obj_group_privilage_model');
		$privileges_temp =  $this->CI->obj_group_privilage_model->get_user_group_privileges($user->ugrp_id);
		foreach ($privileges_temp as $privilege)
		{
				$privileges[$privilege->upriv_id] = $privilege->upriv_name;  
		}
		// Set user privileges to session.
		$logged_in_user->privileges = $privileges; 
		###+++++++++++++++++++++++++++++++++###
		$this->CI->session->set_userdata('logged_in',$logged_in_user);
		return TRUE;
	}	
	
/**
	 * regenerate_ci_session_id
	 * Regenerate CodeIgniters session id like native PHP session_regenerate_id(TRUE), used whenever a users permissions change.
	 *
	 * @return bool
	 * @author Rob Hussey
	 */
	private function regenerate_ci_session_id()
	{	
		// This is targeting a native CodeIgniter cookie, not a flexi_auth cookie.
		$ci_session = array(
			'name'   => $this->CI->config->item('sess_cookie_name'),
			'value'  => '',
			'expire' => ''
		);
		set_cookie($ci_session);	
	}
	
	/**
	 * forgotten_password
	 * Sends the user an email containing a link the user must click to verify they have requested to change their forgotten password.
	 *
	 * @return bool
	 * @author Rob Hussey
	 */
	public function forgotten_password($identifier) 
	{
		// Get users primary identity.
		if (!$identity = $this->CI->auth_model->get_primary_identity($identifier))
		{
			$this->CI->auth_model->set_error_message('email_forgot_password_unsuccessful', 'config');
			return FALSE;
		}
	
		if ($this->CI->auth_model->forgotten_password($identity))
		{
			// Get user information.
			$sql_select = array(
				'uacc_id',
				'uacc_email',
				'uacc_forgotten_password_token'
			);			
			$sql_where['uacc_email'] = $identity;
			
			$user = $this->CI->auth_model->get_user_by_email($identity);
			$user_id = $user->uacc_id;
			$forgotten_password_token = $user->uacc_forgotten_password_token; 

			// Set email data.
			$this->CI->load->library('emailsend');
			$user_data = array(
				'user_id' => $user_id,
				'identity' => $identity,
				'forgotten_password_token' => $forgotten_password_token
			);
			$email_title = ' - Forgotten Password Verification';
			if($this->CI->emailsend->send_email($user->uacc_email, $email_title, 'forgot_password.tpl.php', $user_data))
			{
				$this->CI->auth_model->set_status_message('email_forgot_password_successful', 'config');
				return TRUE;
			}
		}
		
		$this->CI->auth_model->set_error_message('email_forgot_password_unsuccessful', 'config');
		return FALSE;
	}
	
	/**
	 * forgotten_password_complete
	 * This function is similar to the above 'validate_forgotten_password()' function, however, if validated the function also updates the database
	 * with a new password, then if defined via $send_email, an email will be sent to the user containing the new password.
	 *
	 * @return void
	 * @author fintotal
	 */
	public function forgotten_password_complete($user_id, $forgot_password_token, $new_password = FALSE, $send_email = FALSE)
	{
		if ($this->CI->auth_model->validate_forgotten_password_token($user_id, $forgot_password_token))
		{
			$user = $this->CI->auth_model->get_user_by_user_id($user_id,false);

			if (!is_object($user))
			{
				$this->CI->auth_model->set_error_message('password_change_unsuccessful', 'config');
				return FALSE;
			}

			$identity = $user->uacc_email;
			$database_salt = $user->uacc_salt;

			// If no new password is set via $new_password, the function will generate a new one.
			$new_password = $this->CI->auth_model->change_forgotten_password($user_id, $forgot_password_token, $new_password, $database_salt);
			
			// Send user email with new password if function variable $send_email = TRUE.
			if ($send_email)
			{
				// Set email data
				$this->CI->load->library('emailsend');
				$user_data = array(
					'identity' => $identity,
					'new_password' => $new_password
				);
				$email_title = ' - New Password';
				if($this->CI->emailsend->send_email($user->uacc_email, $email_title, 'new_password.tpl.php', $user_data))
				{
					$this->CI->auth_model->set_status_message('email_new_password_successful', 'config');
					return TRUE;
				}
			}
			// If new password is not set to be emailed, but has been successfully changed.
			else if ($new_password)
			{
				$this->CI->auth_model->set_status_message('password_change_successful', 'config');
				return TRUE;
			}
		}
		
		$this->CI->auth_model->set_error_message('password_token_invalid', 'config');
		return FALSE;
	}
	
	/**
	 * activate_user
	 * Activates a users account allowing them to login to their account. 
	 * If $verify_token = TRUE, a valid $activation_token must also be submitted.
	 *
	 * @return void
	 * @author Rob Hussey
	 */
	public function activate_user($user_id, $activation_token = FALSE, $verify_token = TRUE)
	{
		if ($this->CI->auth_model->activate_user($user_id, $activation_token, $verify_token))
		{
			$this->CI->auth_model->set_status_message('activate_successful', 'config');
			return TRUE;
		}
		$this->CI->auth_model->set_error_message('activate_unsuccessful', 'config');
		return FALSE;
	}
	
	/**
	 * deactivate_user
	 * Deactivates a users account, preventing them from logging in.
	 *
	 * @return void
	 * @author Mathew Davies
	 */
	public function deactivate_user($user_id)
	{
		if ($this->CI->auth_model->deactivate_user($user_id))
		{
			$this->CI->auth_model->set_status_message('deactivate_successful', 'config');
			return TRUE;
		}

		$this->CI->auth_model->set_error_message('deactivate_unsuccessful', 'config');
		return FALSE;
	}
	/**
	 * resend_activation_token
	 * Resends user a new activation token incase they have lost the previous one.
	 *
	 * @return bool
	 * @author Rob Hussey
	 */
	public function resend_activation_token($identity) 
	{
		// Get primary identity.
		$identity = $this->CI->auth_model->get_primary_identity($identity);
		
		if (empty($identity))
		{
			$this->CI->auth_model->set_error_message('activation_email_unsuccessful', 'config');
			return FALSE;
		}
		
		$user = $this->CI->auth_model->get_user_by_email($identity);

		$user_id = $user->uacc_id;
		$active_status = $user->uacc_active;		
		
		// If account is already activated.
		if ($active_status == 1)
		{
			$this->CI->auth_model->set_status_message('account_already_activated', 'config');
			return TRUE;
		}
		// Else, run the deactivate_user() function to reset the users activation token.
		else if ($this->CI->auth_model->deactivate_user($user_id))
		{
			$user = $user = $this->CI->auth_model->get_user_by_email($identity);
			
			$email = $user->uacc_email ;
			$activation_token = $user->uacc_activation_token;
			
			// Set email data.
			$this->CI->load->library('emailsend');
			$user_data = array(
					'user_id' => $user_id,
					'identity' => $email,
					'activation_token' => $activation_token
			);
			if ($this->CI->emailsend->send_account_activation_email($email,$user_data))
			{
				$this->CI->auth_model->set_status_message('activation_email_successful', 'config');
				return $user_id;
			}
			else 
			{
				$this->CI->auth_model->set_error_message('activation_email_unsuccessful', 'config');
				return FALSE;
			}
		}
		
		$this->CI->auth_model->set_error_message('activation_email_unsuccessful', 'config');
		return FALSE;
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// LOGOUT FUNCTION
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	
	/**
	 * logout
	 * Logs a user out of their account. 
	 * Note: The $all_sessions variable allows you to define whether to delete all database sessions or just the current session.
	 * When set to FALSE, this can be used to logout a user off of one computer (Internet Cafe) but not another (Home).
	 *
	 * @return bool
	 * @author fintotal
	 */
	public function logout($all_sessions = TRUE)
	{
		$user = $this->CI->session->userdata($this->session_name);
		if(!empty($user))
		{
			$user_id = $user->user_id;
				
			// Delete database login sessions and 'Remember me' cookies.
			//todo later
			//$this->delete_database_login_session($user_id, $all_sessions);

			// Delete session login data.
			//$this->auth->session_data = $this->set_auth_defaults();
			$this->CI->session->unset_userdata($this->session_name);

			// Run database maintenance function to clean up any expired login sessions.
			//todo later
			//$this->delete_expired_remember_users();
			$this->CI->auth_model->set_status_message('logout_successful', 'config');
		}
		return TRUE;
	}
	public function email_available($email = FALSE, $user_id = FALSE)
	{
		if($this->CI->auth_model->email_available($email,$user_id))
		 return TRUE;
		return FALSE; 
	}
	
	public function get_next_manager()
	{
		return  $this->CI->auth_model->get_next_manager();
	}
}