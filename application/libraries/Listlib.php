<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Listlib
{
	const query_status = 1;
	const message_type = 2 ;
	const mfin_modules = 3;
	const query_archival_category = 4;
	const life_stage = 5;
	const home_stage = 6;
	//const yes_no = 7;
	
	public $message = array();
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->model('list_model');
	}
	public function get_messages()
	{
		
		$this->message["erros"] =$this->CI->list_model->error_messages();
		$this->message["status"] =$this->CI->list_model->status_messages();
		return $this->message;
	}
	public function get_errors()
	{
		return $this->CI->list_model->error_messages();
	}
	public function get_status()
	{
		return $this->CI->list_model->status_messages();
	}
	
	/**
	 * 
	 * Returns the List Items for given list ID
	 * @param $list_id : const Listlib (Refer const of this class) 
	 * @param bool $default_row : true or flase
	 */
	public function get_list($list_id,$default_row=false)
	{
		 $list = $this->CI->list_model->get_list_items_by_list_type_id($list_id);
		 if($default_row)
			 $formatted_list = array(""=>"Please select");
		 else 
		      $formatted_list = array();
		 foreach ($list as $listItem)
		 {
		 	$formatted_list[$listItem->li_id] = $listItem->li_name;  
		 }
		 return $formatted_list;
	}
	public function get_list_types()
	{
		
	}
}
