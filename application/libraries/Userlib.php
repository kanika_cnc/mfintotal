<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Userlib 
{
	public $message ='';
	//@todo : enable it later
	private $validate_login_onload = false; 	
	
	
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->model('auth_model');
	}

	public function errors()
	{
		return $this->CI->auth_model->error_messages();
	}
	public function messages()
	{
		return  $this->CI->auth_model->status_messages();
	}
	
	public function get_users_list_array_by_group_id($group_id)
	{
		return $this->CI->auth_model->get_users_list_array_by_group_id($group_id);
	}
}
	
	