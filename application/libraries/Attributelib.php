<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 * Enter description here ...
 * @author fintotal
 *
 */
class Attributelib
{
	public $message = array();
	
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->model('attribute_model');
	}
	public function get_messages()
	{
		$this->message["erros"] =$this->CI->attribute_model->error_messages();
		$this->message["status"] =$this->CI->attribute_model->status_messages();
		return $this->message;
	}
	public function get_errors()
	{
		return $this->CI->attribute_model->error_messages();
	}
	public function get_status()
	{
		return $this->CI->attribute_model->status_messages();
	}
	
	public function get_attributes($module_id='')
	{
		return $this->CI->attribute_model->fetch($module_id); 
	}
	
	public function get_modules()
	{
		
	}
}
