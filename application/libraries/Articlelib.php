<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 * Enter description here ...
 * @author fintotal
 *
 */
class Articlelib
{
	public $message = array();
	
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->model('article_repos_model');
	}
	public function get_messages()
	{
		$this->message["erros"] =$this->CI->article_repos_model->error_messages();
		$this->message["status"] =$this->CI->article_repos_model->status_messages();
		return $this->message;
	}
	public function get_errors()
	{
		return $this->CI->article_repos_model->error_messages();
	}
	public function get_status()
	{
		return $this->CI->article_repos_model->status_messages();
	}
	
	/**
	 * 
	 * Return the object List arcticles from the repository 
	 * @param bool $active : If true returns only the active articles , else return all
	 */
	public function fetch($active=true)
	{
		return $this->CI->article_repos_model->fetch($active); 
	}

	/**
	 * 
	 * Return the list of articles in key=>value
	 * @param bool $active : If true returns only the active articles , else return all
	 * @param bool $default_row : If true returns "Please select row"  
	 */
	public function get_article_list($active=true,$default_row=false)
	{
		$list = $this->CI->article_repos_model->fetch($active);
		 if($default_row)
			 $formatted_list = array(""=>"Please select");
		 else 
		      $formatted_list = array();
		 foreach ($list as $listItem)
		 {
		 	$formatted_list[$listItem->mar_id] = $listItem->mar_title;  
		 }
		 return $formatted_list;
	}
}
