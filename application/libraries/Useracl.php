<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UserAcl
{
	const USERGROUP_ADMIN = 1;
	const USERGROUP_SMM = 2;
	const USERGROUP_SM = 3;
	const USERGROUP_RC = 4;
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->model('auth_model');
	}
	
	
	public function is_member()
	{
		return ($this->in_group('RC'));
	}
	public function is_sm()
	{
		return ($this->in_group(UserAcl::USERGROUP_SM));
	}
	public function is_smm()
	{
		return ($this->in_group(UserAcl::USERGROUP_SMM));
	}
	public function check_member_access()
	{
		if(!$this->is_member())
		{
		 	
		}
	}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// LOGIN STATUS FUNCTIONS
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	

	/**
	 * is_logged_in
	 * Verifies a user is logged in either via entering a valid password or using the 'Remember me' feature.
	 *
	 * @return bool
	 * @author  fintotal
	 */
	public function is_logged_in()
	{
		return (bool) $this->get_logged_in_user();
	}

	/**
	 * is_logged_in_via_password
	 * Verifies a user has logged in via entering a valid password rather than using the 'Remember me' feature (Login by password is more secure).
	 *
	 * @return bool
	 * @author fintotal
	 */
	public function is_logged_in_via_password()
	{
		return (bool) $this->get_logged_in_user()->logged_in_via_password;
	}

	/**
	 * is_admin
	 * Verifies a user belongs to a user group with admin permissions.
	 *
	 * @return bool
	 * @author  fintotal
	 */
	public function is_admin()
	{
		
		if($this->get_logged_in_user())
			return (bool) $this->get_logged_in_user()->is_admin;
	}

	/**
	 * in_group
	 * Verifies whether a user is assigned to a particular user group, by comparing by either the group id or name.
	 *
	 * @return bool
	 * @author  fintotal
	 */
	public function in_group($groups = FALSE)
	{
		// Get users group and convert group name to lowercase for comparison.
		$user_group = array();
		if (! empty($this->get_logged_in_user()->group))
		{
			$session_group = $this->get_logged_in_user()->group;
			$user_group[$session_group["ugrp_id"]] = strtolower($session_group["ugrp_name"]); 
		}
		// If multiple groups submitted as an array, loop through each.
		if (is_array($groups))
		{
			foreach($groups as $group)
			{
				if ((is_numeric($group) && $group == key($user_group)) || strtolower($group) == strtolower(current($user_group)))
				{
					return TRUE;
				}
			}
			return FALSE;
		}

		return ((is_numeric($groups) && $groups == key($user_group)) || strtolower($groups) == strtolower(current($user_group)));
	}

	/**
	 * is_privileged
	 * Verifies whether a user has a specific privilege, by comparing by either privilege id or name.
	 *
	 * @return bool
	 * @author fintotal
	 */
	public function is_privileged($privileges = FALSE)
	{
		if($this->is_admin() && $this->in_group(UserAcl::USERGROUP_ADMIN))
			return true;
		// Get users privileges and convert names to lowercase for comparison.
		$user_privileges = array();
		if (! empty($this->get_logged_in_user()->privileges))
		{
			foreach($this->get_logged_in_user()->privileges as $id => $name)
			{
				$user_privileges[$id] = strtolower($name);
			}
		}

		// If multiple groups submitted as an array, loop through each.
		if (is_array($privileges))
		{
			foreach($privileges as $privilege)
			{
				if ((is_numeric($privilege) && array_key_exists($privilege, $user_privileges)) || in_array(strtolower($privilege), $user_privileges))
				{
					return TRUE;
				}
			}
			return FALSE;
		}

		return ((is_numeric($privileges) && array_key_exists($privileges, $user_privileges)) || in_array(strtolower($privileges), $user_privileges));
	}
	
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// GET USER FUNCTIONS
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	
	/**
	 * get_user_id
	 * Get the users id from the session.
	 *
	 * @return void
	 * @author fintotal
	 */
	public function get_logged_in_user_id()
	{
		return ($this->get_logged_in_user() !== FALSE) ? 
			$this->get_logged_in_user()->user_id : FALSE;
	}
	
	/**
	 * get_user_identity
	 * Get the users primary identity from the session.
	 *
	 * @return void
	 * @author fintotal
	 */
	public function get_user_identity()
	{			
		return ($this->get_logged_in_user() !== FALSE) ? 
			$this->get_logged_in_user()->user_identifier : FALSE;
	}
	
	/**
	 * get_user_group_id
	 * Get the users group id from the session.
	 *
	 * @return void
	 * @author Rob Hussey
	 */
	public function get_user_group_id()
	{
		if($this->get_logged_in_user() !== FALSE)
		{
			if(!empty($this->get_logged_in_user()->group))
			{
				return $this->get_logged_in_user()->group['ugrp_id'];
			}
		}
		return FALSE;
	}
	
	/**
	 * get_user_group
	 * Get the users user group name from the session.
	 *
	 * @return void
	 * @author Rob Hussey
	 */
	public function get_user_group()
	{
		if($this->get_logged_in_user() !== FALSE)
		{
			if(!empty($this->get_logged_in_user()->group))
			{
				return $this->get_logged_in_user()->group['ugrp_name'];
			}
		}
		return FALSE;
	}
	
	public function get_logged_in_user()
	{
		$user = $this->CI->session->userdata('logged_in');
		return $user;
	}
	public function get_logged_in_user_manager_id()
	{
		$user = $this->CI->session->userdata('logged_in');
		if($user){
			return $user->manager_id;
		}
		return false;
	}
	
	
	public function check_page_access()
	{
		if(!$this->is_logged_in())
		{
			redirect('home', 'refresh');
		} 
		else
		{
			
			$access_directory =  strtolower($this->CI->router->fetch_directory());
			if($this->is_admin())
			{
				if(($access_directory != "admin/" && $access_directory != "core/"))
					redirect('/admin/dashboard/', 'refresh');
			}
			else
			{
				if(($access_directory != "members/" && $access_directory != "core/"))
					redirect('/members/dashboard/', 'refresh');
			}
		}
	}
	
	public function authorise_privilege($privileges = FALSE)
	{
		if($this->is_admin() && $this->in_group(UserAcl::USERGROUP_ADMIN))
			return true;
		if(!$this->is_privileged($privileges))
		{
			$this->CI->session->set_flashdata('message', '<p class="error_msg">You do not have privileges to ' . $privileges. '</p>');
			$this->process_redirection();
		}
		return TRUE;
	}
	public function process_redirection($return_url='')
	{
		if(!$this->is_logged_in())
			redirect('home', 'refresh');
			
		if($return_url == '')
		{
			
				if($this->is_admin())
				{
					redirect('/admin/dashboard/', 'refresh');
				}
				else
				{
					redirect('/members/dashboard/', 'refresh');
				}
			
		}
		else
			redirect($return_url);
	}
}


