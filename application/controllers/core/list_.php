<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class List_ extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		//$this->init_admin_content();
		
		$this->load->library('querylib');
		$this->load->library('userlib');
	}
	public function index()
	{
		
	}
	public function list_items($list_id,$default_row= true)
	{
		$this->load->library('listlib');
		$data = $this->listlib->get_list($list_id,true);
		echo json_encode($data);
	}
	public function get_attributes($module_id)
	{
		$this->load->library('attributelib');
		$data = $this->attributelib->get_attributes($module_id);
		$list_data = array();
		foreach ($data as $attr)
		{
			$list_data[$attr->mfa_id] =  $attr->mfa_name;
		}
		echo json_encode($list_data);
	}
	
	/**
	 * 
	 * Returns the list of archives titles 
	 * @param bool $unique = true returns the unique titles
	 */
	public function get_archived_titles($category_id,$unique=true)
	{
		$this->load->library('querylib');
		$data = $this->querylib->get_archived_messages_title_list($category_id);
		$list_data = array();
		foreach ($data as $attr)
		{
			$list_data[$attr->qa_qm_id] =  $attr->qa_title;
		}
		echo json_encode($list_data);
	}
}