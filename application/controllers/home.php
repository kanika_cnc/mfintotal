<?php
//Non logged in user
class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->masterpage->setMasterPage ('web/master');
		$this->masterpage->setPageTitle('');
		$this->masterpage->addContentPage ('web/home', 'content','');
        $this->masterpage->show();
	}
}

