<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Non logged in user
class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * 
	 * function to load the login view
	 */
	function index()
	{
		redirect(base_url()."	auth/acl/login");
	}
}

