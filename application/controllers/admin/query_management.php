<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 
class Query_Management extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		//$this->init_admin_content();
		
		$this->load->library('querylib');
		$this->load->library('userlib');
	}
	public function index()
	{
		$this->load_masters();
		$this->refresh_view();
		//$this->unanswered_queries();
	}
	public function refresh_view()
	{
		$name ='admin/query_manage/query_management_view';
		$title = 'Assistance';
		$this->masterpage->setPageTitle($title);
		$this->masterpage->addContentPage ($name, 'content',(array)$this->content);
        $this->masterpage->show();
	}
	
	public function unanswered_queries()
	{
		if($this->useracl->is_smm())
			$messages = $this->querylib->get_unapproved_messages($this->useracl->get_logged_in_user_id(),$this->useracl->get_user_group_id());
		else
			$messages = $this->querylib->get_unanswered_messages($this->useracl->get_logged_in_user_id(),$this->useracl->get_user_group_id());
		$this->content->form_url = base_url().'admin/query_management/submit';
		$this->content->messages =$messages;
		$this->load->view('admin/query_manage/queries_view',$this->content);
		//$this->refresh_view();
	}
	public function answered_queries()
	{
		if($this->useracl->is_smm())
			$messages = $this->querylib->get_approved_messages($this->useracl->get_logged_in_user_id(),$this->useracl->get_user_group_id());
		else
			$messages = $this->querylib->get_answered_messages($this->useracl->get_logged_in_user_id(),$this->useracl->get_user_group_id());
		$this->content->form_url = base_url().'admin/query_management/submit';
		$this->content->messages =$messages;
		$this->load->view('admin/query_manage/queries_view',$this->content);
		//$this->refresh_view();
	}
	public function submit()
	{
		if ($this->input->is_ajax_request())
		{
			debug($this->input->post());
			$action = $this->input->post("hdn_action");
			switch($action)
			{
				case 'Submit':
					$this->save_message();
					break;
				case 'Approve':
					$this->approve_message();	
					break;
				case 'Transfer' :
					$this->transfer_message();
					break;	
						
			}
			$this->answered_queries();				
		}
		
	}
	
	private function save_message()
	{
		$member_id =$this->input->post("hdn_member_id");
		$description =  $this->input->post("area");
		
		$sel_attributes_from_post = $this->input->post("hdn_selected_attr_id"); 
		$sel_arr_string = 	explode(',',$sel_attributes_from_post);
		$sel_attributes = array();
		foreach ($sel_arr_string as $str)
		{
			$tmp_str =   explode("|",$str);
			$sel_attributes[] = $tmp_str[0];
		}
		
		$sel_atricles_from_post = $this->input->post("hdn_selected_article_suggestion_id");
		$sel_atricles = array(); 
		if($sel_atricles_from_post != "")
		{
			$sel_arr_string = 	explode(',',$sel_atricles_from_post);
			debug($sel_arr_string);
			foreach ($sel_arr_string as $str)
			{
				$tmp_str =   explode("|",$str);
				$sel_atricles[$tmp_str[0]] = $tmp_str[1];
			}
		}
		$this->querylib->create_message_by_admin($member_id,$this->useracl->get_logged_in_user_id(),$description,0,$sel_attributes,$sel_atricles);
		$error = $this->querylib->get_errors();
		debug($error);
		if(!empty($error))
		{
			echo json_encode(array("fail"=>$this->querylib->get_errors()));
			
		}
		else
		{ 
			echo json_encode(array("success"=>"Message saved"));
		}
	}
	
	private function approve_message()
	{
		$message_id =$this->input->post("hdn_message_id");
		$this->querylib->approve_message($message_id,$this->useracl->get_logged_in_user_id());
		$error = $this->querylib->get_errors();
		if(!empty($error))
		{
			echo json_encode(array("fail"=>$this->querylib->get_errors()));
			
		}
		else
		{ 
			echo json_encode(array("success"=>"Message Approved"));
		}
	}
	
	private function transfer_message()
	{
		$message_id =$this->input->post("hdn_message_id");
		$sm_id =$this->input->post("smusers");
		debug($sm_id);
		$this->querylib->transfer_message($message_id,$sm_id,$this->useracl->get_logged_in_user_id());
		$error = $this->querylib->get_errors();
		if(!empty($error))
		{
			echo json_encode(array("fail"=>$this->querylib->get_errors()));
			
		}
		else
		{ 
			echo json_encode(array("success"=>"Message Transfered"));
		}	
	}
	private function load_masters()
	{
		$array_sm = $this->userlib->get_users_list_array_by_group_id(UserAcl::USERGROUP_SM);
		$this->content->master['sm_array'] = $array_sm;
		
		$this->load->library('listlib');
		$this->content->master['attr_modules'] = $this->listlib->get_list(Listlib::mfin_modules,true);
		
		$this->load->library('articlelib');
		$this->content->master['articles'] = $this->articlelib->get_article_list(true,true);
		
		$this->content->master['archive_cat'] = $this->listlib->get_list(Listlib::query_archival_category,true);
	}
	
	public function get_archived_messages($message_id)
	{
		$message = $this->querylib->get_archived_messages('','',$message_id);
		//if ($this->input->is_ajax_request())
		{
			echo json_encode($message);
		}
				
	}

}