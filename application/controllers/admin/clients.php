<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//logged in admin
class Clients extends MY_Controller
{
	private $module_url = 'admin/clients';
	private $manage_user_group_url = 'admin/user_management/manage_groups';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model','obj_user_model');
		$this->load->model('group_privilage_model','obj_group_privilage_model');
		$this->load->library('authuser');
		
	}
	public function index()
	{
		$this->useracl->authorise_privilege('Manage Admin Users');
		//Get all admin groups
		$this->content->groups = $this->obj_group_privilage_model->get_groups_list_array(true);
		//Get all users which belongs to SMM group
		$this->content->groups_users = $this->obj_user_model->get_users_list_array_by_group_id(UserAcl::USERGROUP_SMM);
		//Get the list of all users to manage
		$this->content->users =  $this->obj_user_model->get_users(0);
		$this->content->base_url = base_url().$this->module_url;
		$this->content->message = (!isset($this->content->message)) ? $this->session->flashdata('message') : $this->content->message;
		$this->refresh_view('users_view',$title='Users');
	}
	public function refresh_view($name='',$title='')
	{
		$name= 'admin/user_acc/'.$name;
		$this->masterpage->setPageTitle($title);
		$this->masterpage->addContentPage ($name, 'content',(array)$this->content);
        $this->masterpage->show();
	}
}
	
