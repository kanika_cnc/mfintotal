<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->init_member_content();
		$this->load->library('querylib');
		$this->load->library('listlib');
	}
	
	public function index()
	{
		echo "loged in user - >" .  $this->useracl->get_logged_in_user_id();
		echo "<br/> looggen user group ->" . $this->useracl->get_user_group_id();
		//$messages =  $this->querylib->get_user_messages($this->useracl->get_logged_in_user_id());
		//debug($messages);
		//$messages = $this->querylib->get_unanswered_messages($this->useracl->get_logged_in_user_id(),$this->useracl->get_user_group_id());
		//debug($messages);
		//$messages = $this->querylib->get_answered_messages($this->useracl->get_logged_in_user_id(),$this->useracl->get_user_group_id());
		//debug($messages);
		//$messages = $this->querylib->get_approved_messages($this->useracl->get_logged_in_user_id(),$this->useracl->get_user_group_id());
		//debug($messages);
		//$messages =  $this->querylib->get_message_by_id(10);
		//debug($messages);
		//$messages = $this->querylib->get_unapproved_messages($this->useracl->get_logged_in_user_id(),$this->useracl->get_user_group_id());
		//debug($messages);
		//$list_items = $this->listlib->get_list(Listlib::query_archival_category,true);
		//debug($list_items);
		//$messages = $this->querylib->get_archived_messages_title_list();
		//debug($messages);
		//$messages = $this->querylib->get_archived_messages_title_list(19);
		//debug($messages);
		
		/**********************
		 * SAVE METHODS
		 */
		//57 : memeber id for whose message we are replying
		//
		//$this->querylib->create_message_by_admin(memeber_id,$this->useracl->get_logged_in_user_id(),"description",$has_attachments,$attributes_array,$articles_array,$archival_array,$status_id='')
		///Save message without any atributes,articles,archivals
		//$this->querylib->create_message_by_admin(57,$this->useracl->get_logged_in_user_id(),"description",1);
		//$this->querylib->create_message_by_admin(57,$this->useracl->get_logged_in_user_id(),"description with attributes",1,array(1,2,3,4,5));
		//$this->querylib->create_message_by_admin(57,$this->useracl->get_logged_in_user_id(),"description with attributes and articles",1,array(1,2,3,4,5),array("text1 "=>"http://url1","text2"=>"http://url2"));
		//$this->querylib->create_message_by_admin(57,$this->useracl->get_logged_in_user_id(),"description with attributes and articles and archived and ignore the message",1,array(1,2,3,4,5),array("text1 "=>"http://url1","text2"=>"http://url2",),array("title"=>"titlke of archived message","category_id"=>19),QStatus::IGNORED);
		//$this->querylib->archive_message("titlke of archived message",19,48,$this->useracl->get_logged_in_user_id());
		//$this->querylib->transfer_message(48,6,$this->useracl->get_logged_in_user_id());
		//$this->querylib->ignore_message(48,$this->useracl->get_logged_in_user_id());
		//for smm
		$this->querylib->approve_message(48,$this->useracl->get_logged_in_user_id());
		//$this->content->messages =$messages;
		
		
		$this->refresh_view();
	}
	public function refresh_view()
	{
		$name ='admin/query_manage/query_management_view';
		$title = 'Assistance';
		$this->masterpage->setPageTitle($title);
		$this->masterpage->addContentPage ($name, 'content',(array)$this->content);
        $this->masterpage->show();
	}
	
}
