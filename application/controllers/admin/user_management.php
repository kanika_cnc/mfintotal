<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//logged in admin
class User_Management extends MY_Controller
{
	private $module_url = 'admin/user_management';
	private $manage_user_group_url = 'admin/user_management/manage_groups';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model','obj_user_model');
		$this->load->model('group_privilage_model','obj_group_privilage_model');
		$this->load->library('authuser');
		
	}
	public function index()
	{
		$this->useracl->authorise_privilege('Manage Admin Users');
		//Get all admin groups
		//@todo : Add one blank row
		$this->content->groups = $this->obj_group_privilage_model->get_groups_list_array(true);
		//Get all users which belongs to SMM group
		//@todo : make an enum for standard groups
		$this->content->groups_users = $this->obj_user_model->get_users_list_array_by_group_id(UserAcl::USERGROUP_SMM);
		//Get the list of all users to manage
		$this->content->users =  $this->obj_user_model->get_users(1);
		$this->content->base_url = base_url().$this->module_url;
		$this->content->message = (!isset($this->content->message)) ? $this->session->flashdata('message') : $this->content->message;
		$this->refresh_view('users_view',$title='Users');
	}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// User
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	public function edit_user($user_id)
	{
		if(!is_numeric($user_id) )
		{
			$this->session->set_flashdata('message', '<p class="error_msg">Invalid User ID.</p>');
			redirect($this->module_url);
		}
		if (!$this->useracl->is_privileged('Manage Admin Users'))
		{
			$this->session->set_flashdata('message', '<p class="error_msg">You do not have privileges to view admin users.</p>');
			redirect($this->module_url);
		}
		
		$this->load->model('group_privilage_model','obj_group_privilage_model');
		$this->content->groups = $this->obj_group_privilage_model->get_groups_list_array(true);
		$this->content->groups_users = $this->obj_user_model->get_users_list_array_by_group_id(2);
		// Get users current data.
		
		$this->load->library('form_validation');
		if ($this->form_validation->run('admin_register_user')) 
		{
			$user = new stdClass();
			$user = $this->input->post();
			$user["uacc_email"] = $user["uacc_email"] . "@fintotal.com";  
			if($this->obj_user_model->update_user($user_id, $user))
			{
				$this->content->status = 1;
				return true;
			}
			/*@todo : later
			 * else
			{
				debug($this->obj_user_model->get_messages('error'));
				$this->content->message = $this->obj_user_model->get_messages();
			}*/
		}
		else
		{
			$this->content->message =  validation_errors();
		}
		$this->content->user = $this->obj_user_model->get_user_by_user_id($user_id);
		 
		$this->content->base_url = base_url().$this->module_url;
		$this->content->message = (!isset($this->content->message)) ? $this->session->flashdata('message') : $this->content->message;
		$this->load->view('admin/user_acc/edit_user',$this->content);
	}
	public function delete_user_via_ajax($user_id)
	{
		if (!$this->useracl->is_privileged('Manage Admin Users'))
		{
			$this->session->set_flashdata('message', '<p class="error_msg">You do not have privileges to delete users.</p>');
			echo 'You do not have privileges to view admin users';
			return false;
		}
		$this->obj_user_model->delete_user($user_id);
	}
	public function generate_password($user_id)
	{
		if($this->input->is_ajax_request())
		{
		if(!is_numeric($user_id))
		{
			echo "Invalid User ID";
			return false;
		}
		$user = $this->obj_user_model->get_user_by_user_id($user_id);
		if(is_object($user))
		{
			$uacc_password_new =  $this->authuser->get_random_password();
			if(!$this->obj_user_model->change_password($user->uacc_email, $user->uacc_password, $uacc_password_new))
			{
				echo "password not verified";
				return false;
			}
			$this->load->library('emailsend');
			$email_data = array('identity' => $user->uacc_email,'new_password'=>$uacc_password_new);
			if($this->emailsend->send_email($user->uacc_email, 'Welcome', 'new_password.tpl.php', $email_data))
			{
				echo "password generated. An email has been sent to user";
				return true;
			}
			else
			{
				echo "password generated. But there is problem sending email.Please try after sometime";
				return false;
			}	
		}
		else
		{
			echo "Invalid User";
			return false;
		}
		}
	}
	/**
	 * register_account
	 * User registration page used by all new users wishing to create an account.
	 * Note: This page is only accessible to users who are not currently logged in, else they will be redirected.
	 */ 
	public function register_account()
	{
		// If 'Registration' form has been submitted, attempt to register their details as a new account.
		if ($this->input->post('register_user'))
		{			
			$this->load->library('form_validation');
			// Run the validation.
			if ($this->form_validation->run('admin_register_user'))
			{
				// Get user login details from input.
				$uacc_email = $this->input->post('uacc_email') . "@fintotal.com";
				$uacc_username = $this->input->post('uacc_username');
				$uacc_group_fk = $this->input->post('uacc_group_fk');
				$uacc_manager_acc_id = $this->input->post('uacc_manager_acc_id');
				$uacc_password =  $this->authuser->get_random_password();
				//Set whether to instantly activate account.
				$instant_activate = TRUE;
	
				$response = $this->authuser->insert_user($uacc_email, $uacc_username, "12345", $uacc_manager_acc_id,false ,$uacc_group_fk, $instant_activate);

				if ($response)
				{
					$this->load->library('emailsend');
					$email_data = array('identity' => $uacc_email,'password'=>$uacc_password);
					$this->emailsend->send_email($uacc_email, 'Welcome', 'registration_welcome.tpl.php', $email_data);
					echo json_encode(array("message" =>$this->authuser->messages()));
				}
				else
				{
					echo json_encode(array("error" => $this->authuser->errors()));
				}
			}
			else 
			{
				echo json_encode(array("error"=> validation_errors()));
				return FALSE;
			}
		}
		//redirect($this->module_url);		
	}
	public function view_activity_log()
	{
		
	}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// Groups
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	public function manage_groups()
	{
		// Check user has privileges to view user groups, else display a message to notify the user they do not have valid privileges.
		if (! $this->useracl->is_privileged('View User Groups'))
		{
			$this->session->set_flashdata('message', '<p class="error_msg">You do not have privileges to view user groups.</p>');
			redirect($this->module_url);		
		}
		$this->content->base_url =  base_url().$this->module_url;
		$this->load->model('group_privilage_model','obj_group_privilage_model');
		// If 'Manage User Group' form has been submitted and user has privileges, delete user groups.
		if ($this->input->post('delete_group') && $this->useracl->is_privileged('Delete User Groups')) 
		{
			$group_ids = (array_keys($this->input->post('delete_group')));
			$this->obj_group_privilage_model->delete_group($group_ids);
			redirect($this->manage_user_group_url);
		}
		//get groups
		$this->content->user_groups = $this->obj_group_privilage_model->get_groups();
				
		// Set any returned status/error messages.
		$this->content->message = (!isset($this->content->message)) ? $this->session->flashdata('message') : $this->content->message;		
		$this->refresh_view('user_groups_view',$title='User Groups');		
		
	}
	public function add_group()
	{
		$this->load->library('form_validation');
		
		// Check user has privileges to insert user groups, else display a message to notify the user they do not have valid privileges.
		if (! $this->useracl->is_privileged('Insert User Groups'))
		{
			$this->session->set_flashdata('message', '<p class="error_msg">You do not have privileges to insert new user groups.</p>');
			redirect($this->manage_user_group_url);		
		}
		// If 'Add User Group' form has been submitted, insert the new user group.
		if ($this->input->post('insert_user_group')) 
		{
			if ($this->form_validation->run('insert_user_group'))
			{
				$this->load->model('group_privilage_model','obj_group_privilage_model');
				
				$group_name = $this->input->post('group_name');
				$group_desc = $this->input->post('group_description');
				$group_admin = ($this->input->post('group_admin')) ? 1 : 0;
				$this->obj_group_privilage_model->insert_group($group_name, $group_desc, $group_admin);

				// Save any public or admin status or error messages to CI's flash session data.
				$this->session->set_flashdata('message', $this->obj_group_privilage_model->get_messages());
				// Redirect user.
				redirect($this->manage_user_group_url);			
			}
		}
		// Set any returned status/error messages.
		$this->content->message = (!isset($this->content->message)) ? $this->session->flashdata('message') : $this->content->message;
		$this->content->base_url =  base_url().$this->module_url;		
		$this->refresh_view('user_group_insert_view',$title='Users Groups');
	}
	public function edit_group($group_id)
	{
		if(!is_numeric($group_id) ||(is_numeric($group_id) && $group_id <= 0))
		{
			$this->session->set_flashdata('message','Invalid Group ID');
			redirect($this->manage_user_group_url);
		}
		$this->load->model('group_privilage_model','obj_group_privilage_model');
		$this->load->library('form_validation');
		if ($this->input->post('update_user_group')) 
		{
			if ($this->form_validation->run('insert_user_group'))
			{
				$dl_object =  new User_Group_Entity();
	
				// Get user group data from input.
				$dl_object->ugrp_name = $this->input->post('group_name');
				$dl_object->ugrp_desc =  $this->input->post('group_description');
				$dl_object->ugrp_admin  = $this->input->post('group_admin');
				$this->obj_group_privilage_model->update_group($group_id, $dl_object);
				// Save any public or admin status or error messages to CI's flash session data.
				$this->session->set_flashdata('message', $this->obj_group_privilage_model->get_messages());
				// Redirect user.
				redirect('admin/user_management/manage_groups');			
			}
		}
		
		// Get user groups current data.
		$this->content->group =  $this->obj_group_privilage_model->get_group($group_id);
		$this->content->message = (!isset($this->content->message)) ? $this->session->flashdata('message') : $this->content->message;
		$this->content->base_url =  base_url().$this->module_url;
		$this->refresh_view('user_group_update_view',$title='Users Groups');
	}
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// Privilages
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	public function manage_privilages()
	{
		// Check user has privileges to view user privileges, else display a message to notify the user they do not have valid privileges.
		if (! $this->useracl->is_privileged('View Privileges'))
		{
			$this->session->set_flashdata('message', '<p class="error_msg">You do not have access privileges to view user privileges.</p>');
			redirect($this->module_url);		
		}
		$this->load->model('group_privilage_model','obj_group_privilage_model');
		// If 'Manage Privilege' form has been submitted and the user has privileges to delete privileges.
		if ($this->input->post('delete_privilege') && $this->useracl->is_privileged('Delete Privileges')) 
		{
			$privi_ids = (array_keys($this->input->post('delete_privilege')));
			$this->obj_group_privilage_model->delete_privilege($privi_ids);
		}
		$this->content->privileges = $this->obj_group_privilage_model->get_privileges();

		$this->content->edit_url = 'admin/user_management/edit_privilages';
		$this->content->list_url = 'admin/user_management/manage_privilages';
		$this->content->base_url =  base_url();
		$this->content->message = (!isset($this->content->message)) ? $this->session->flashdata('message') : $this->content->message;		
		$this->refresh_view('privileges_view',$title='Privileges');
	}
	public function edit_privilages($privilage_id ='')
	{
		$this->content->edit_url = 'admin/user_management/edit_privilages';
		$this->content->list_url = 'admin/user_management/manage_privilages';
		
		$this->load->model('group_privilage_model','obj_group_privilage_model');
		$privi = new Privilege_Entity();
		$this->load->library('form_validation');

		if ($this->form_validation->run('insert_privilege'))
		{
			// Get privilege data from input.
			$privi->upriv_name = $this->input->post('upriv_name');
			$privi->upriv_desc = $this->input->post('upriv_desc');
			if($privilage_id =='')
				$this->obj_group_privilage_model->insert_privilege($privi->upriv_name, $privi->upriv_desc);
			else
				$this->obj_group_privilage_model->update_privilege($privilage_id, $privi);
				
			// Save any public or admin status or error messages to CI's flash session data.
			$this->session->set_flashdata('message', $this->obj_group_privilage_model->get_messages());
			
			// Redirect user.
			redirect($this->content->list_url);			
		}
		if($privilage_id=='')
			$this->content->privilege = $privi;
		else 
			$this->content->privilege =  $this->obj_group_privilage_model->get_privilege($privilage_id);	
		
		$this->content->mod_url = $this->module_url;
		$this->content->message = (!isset($this->content->message)) ? $this->session->flashdata('message') : $this->content->message;
		$this->refresh_view('privilege_update_view',$title='Privileges');		
	}
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// Group Privilages
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	public function edit_group_privileges($group_id)
	{
		// Check user has privileges to update group privileges, else display a message to notify the user they do not have valid privileges.
		if (! $this->useracl->is_privileged('Update Privileges'))
		{
			$this->session->set_flashdata('message', '<p class="error_msg">You do not have access privileges to update group privileges.</p>');
			redirect($this->module_url);		
		}
		$this->load->model('group_privilage_model','obj_group_privilage_model');
		// If 'Update Group Privilege' form has been submitted, update the privileges of the user group.
		if ($this->input->post('update_group_privilege')) 
		{
			$this->update_group_privileges($group_id);
		}
		
		// Get data for the current user group.
		$this->content->group = $this->obj_group_privilage_model->get_group($group_id);
		$this->content->privileges = $this->obj_group_privilage_model->get_privileges();
		
		// Get data for the current privilege group.
		$group_privileges = $this->obj_group_privilage_model->get_user_group_privileges($group_id);
		$this->content->group_privileges = array();
		foreach($group_privileges as $privilege)
		{
			$this->content->group_privileges[] = $privilege->upriv_groups_upriv_fk;
		}
	
		$this->content->form_url = $this->module_url;     
		$this->content->message = (!isset($this->content->message)) ? $this->session->flashdata('message') : $this->content->message;		
		$this->refresh_view('user_group_privileges_update_view',$title='Privileges');	
		
	}
	private function update_group_privileges($group_id)
	{
		// Update privileges.
		foreach($this->input->post('update') as $row)
		{
			if ($row['current_status'] != $row['new_status'])
			{
				// Insert new user privilege.
				if ($row['new_status'] == 1)
				{
					$this->obj_group_privilage_model->insert_user_group_privilege($group_id, $row['id']);	
				}
				// Delete existing user privilege.
				else
				{
					$this->obj_group_privilage_model->delete_user_group_privilege($group_id,$row['id']);
				}
			}
		}
		// Save any public or admin status or error messages to CI's flash session data.
		$this->session->set_flashdata('message', $this->obj_group_privilage_model->get_messages());
		// Redirect user.
		redirect($this->manage_user_group_url);			
		
	}
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// Private functions
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	public function refresh_view($name='',$title='')
	{
		$name= 'admin/user_acc/'.$name;
		$this->masterpage->setPageTitle($title);
		$this->masterpage->addContentPage ($name, 'content',(array)$this->content);
        $this->masterpage->show();
	}
}