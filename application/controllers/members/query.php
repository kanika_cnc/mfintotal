<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Query extends MY_Controller
{ 
	public function __construct()
	{
		parent::__construct();
		$this->init_member_content();
		$this->load->library('querylib');
	}
	
	/**
	 * @todo : upload file and attachments
	 * @todo :Add timer
	 * Enter description here ...
	 */
	public function index()
	{
		if ($this->input->is_ajax_request())
		{
				$this->load->library('form_validation');
				// Run the validation.
				if ($this->form_validation->run('ask_query'))
				{
					$has_attachment = 0;
					if($this->input->post('f') != "")
					$has_attachment = 1;
					$query = array("description"=>$this->input->post('q'),
							"has_attachment"=>$has_attachment);
					$message_id = $this->querylib->create_message_by_member($query,$this->useracl->get_logged_in_user_id(),$this->useracl->get_logged_in_user_manager_id());
					if($this->input->post('f') != "")
					{
						$files = $this->input->post('f');
						mkdir("./uploads/query/".$message_id, 0755);
						$files = $this->input->post('f');
						$arr_files = explode(",",$files);
						foreach($arr_files as $file)
						{
						if(!empty($file))
						{
						rename("./uploads/query/".trim($file),$_SERVER['DOCUMENT_ROOT']."mfintotal1/uploads/query/".$message_id.'/'.trim($file));
						//unlink($_SERVER['DOCUMENT_ROOT']."mfintotal1/uploads/query/".trim($file));
						}
						}
					}
					$message = $this->querylib->get_message_by_id($message_id);
					$this->content->message =$message;
					//debug($message);
					$this->load->view('common/qm_message',(array)$this->content);
				}
				else
				{
					echo json_encode(array("message"=>validation_errors()));
				}
		}
		else
		{
			
			$messages = $this->querylib->get_user_messages($this->useracl->get_logged_in_user_id());
			$this->content->messages =$messages;
			//debug($messages);
			$this->refresh_view();
		}
	}
	public function query_attributes($message_id=0)
	{
		if($this->input->post())
		{
			
			$this->load->library('form_validation');
			if ($this->form_validation->run('attributes'))
			{
				debug($this->input->post());
				$messages =  $this->querylib->save_user_message_answered_attributes($this->useracl->get_logged_in_user_id(),$this->input->post());
				//debug($messages);
			}
			else
			{
					echo json_encode(array("message"=>validation_errors()));
			}
		}
		else
		{
			$message = $this->querylib->get_message_by_id($message_id);
			$this->content->attributes = $message->qm_attributes;
			$this->load->view('member/attribute_form',(array)$this->content);
		}
	}
	public function refresh_view()
	{
		$name ='member/query_management_view';
		$title = 'Assistance';
		$this->masterpage->setPageTitle($title);
		$this->masterpage->addContentPage ($name, 'content',(array)$this->content);
        $this->masterpage->show();
	}
	public function upload_files()
	{
		$targetFolder = './uploads/query'; // Relative to the root
		
		if (!empty($_FILES))
		{
			$tempFile = $_FILES['Filedata']['tmp_name'];
			$targetPath = $targetFolder;
			$targetFile = rtrim($targetPath,'/') . '/' . $_FILES['Filedata']['name'];
			// Validate the file type
			$fileTypes = array('jpg','jpeg','gif','png','doc','docx','pdf','xls','xlsx','csv'); // File extensions
			$fileParts = pathinfo($_FILES['Filedata']['name']);
			if (in_array($fileParts['extension'],$fileTypes)) 
			{
				move_uploaded_file($tempFile,$targetFile);
				echo '1';
			} 
			else 
			{
				echo 'Invalid file type.';
			
			}
		}
	}
	public function validate_uploaded_files()
	{
		return true;
	}
}
