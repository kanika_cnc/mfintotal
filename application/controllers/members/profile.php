<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * Class to update logged in user profile information
 * Access : member , logged in user
 * Model : user_model
 * Lib : authuser
 * Lib : useracl 
 * @author fintotal
 * @todo: to remove call of user model and call through authuser
 * @todo : update user should update user session for user name
 */
class profile extends MY_Controller
{
	public $user; //logged in user
	public $user_id; //logged in user id
	public function __construct()
	{
		parent::__construct();
		
		$this->init_member_content();
		$this->content->base_url =  base_url().'members/profile/';
		
		
		$this->load->library('authuser');
		
		//get user data
		$this->load->model('user_model','obj_user_model');
		$this->user_id = $this->useracl->get_logged_in_user_id();
		$this->user = $this->obj_user_model->get_user_by_user_id($this->user_id);
		
	}
	public function index()
	{
		$this->view_profile();
	}
	public function view_profile()
	{
		$this->upload_photo();
		$this->change_password();
		$this->edit_profile();	
		$this->refresh_view($this->content,'Profile');
	}
	public function edit_profile()
	{
		$this->content->message["profile"] ="";
		//if page is submit for update
		if($this->input->post("editProfile"))
		{
			//validate form
			$this->load->library('form_validation');
			if($this->form_validation->run('rc_user_save'))
			{
				//if sucess update the profile (email non editable)
				$user_data =array();
				$user_data =  $this->input->post();
				if(!$this->obj_user_model->update_user($this->user_id,$user_data))
					$this->content->message["profile"] = "There is a problem updating profile. Please try after some time";
				else
					$this->content->message["profile"]= "Profile updated !!!";	
			}
			else
			{
				$this->content->message["profile"] = validation_errors();
			}	
		}
		
		$this->content->tab_data["user"] = $this->obj_user_model->get_user_by_user_id($this->user_id);;	
		$this->content->tab_name= 'profile';
	}
	public function upload_photo()
	{
		if($this->input->post("Upload"))
		{
			$this->load->library('form_validation');
			foreach ($_FILES as $key => $value)
			{
				if($value["name"] != "")
				{
					$this->load->library('upload');
			    	$this->upload->initialize($this->set_upload_options());
		    		if (!$this->upload->do_upload('flupload'))
					{
						$this->content->error = $this->upload->display_errors();
					}
					else
					{
						$data = $this->upload->data();
					}
				}
			}
			if(!empty($data))
			{
				$user_data["profile_photo"] =  $data["file_name"];
				$this->obj_user_model->update_user_profile($this->user_id,$user_data);
				$this->user->profile_photo =  $data["file_name"];;
			}
		}
		$photo = new stdClass();
		$photo->no_image_url = base_url(). "uploads/profile_images/" ."no_image.jpg";
		$photo->image_url = base_url(). "uploads/profile_images/".$this->user_id."/".$this->user->profile_photo;
		$this->content->photo =  $photo;
		$this->content->message["photo"] ="";
	}
	private function set_upload_options()
	{   
		$folder_name = './uploads/profile_images/'.$this->user_id;
		if(!is_dir($folder_name))
		{
	   		mkdir($folder_name,0777);
		}
		//  upload an image options
	    $config = array();
	    $config['upload_path'] = $folder_name;
	    $config['allowed_types'] = 'gif|jpg|png';
	    $config['max_size']      = '0';
	    $config['overwrite']     = FALSE;
	    return $config;
	}
	public function change_password()
	{
		$this->content->message["password"] = "";
		if($this->input->post("change_password"))
		{
			$this->load->library('form_validation');
			if($this->form_validation->run('user_change_password'))
			{
				$this->authuser->change_password($this->user_id,$this->input->post('current_password'),$this->input->post('new_password'));
				$this->content->message["password"] =  $this->authuser->message;
			}
			else
			{
				$this->content->message["password"] = validation_errors();
			}
		}
	}
	public function refresh_view($content)
	{
		$name ='member/user_acc/profile_view';
		$title = 'Profile';
		$this->masterpage->setMasterPage ('member/master');
		$this->masterpage->setPageTitle($title);
		$this->masterpage->addContentPage ($name, 'content',(array)$content);
        $this->masterpage->show();
	}
}