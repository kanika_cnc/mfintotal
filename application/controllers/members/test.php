<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->init_member_content();
		$this->load->library('querylib');
		$this->load->library('listlib');
	}
	
	/**
	 * @todo : upload file and attachments
	 * @todo :Add timer
	 * Enter description here ...
	 */
	public function index()
	{
		$list = $this->listlib->get_list(Listlib::mfin_modules);
		debug($list);
		
		$temp_attr = array("1"=>"10","2"=>"9","3"=>13,"4"=>"20000","5"=>"1");
		//$messages =  $this->querylib->save_user_message_answered_attributes($this->useracl->get_logged_in_user_id(),$temp_attr);
		//debug($messages);
		//exit();
		$temp = array("description"=>"added by kanika","has_attachment"=>0);
		
		//$messages = $this->querylib->create_message_by_member($temp,$this->useracl->get_logged_in_user_id(),$this->useracl->get_logged_in_user_manager_id());
		//debug($messages);
		//exit();
		$messages;
		//$messages =  $this->querylib->get_user_messages($this->useracl->get_logged_in_user_id());
		//debug($messages);
		$messages =  $this->querylib->get_message_by_id(10);
		debug($messages);
		exit();
		
		if($this->input->post('q') != "")
		{
			//echo $this->input->post('q');
			$temp = array("description"=>$this->input->post('q'),"has_attachment"=>0);
			$messages = $this->querylib->create_message_by_member($temp,$this->useracl->get_logged_in_user_id(),$this->useracl->get_logged_in_user_manager_id());
			//debug($messages);
			$this->content->messages =$messages;
			$this->load->view('common/qm_message',(array)$this->content);
		}
		else
		{
			$messages = $this->querylib->get_user_messages($this->useracl->get_logged_in_user_id());
			$this->content->messages =$messages;
			$this->refresh_view();
		}
	}
	
	public function refresh_view()
	{
		$name ='member/query_management_view';
		$title = 'Assistance';
		$this->masterpage->setPageTitle($title);
		$this->masterpage->addContentPage ($name, 'content',(array)$this->content);
        $this->masterpage->show();
	}
}
