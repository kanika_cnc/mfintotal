<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Non logged in user
class Dashboard extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		//$this->has_access();
	}

	public function index()
	{
		$this->refresh_view();
	}
	public function refresh_view()
	{
		$name ='member/dashboard';
		$title = 'Dashboard';
		$this->masterpage->setMasterPage ('member/master');
		$this->masterpage->setPageTitle($title);
		$this->masterpage->addContentPage ($name, 'content',(array)$this->content);
        $this->masterpage->show();
	}
}

