<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Acl extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('authuser');
	} 
	public function register_account_via_ajax()
	{
		
		if ($this->input->is_ajax_request())
		{
			// If 'Registration' form has been submitted, attempt to register their details as a new account.
			if ($this->input->post('createaccount'))
			{			
				$this->load->library('form_validation');
				// Run the validation.
				if ($this->form_validation->run('user_account_create'))
				{
					// Get user login details from input.
					$uacc_email = $this->input->post('uacc_email');
					$uacc_username = $this->input->post('uacc_username');
					$uacc_group_fk = 4;//UserAcl::USERGROUP_RC;
					$uacc_manager_acc_id = $this->authuser->get_next_manager();//$this->input->post('uacc_manager_acc_id');
					$uacc_password =  $this->input->post('uacc_password');
					$profile_data = array("profile_dob"=>$this->input->post('profile_dob'),
										"profile_location"=>$this->input->post('profile_dob'),
										"profile_gender"=>$this->input->post('profile_gender'));
					//Set whether to instantly activate account.
					$instant_activate = FALSE;
	
					$response = $this->authuser->insert_user($uacc_email, $uacc_username, $uacc_password, $uacc_manager_acc_id,$profile_data ,$uacc_group_fk, $instant_activate);

					if ($response)
					{
						$this->load->library('emailsend');
						$email_data = array('identity' => $uacc_email);
						$this->emailsend->send_email($uacc_email, 'Welcome', 'registration_welcome.tpl.php', $email_data);
						echo json_encode(array("message"=>$this->authuser->messages()));
					}
					else 
					{
						echo json_encode(array("message"=>$this->authuser->errors()));
					}
				}
				else 
				{
					echo json_encode(array("message"=>validation_errors()));
				}
			}
		}
	}
	
	/* login
	 * Login page used by all user types to log into their account.
	 * Users without an account can register for a new account.
	 * Note: This page is only accessible to users who are not currently logged in, else they will be redirected.
	 */ 
    function login()
    {
    	$this->load->library('authuser');
    	// If 'Login' form has been submited, attempt to log the user in.
		if ($this->input->post('login_user'))
		{
			$this->load->library('form_validation');
			// If failed login attempts from users IP exceeds limit defined by config file, validate captcha.
			if ($this->authuser->ip_login_attempts_exceeded($this->input->ip_address()))
			{
				$this->form_validation->set_rules('recaptcha_response_field', 'Captcha Answer', 'required|validate_recaptcha');				
			}
			if ($this->form_validation->run('user_login'))
			{
				// Check if user wants the 'Remember me' feature enabled.
				$remember_user = ($this->input->post('remember_me') == 1);
				if($this->authuser->login($this->input->post('login_identity'), $this->input->post('login_password'), $remember_user))
				{	
					$this->load->library('useracl');
					$this->useracl->process_redirection();
				}
				else
				{
					$this->data['message'] = $this->authuser->errors();
				}
				
			}
			else 
			{
				$this->data['message'] = validation_errors('<p class="error_msg">', '</p>');
			}
		}
		// If the user has exceeded the limit, generate a 'CAPTCHA' that the user must additionally complete when next attempting to login.
		if ($this->authuser->ip_login_attempts_exceeded($this->input->ip_address()))
		{
			$this->data['captcha'] = $this->authuser->recaptcha(FALSE);
		}
		// Get any status message that may have been set.
		$this->data['message'] = (! isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];		
		//$this->load->view('auth/login_view', $this->data);
		$this->refresh_view('login_view',$this->data);
	}
	/**
	 * login_via_ajax
	 * A simplified version of the above 'login' method that instead uses ajax to submit a users credentials.
	 * Note: This page is only accessible to users who are not currently logged in, else they will be redirected.
	 */ 
    function login_via_ajax()
    {
		if ($this->input->is_ajax_request())
		{
			$this->load->model('auth/demo_auth_model');
			
			$this->demo_auth_model->login_via_ajax();

			die($this->flexi_auth->is_logged_in());
		}
		else
		{
			$this->load->view('demo/login_via_ajax_view', $this->data);
		}
    }
    
	public function forgotten_password()
	{
		/// If the 'Forgotten Password' form has been submitted, then email the user a link to reset their password.
		if ($this->input->post('send_forgotten_password')) 
		{
			$this->load->library('form_validation');

			// Run the validation.
			if ($this->form_validation->run('forgot_password'))
			{
				// The 'forgotten_password()' function will verify the users identity exists and automatically send a 'Forgotten Password' email.
				$response = $this->authuser->forgotten_password($this->input->post('forgot_password_identity'));
				if($response)
				{
					$this->session->set_flashdata('message', $this->authuser->messages());
					$this->data['message'] = $this->authuser->messages();
				}
				else
				{
					$this->session->set_flashdata('message', $this->authuser->errors());
					$this->data['message'] = $this->authuser->errors();
						
				}
			}
			else
			{
				// Set validation errors.
				$this->data['message'] = validation_errors('<p class="error_msg">', '</p>');
			}
		}
		
		// Get any status message that may have been set.
		$this->data['message'] = (! isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];		
		$this->refresh_view('forgot_password_view',$this->data);
	}
	
	/**
	 * auto_reset_forgotten_password
	 * This is an example of automatically reseting a users password as a randomised string that is then emailed to the user. 
	 * See the manual_reset_forgotten_password() function above for the manual method of changing a forgotten password.
	 * In this demo, this page is accessed via a link in the 'views/includes/email/forgot_password.tpl.php' email template, which must be set to 'auth/auto_reset_forgotten_password/...'.
	 */
	function auto_reset_forgotten_password($user_id = FALSE, $token = FALSE)
	{
		// forgotten_password_complete() will validate the token exists and reset the password.
		// To ensure the new password is emailed to the user, set the 4th argument of forgotten_password_complete() to 'TRUE' (The 3rd arg manually sets a new password so set as 'FALSE').
		// If successful, the password will be reset and emailed to the user.
		if($this->authuser->forgotten_password_complete($user_id, $token, FALSE, TRUE))
		{
			// Set a message to the CI flashdata so that it is available after the page redirect.
			$this->session->set_flashdata('message', $this->authuser->messages());
			
		}
		else
		{
			// Set a message to the CI flashdata so that it is available after the page redirect.
			$this->session->set_flashdata('message', $this->authuser->errors());
		}
		$this->data['message'] = (! isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];
		$this->refresh_view('message_view',$this->data);
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// Account Activation
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	

	/**
	 * activate_account
	 * User account activation via email.
	 * The default setup of this demo requires that new account registrations must be authenticated via email before the account is activated.
	 * In this demo, this page is accessed via an activation link in the 'views/includes/email/activate_account.tpl.php' email template.
	 */ 
	function activate_account($user_id, $token = FALSE)
	{
		// This should always be set to TRUE for users verifying their account via email.
		// Only set this variable to FALSE in an admin environment to allow activation of accounts without requiring the activation token.
		if($this->authuser->activate_user($user_id, $token, TRUE))
			$this->data["message"] = $this->authuser->messages();
		else
			$this->data["message"] = $this->authuser->errors();
			
		$this->refresh_view('message_view',$this->data);	
		// Save any public status or error messages (Whilst suppressing any admin messages) to CI's flash session data.
	}
	
	/**
	 * resend_activation_token
	 * Resend user an activation token via email.
	 * If a user has not received/lost their account activation email, they can request a new activation email to be sent to them.
	 * In this demo, this page is accessed via a link on the login page.
	 */ 
	function resend_activation_token()
	{
		// If the 'Resend Activation Token' form has been submitted, resend the user an account activation email.
		if ($this->input->post('send_activation_token')) 
		{
			$this->load->library('form_validation');
			$this->form_validation->set_rules('activation_token_identity', 'Identity (Email / Login)', 'required');
			
			// Run the validation.
			if ($this->form_validation->run())
			{					
				// Verify identity and resend activation token.
				if($this->authuser->resend_activation_token($this->input->post('activation_token_identity')))
				{
					$this->data['message'] = $this->authuser->messages();
				}
				else
				{
					$this->data['message'] = $this->authuser->errors();
				}
			}
			else
			{	
				// Set validation errors.
				$this->data['message'] = validation_errors('<p class="error_msg">', '</p>');
			}
		}
		
		// Get any status message that may have been set.
		$this->data['message'] = (! isset($this->data['message'])) ? $this->session->flashdata('message') : $this->data['message'];		

		$this->refresh_view('resend_activation_token_view',$this->data);		
	}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// Logout
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	

	/**
	 * logout
	 * This example logs the user out of all sessions on all computers they may be logged into.
	 * In this demo, this page is accessed via a link on the demo header once a user is logged in.
	 */
	function logout() 
	{
		// By setting the logout functions argument as 'TRUE', all browser sessions are logged out.
		$this->authuser->logout(TRUE);
		// Set a message to the CI flashdata so that it is available after the page redirect.
		$this->session->set_flashdata('message', $this->authuser->messages());
		$this->load->library('useracl');
		$this->useracl->process_redirection();
	}
	
	private function refresh_view($name='',$content=array(),$title='')
	{
		$this->masterpage->setMasterPage ('web/master');
		$this->masterpage->setPageTitle($title);
		$this->masterpage->addContentPage ('auth/'.$name, 'content',$content);
        $this->masterpage->show();
	}
}