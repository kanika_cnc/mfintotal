<?php
class MY_Controller extends CI_Controller
{
	/**
	 * 
	 * Global variable used by all controllers to pass data from controller to view
	 * @var stdclass
	 */
	public $content;
	
	
	public function __construct()
	{
		parent::__construct();
		$this->content = new stdClass();
		$this->content->error='';
		$this->content->message='';
		$this->load->library('useracl');
		//defined in base controller
		$this->useracl->check_page_access();
 
		if($this->useracl->is_member())
		{
			$this->init_member_content();
		}
		else
		{
			$this->init_admin_content();
		}
	}
	
	
	public function is_admin()
	{
		echo "i am called from controo";
	}
	
	public function init_member_content()
	{
		$this->content->user =  $this->useracl->get_logged_in_user();
		$this->masterpage->setMasterPage ('member/master');
	}
	public function init_admin_content()
	{
		$this->content->user =  $this->useracl->get_logged_in_user();
		$this->masterpage->setMasterPage ('admin/master');
	}
}