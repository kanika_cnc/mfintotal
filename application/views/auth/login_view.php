<?php $base_url = base_url();?>
<div class="content_wrap main_content_bg">
	<div class="biglogin-box">
	<?php if (! empty($message)) { ?>
		<div id="message">
			<?php echo $message; ?>
		</div>
	<?php } ?>
				
	<?php echo form_open(base_url().'auth/acl/login',"");?>  	
		<h3>Login</h3>
		<div class="border"></div>
		<table cellpadding="5" cellspacing="5" border="0" >
			<tr>
				<td>
					<label for="identity">Email</label>
				</td>
				<td>
					<input type="text" id="identity" class="input" Placeholder='Email' name="login_identity" value="<?php echo set_value('login_identity');?>" class="tooltip_parent"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="password">Password:</label>
				
				</td>
				<td>
				<input type="password" id="password" class="input" Placeholder='Password' name="login_password" value="<?php echo set_value('login_password');?>"/>	
				</td>
			</tr>
			<tr>
				<td>
				
				</td>
				<td style="text-align:right">
					<input type="checkbox" id="remember_me" name="remember_me"  value="1" <?php echo set_checkbox('remember_me', 1); ?>/><label for="remember_me">Remember Me</label>
				
				</td>
			</tr>
			<tr>
				<td>
				
				</td>
				<td style="text-align:right">
						<?php 
							if (isset($captcha)) 
							{
								echo "<br/>"; 
								echo "<li>\n";
								echo $captcha;
								echo "</li>\n";
							}
						?>
					<input type="submit" name="login_user" id="submit" value="Submit" class="button"/>
				
				</td>
			</tr>
			<tr>
				<td>
				</td>
				<td style="text-align:right">
					<a href="<?php echo base_url();?>auth/acl/resend_activation_token" >Resend
					Account Activation Token</a>
				</td>
			</tr>
			<tr>
				<td>
				
				</td>
				<td style="text-align:right">
					<a href="<?php echo base_url();?>auth/acl/forgotten_password" >Reset
					Forgotten Password</a>
				
				</td>
			</tr>
		</table>
			<?php echo form_close();?>
		</div>
	</div>

<?php $this->load->view('auth/register_widget');?>

	