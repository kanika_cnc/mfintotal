<?php echo form_open(base_url().'auth/acl/login',"name='frmloginw'");?>
<div class="login-box">
	<input type="text"
		id="identity" name="login_identity"
		value=""
		class="input" Placeholder='Email' /> 
	
	<input type="password"
		id="password" name="login_password"
		value="" class="input" Placeholder='Password' />
	<?php if (isset($captcha)){	echo $captcha; }?>
	<input type="submit"
		name="login_user" id="submit" value="Login" class="button" />
	<br/>	
	<input
		type="checkbox" id="remember_me" name="remember_me" value="1"
		<?php echo set_checkbox('remember_me', 1); ?> /><span class="remember_text">Remember Me</span>
	<a href="<?php echo base_url();?>auth/acl/forgotten_password" class="login_text">Forgot Password?</a>
	<a href="<?php echo base_url();?>auth/acl/resend_activation_token" class="login_text" style="display:none" >Resend Account Activation Token</a>
</div>

<?php echo form_close();?>

	