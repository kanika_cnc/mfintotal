<script language="javascript">
$(document).ready(function(e){
	$('form[name="registeruser"]').submit(function(event){
   //$('#createaccount').click(function(event) {
	   event.preventDefault();
	   var url = '<?php echo base_url()?>' + 'auth/acl/register_account_via_ajax';
		// Get the form data.
		var $form_inputs = $('form').find(':input');
		var form_data = {};
		$form_inputs.each(function() 
		{
			form_data[this.name] = $(this).val();
		});
	   $.ajax(
		{
			url: url,
			type: 'POST',
			data: form_data,
			success:function(data)
			{
				$("#message").html(data);
				
			}
		});
	});
});
</script>

<div class="link">
	<button id="btnCreateAccount" class="account" onClick="javascript:showHide('register-box');">Create Account Here !</button>
</div>
<div id="register-box" style="display:none;">
	<div id="message" style="overflow:auto">
	</div>
	<h3>Register <button onClick="javascript:showHide('register-box');">X</button></h3>
    <div class="border"></div>
	<form action="" name="registeruser" method="post" id="registeruser" class="registeruser">
		<table border="0" cellpadding="5" cellspacing="5" width="90%">
			<tr>
				<td>
					<?php echo form_label("Name");?> 
			
				</td>
					  
				<td>
					<?php echo form_input("uacc_username","","class=''");	?>
				</td>
			</tr>
			<tr>
				<td>
					<?php echo form_label("Email");?> 
			
				</td>
				<td>
					<?php echo form_input("uacc_email","","class=''");	?>
				</td>
			</tr>
			<tr>
				<td>
					<?php echo form_label("Password");?> 
			
				</td>
				<td>
					<?php echo form_password("uacc_password","","class=''");	?>
				</td>
			</tr>
			<tr>
				<td>
					<?php echo form_label("Confirm Password");?> 
			
				</td>
				<td>
					<?php echo form_password("uacc_password_match","","class=''");	?>
				</td>
			</tr>
			<tr>
				<td>
					<?php echo form_label("DOB");?> 
			
				</td>
					  
				<td>
					<script>
					  $(function() {
					    $( "#datepicker" ).datepicker();
					  });
					  </script>
					<?php echo form_input("profile_dob","","class='' id='datepicker'");	?>
				</td>
			</tr>
			<tr>
				<td>
					<?php echo form_label("Location");?> 
			
				</td>
					  
				<td>
					<?php echo form_input("profile_location","","class=''");	?>
				</td>
			</tr>
			<tr>
				<td>
					
					<?php echo form_label("Gender");?> 
			
				</td>
					  
				<td>
					<?php echo form_radio("profile_gender","F","","")?>Female
					<?php echo form_radio("profile_gender","M","","")?>Male
				</td>
			</tr>

		</table>
		<?php echo form_submit('createaccount',"Register","id='createaccount'");?>
		<?php echo form_submit('clear',"Clear","id='clear'");?> 
		<div class="clear"></div>
	</form>
</div>

<script type="text/javascript">
function showHide(id){ 
	if (document.getElementById){ 
		obj = document.getElementById(id); 
		if (obj.style.display == "none"){ 
			obj.style.display = "block";
			//$(".link").html('<a href=javascript:showHide(\'register-box\');>Hide the Skwerl</a> <button onclick=javascript:showHide(\'register-box\');>Hide the Skwerl</button>');
		} else { 
			obj.style.display = "none"; 
			//$(".link").html('<a href=javascript:showHide(\'register-box\');>Show the Skwerl</a> <button onclick=javascript:showHide(\'register-box\');>Show the Skwerl</button>');
		} 
	} 
  }
</script>