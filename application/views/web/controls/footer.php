<div id="footer"><br />
        <div class="border"></div>
        
        <div style="padding-top:8px;">
        <ul>
            <li><a href="http://www.fintotal.com/">Home</a></li>
            <li><a href="http://www.fintotal.com/about/">About Us</a></li>
            <li><a href="http://www.fintotal.com/resources/">Resources to Use</a></li>
            <li><a href="http://fintotalblog.blogspot.in/">Blog</a></li>
            <li><a href="http://fintotal.com/media">Media</a></li>
            <li><a href="http://find.fintotal.com/">Find</a></li>
            <li><a href="http://www.fintotal.com/privacy.html">Privacy Statement</a></li>
            <li><a href="http://www.fintotal.com/terms.html">Terms &amp; Conditions</a></li>
            <li class="last"><a href="http://www.fintotal.com/disclaimer.html">Disclaimer</a></li>
        </ul> 
        <p align="center">All Rights Reserved, Copyright © 2011-2012 <a href="#">Fintotal Advisory  and Consulting Pvt. Ltd.</a></p>
        </div>  
    </div><!--end footer-->
</div><!--end wrapper-->

<script type="text/javascript">

$(document).ready(function(){
//open popup
$("#pop").click(function(){
  $("#overlay_form").fadeIn(1000);
  positionPopup();
});

//close popup
$("#close").click(function(){
	$("#overlay_form").fadeOut(100);
});
});

//position the popup at the center of the page
function positionPopup(){
  if(!$("#overlay_form").is(':visible')){
    return;
  } 
  $("#overlay_form").css({
      left: ($(window).width() - $('#overlay_form').width()) / 2,
      top: ($(window).width() - $('#overlay_form').width()) / 7,
      position:'absolute'
  });
}




$(document).ready(function(){
//open popup
$("#pop-password").click(function(){
  $("#forgot-password").fadeIn(1000);
  positionPopup();
});

//close popup
$("#close").click(function(){
	$("#forgot-password").fadeOut(100);
});
});

//position the popup at the center of the page
function positionPopup(){
  if(!$("#forgot-password").is(':visible')){
    return;
  } 
  $("#forgot-password").css({
      left: ($(window).width() - $('#forgot-password').width()) / 2,
      top: ($(window).width() - $('#forgot-password').width()) / 7,
      position:'absolute'
  });
}

//maintain the popup at center of the page when browser resized
$(window).bind('resize',positionPopup);

</script>
