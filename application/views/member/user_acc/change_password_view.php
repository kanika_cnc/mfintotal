<!--Popup function-->
<script type="text/javascript">
$(document).ready(function(e){
	$('#change_password').click(function(event) {
		togglePopup("edtusr");
		return false;
		});
	$('#closewindow').click(function(event) {
		togglePopup("edtusr");
		return false;
		});
	
	});
function togglePopup(box){
	var popupBox = document.getElementById(box);
	if(popupBox.style.display == "block"){
		popupBox.style.display = "none";
	} else {
		popupBox.style.display = "block";
	}
}
</script>
	<a href="#" id="change_password">Change Password</a>
</div>

<div class="popupBox" id="edtusr">
	<div class="content_wrap main_content_bg">
		<div class="content clearfix">
			<div class="col100">
				<div>
					<h2>Update Password</h2>
					<a href="#" id="closewindow" style="float:right">X</a>
				</div>
				<div id="message">
					<?php echo $message["password"]; ?>
				</div>
				<?php echo form_open("","name='frm_change_password'" ,"id='frm_change_password'");	?>  	
					<div class="w100 frame">
						<ul>
							<li class="info_req">
								<label for="current_password">Current Password:</label>
								<input type="password" id="current_password" name="current_password" value="<?php echo set_value('current_password');?>"/>
							</li>
							<li class="info_req">
								<label for="new_password">New Password:</label>
								<input type="password" id="new_password" name="new_password" value="<?php echo set_value('new_password');?>"/>
							</li>
							<li class="info_req">
								<label for="confirm_new_password">Confirm New Password:</label>
								<input type="password" id="confirm_new_password" name="confirm_new_password" value="<?php echo set_value('confirm_new_password');?>"/>
							</li>
							<li>
								<input type="submit" name="change_password" id="submit" value="Submit" class="link_button large"/>
							</li>
						</ul>
					</div>
				<?php echo form_close();?>
			</div>
		</div>
	</div>	
</div>
<script>
var is_failed_postback = '<?php echo $message["password"] != "" ? true:false ?>';
if(is_failed_postback == 1)
{
	togglePopup("edtusr");
}

</script>