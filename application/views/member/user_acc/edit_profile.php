<script language="javascript">
$(document).ready(function(e){
   $('#editProfile_later').click(function(event) {
	   event.preventDefault();
	   alert("here");
	   var url = '<?php echo $base_url?>' + 'edit_profile';
		// Get the form data.
		var $form_inputs = $('form').find(':input');
		var form_data = {};
		$form_inputs.each(function() 
		{
			form_data[this.name] = $(this).val();
		});
	   $.ajax(
		{
			url: url,
			type: 'POST',
			data: form_data,
			success:function(data)
			{
				alert(data);
			}
		});
	});
})
</script>

<div class="boxbody">
	<div id="message">
		<?php echo $message["profile"]; ?>
		<?php echo $error; ?>
	</div>
	<form action="" name="registeruser" method="post" id="edtusrfrm" class="edtusrfrm">
		<table border="1" cellpadding="0" cellspacing="0" width="90%">
			<tr>
				<td>
					<?php echo form_label("Name");?> 
			
				</td>
					  
				<td>
					<?php echo form_input("uacc_username",$user->uacc_username,"class=''");	?>
				</td>
			</tr>
			<tr>
				<td>
					<?php echo form_label("Email");?> 
			
				</td>
				<td>
					<?php echo $user->uacc_email;?>
				</td>
			</tr>
			<tr>
				<td>
					<?php echo form_label("DOB");?> 
			
				</td>
					  
				<td>
					<script>
					  $(function() {
					    $( "#datepicker" ).datepicker();
					  });
					  </script>
					<?php echo form_input("profile_dob",$user->profile_dob,"class='' id='datepicker'");	?>
				</td>
			</tr>
			<tr>
				<td>
					<?php echo form_label("Location");?> 
			
				</td>
					  
				<td>
					<?php echo form_input("profile_location",$user->profile_location,"class=''");	?>
				</td>
			</tr>
			<tr>
				<td>
					
					<?php echo form_label("Gender");?> 
			
				</td>
					  
				<td>
					<?php echo form_radio("profile_gender","F",$user->profile_gender=="F"?"checked":"","")?>Female
					<?php echo form_radio("profile_gender","M",$user->profile_gender=="M"?"checked":"","")?>Male
				</td>
			</tr>

		</table>
		<?php echo form_submit('editProfile',"Update Profile","id='editProfile'");?> 
		<div class="clear"></div>
	</form>
</div>

