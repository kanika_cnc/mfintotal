<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><mp:Title/></title>
<link type="text/css" href="<?php echo asset_url(); ?>css/style.css" rel="stylesheet" media="all"/>
<link type="text/css" href="<?php echo asset_url(); ?>css/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" media="all"/>
<link rel="stylesheet" type="text/css" href="<?php echo asset_url(); ?>source/jquery.fancybox.css" media="screen" />
<link type="text/css" href="<?php echo asset_url(); ?>css/uploadify.css" rel="stylesheet" media="all"/>
<script src="<?php echo asset_url(); ?>js/jquery-1.9.1.js"></script>
<script src="<?php echo asset_url(); ?>ckeditor/ckeditor.js"></script>
<script src="<?php echo asset_url(); ?>ckeditor/adapters/jquery.js"></script>
<script type="text/javascript" src="<?php echo asset_url(); ?>js/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="<?php echo asset_url(); ?>source/jquery.fancybox.js"></script>

<script src="<?php echo asset_url(); ?>js/jquery-ui-1.10.3.custom.js"></script>
<script src="<?php echo asset_url(); ?>js/dropdowncontent.js"></script>
<script src="<?php echo asset_url(); ?>js/jquery.uploadify.js"></script>
<script type="text/javascript">
function PopUp(hideOrshow) {
	if(hideOrshow == 'show')
     document.getElementById('popup').style.display = "block";
	 else
	 document.getElementById('popup').style.display = "none";
}
window.onload = function () {
    setTimeout(function () {
        PopUp('show');
    }, 5000);
}
$(function() {
	$( 'textarea' ).ckeditor();

	$(".fancybox").fancybox();
	
} );


</script>
</head>

<body>
	<div class="wrapper">
	<div class="popupbox" id="popup">
    	<img src="<?php echo asset_url(); ?>images/popup.png" width="255" height="130" onclick="PopUp('hide')" />
    </div>
	
    	<?php  $this->load->view('member/controls/header',$user)?>
	<div class="container">
    	<mp:Content/>
    </div>
</div>
	</div>
	</body>
</html>
