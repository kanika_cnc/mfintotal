
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><mp:Title/> | Admin |Fintotal.com</title>
<link rel="stylesheet" href="<?php echo asset_url()?>css/admin.css" />
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/>
<script src="<?php echo asset_url()?>js/jquery-1.9.1.js"></script>
<script src="<?php echo asset_url()?>js/jquery-ui-1.10.3.custom.js"></script>
</head>
<body>
 <!--Wrapper starts-->
<div class="wrapper">
<!--container starts-->
	<div class="container">
			<?php $this->load->view( "admin/controls/header"); ?>
			<mp:Content/>
    </div>
    <!--container ends-->
</div>
 <!--Wrapper ends-->
</body>
</html>