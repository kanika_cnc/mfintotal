<?php
$base_url = base_url(); 
?>
<!-- Main Content -->
<div class="content_wrap main_content_bg">
<div class="content clearfix">
<div class="col100">
<h2>Update User Group Privileges of Group '<?php echo $group->ugrp_name; ?>'</h2>
<a href="<?php echo $base_url.$form_url;?>/manage_groups">
Manage User Groups</a> | 
<a 	href="<?php echo $base_url.$form_url;?>/edit_group/<?php echo $group->ugrp_id; ?>">Update
User Group</a> 
<?php if (! empty($message)) { ?>
<div id="message"><?php echo $message; ?></div>
<?php } ?> 

<?php echo form_open(current_url());	?>
<div class="userlist">
<table id="usrlst" width="100%" border="1" cellspacing="0" cellpadding="0">
	<thead>
		<tr>
			<th class="tooltip_trigger" title="The name of the privilege." />
			Privilege Name
			</th>
			<th class="tooltip_trigger"
				title="A short description of the purpose of the privilege." />
			Description
			</th>
			<th class="spacer_150 align_ctr tooltip_trigger"
				title="If checked, the group will be granted the privilege." />
			Group Has Privilege
			</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($privileges as $privilege) { ?>
		<tr>
			<td><input type="hidden"
				name="update[<?php echo $privilege->upriv_id; ?>][id]"
				value="<?php echo $privilege->upriv_id; ?>" />
				<?php echo $privilege->upriv_name;?>
			</td>
			<td><?php echo $privilege->upriv_desc;?></td>
			<td class="align_ctr"><?php 
			// Define form input values.
			$current_status = (in_array($privilege->upriv_id, $group_privileges)) ? 1 : 0;
			$new_status = (in_array($privilege->upriv_id, $group_privileges)) ? 'checked="checked"' : NULL;
			?> <input type="hidden"
				name="update[<?php echo $privilege->upriv_id;?>][current_status]"
				value="<?php echo $current_status ?>" /> <input type="hidden"
				name="update[<?php echo $privilege->upriv_id;?>][new_status]"
				value="0" /> <input type="checkbox"
				name="update[<?php echo $privilege->upriv_id;?>][new_status]"
				value="1" <?php echo $new_status ?> /></td>
		</tr>
		<?php } ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="3"><input type="submit" name="update_group_privilege"
				value="Update Group Privileges" class="link_button large" /></td>
		</tr>
	</tfoot>
</table>
	</div>	
		<?php echo form_close();?></div>
</div>
</div>
