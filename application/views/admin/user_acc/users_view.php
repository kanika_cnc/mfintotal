<script language="javascript">
$(document).ready(function(e){
   $('#addusr').click(function() {
    $('.hdnfrm').slideToggle('fast', function() {
     
  });
});
  
   $('.btnEditUser').click(function(event) {
	   var edit_id = this.id.split('_')[1];
	   var url = '<?php echo $base_url?>' + '/edit_user/'+ edit_id;
	   $.ajax(
		{
			url: url,
			type: 'POST',
			success:function(data)
			{
			
				// If the returned login value successul.
				if (data)
				{
					$("#edtusr").html(data);
				}
				else
				{
					$("#edtusr").html(data);
				}
			}
		});
	   togglePopup('edtusr');
	});
   $('.btnDeleteUser').click(function(event) {
	   if(!confirm("Are you sure you want to delete this user"))
	   {
		   return false;
	   }
	   var edit_id = this.id.split('_')[1];
	   var url = '<?php echo $base_url?>' + '/delete_user_via_ajax/'+ edit_id;
	   $.ajax(
		{
			url: url,
			type: 'POST',
			data:'input=delete',
			success:function(data)
			{
				// If the returned login value successul.
				if (data)
				{
					alert(data);
				}
				else
				{
					window.location.href = '<?php echo $base_url?>';
				}
			}
		});
	});
   $('.btnGenPass').click(function(event) {
	   if(!confirm("Are you sure you want to generate the new password for this user"))
	   {
		   return false;
	   }
	   var edit_id = this.id.split('_')[1];
	   var url = '<?php echo $base_url?>' + '/generate_password/'+ edit_id;
	   $.ajax(
		{
			url: url,
			type: 'POST',
			success:function(data)
			{
				// If the returned login value successul.
				if (data)
				{
					alert(data);
				}
				else
				{
					window.location.href = '<?php echo $base_url?>';
				}
			}
		});
	});
   $('#register_user').click(function(event) {
	   event.preventDefault();
	   var url = '<?php echo $base_url?>' + '/register_account/';
		// Get the form data.
		var $form_inputs = $('form').find(':input');
		//var $form_inputs = $(this).find(':select');
		var form_data = {};
		$form_inputs.each(function() 
		{
			form_data[this.name] = $(this).val();
		});
	   $.ajax(
		{
			url: url,
			type: 'POST',
			data: form_data,
			
			success:function(data)
			{
				if (data)
				{
					$("#message").html(data);
					/*var obj = jQuery.parseJSON(data);
					if(obj)
					{
						if(obj.error)
						{
							$("#message").html(obj.error);
						}
						else
						{
							$("#message").html(obj.message);
							//$(this).closest('form[name="registeruser"]').find("input[type=text], textarea").val("");
							//$('.hdnfrm').slideToggle('fast');
						}
					}
					else
					{
						$("#message").html(data);
					}*/
				}
			}
		});
	   //togglePopup('edtusr');
	});

});
</script>
<!--Popup function-->
<script type="text/javascript">
function togglePopup(box){
	var popupBox = document.getElementById(box);
	if(popupBox.style.display == "block"){
		popupBox.style.display = "none";
	} else {
		popupBox.style.display = "block";
	}
}
</script>
<!--Popup function-->

<!--add user-->
<?php if($this->useracl->is_privileged('Manage Admin Users')) {?>
<div class="btmsect"><a href="#" id="addusr">Add User</a></div>
<div id="message">
	<?php echo $message; ?>
	<?php echo validation_errors();?>
</div>
<!--add user-->
<!--add user hidden form-->
<div class="hdnfrm">
	<form action="<?php echo $base_url?>/register_account" name="registeruser" method="post" class="newusrfrm">
		<?php echo form_label("Name"); 
			  echo form_input("uacc_username","","");
	  	?> 
		<div class="clear"></div>
		<?php echo form_label("ID"); 
			  echo form_input("uacc_email","","") . "@fintotal.com";
	  	?> 
		<div class="clear"></div>
		<?php echo form_label("Rights"); 
			 echo form_dropdown("uacc_group_fk",$groups, "");
	  	?>
		<div class="clear"></div>
		<?php echo form_label("Map"); 
			echo form_dropdown("uacc_manager_acc_id",$groups_users, "");
	  	?>
		<div class="clear"></div>
		<?php echo form_submit("register_user","Submit","class='submitbtn' id='register_user'");?>
		<div class="clear"></div>
	</form>
</div>
<!--add user hidden form-->
<?php }?>
<!--user list starts-->
<div class="userlist">
<table width="100%" border="1" cellspacing="0" cellpadding="0"
	id="usrlst">
	<tr>
		<th>Name</th>
		<th>ID</th>
		<th>Rights</th>
		<th>Map</th>
		<?php if($this->useracl->is_privileged('Manage Admin Users')) {?>
		<th>Controls</th>
		<?php }?>
	</tr>
	<?php foreach ($users as $user){?>
	<tr>
		<td><?php echo $user->uacc_username;?></td>
		<td><?php echo $user->uacc_email;?></td>
		<td><?php echo $user->ugrp_name;?></td>
		<td><?php echo $user->uacc_manager_name;?></td>
		<?php if($this->useracl->is_privileged('Manage Admin Users')) {?>
		<td>
		<?php echo form_button("btnEditUser[".$user->uacc_id."]","Edit",'class="btnEditUser" id="edit_'.$user->uacc_id .'"')?>
		 /<?php echo form_button("btnDeleteUser[".$user->uacc_id."]","Delete",'class="btnDeleteUser" id="del_'.$user->uacc_id .'"')?>
		/ <a href="#"><?php echo form_button("btngeneratepassword[".$user->uacc_id."]","Generate Password",'class="btnGenPass" id="pass_'.$user->uacc_id .'"')?>
		password</a> / <a href="#" onclick="javascript:togglePopup('logs')">logs</a></td>
		<?php }?>
	</tr>
	<?php }?>
</table>
</div>
<!--user list ends-->

<!--edit user popup box starts-->
<div class="popupBox" id="edtusr">
</div>
<!--edit user popup box ends-->
<!--logs popup box starts-->
<div class="popupBox" id="logs">
<div class="boxheader">Logs<span class="cancelbtn">
<button onmousedown="togglePopup('logs')"></button>
</span></div>
<div class="boxbody">
<h4>Lost Sessions:</h4>
<ul class="lostsessn">
	<li>21 Nov 2012 12:22 hrs</li>
	<li>21 Nov 2012 12:22 hrs</li>
	<li>21 Nov 2012 12:22 hrs</li>
	<li>21 Nov 2012 12:22 hrs</li>
	<li>21 Nov 2012 12:22 hrs</li>
	<li>21 Nov 2012 12:22 hrs</li>
	<li>21 Nov 2012 12:22 hrs</li>
</ul>
</div>
<div class="boxfooter"></div>
</div>
<!--logs popup box ends-->
