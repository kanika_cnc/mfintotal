<!-- Main Content -->
	<div class="content_wrap main_content_bg">
		<div class="content clearfix">
			<div class="col100">
				<h2>Manage Privileges</h2>
				<a href="<?php echo $base_url.$edit_url;?>">Insert New Privilege</a>

				<div id="message">
					<?php echo $message; ?>
				</div>
				<div class="userlist">
				<?php echo form_open($list_url);	?>  	
					<table id="usrlst" width="100%" border="1" cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<th class="spacer_200 tooltip_trigger" 
									title="The name of the privilege.">
									Privilege Name
								</th>
								<th class="tooltip_trigger" 
									title="A short description of the purpose of the privilege.">
									Description
								</th>
								<!-- <th class="spacer_100 align_ctr tooltip_trigger" 
									title="If checked, the row will be deleted upon the form being updated.">
									Delete
								</th> -->
							</tr>
						</thead>
						<tbody>
						<?php foreach ($privileges as $privilege) { ?>
							<tr>
								<td>
									<a href="<?php echo $base_url.$edit_url;?>/<?php echo $privilege->upriv_id;?>">
										<?php echo $privilege->upriv_name;?>
									</a>
								</td>
								<td><?php echo $privilege->upriv_desc;?></td>
								<!-- <td class="align_ctr">
								<?php if ($this->useracl->is_privileged('Delete Privileges')) { ?>
									<input type="checkbox" name="delete_privilege[<?php echo $privilege->upriv_id;?>]" value="1"/>
								<?php } else { ?>
									<input type="checkbox" disabled="disabled"/>
									<small>Not Privileged</small>
									<input type="hidden" name="delete_privilege[<?php echo $privilege->upriv_id;?>]" value="0"/>
								<?php } ?>
								</td> -->
							</tr>
						<?php } ?>
						</tbody>
						<tfoot>
							<td colspan="3">
								<?php echo $disable = ($this->useracl->is_privileged('Delete Privileges')) ? 'disabled="disabled"' : "";?>
								<input type="submit" name="submit" value="Delete Checked Privileges" class="link_button large" <?php echo $disable; ?>/>
							</td>
						</tfoot>
					</table>
				<?php echo form_close();?>
				</div>
			</div>
		</div>
	</div>	
