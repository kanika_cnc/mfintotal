<?php
$base_url = base_url(); 
?>
	<!-- Main Content -->
	<div class="content_wrap main_content_bg">
		<div class="content clearfix">
			<div class="col100">
				<h2>Update Privilege</h2>
				<a href="<?php echo $base_url.$list_url;?>">Manage Privileges</a>

				<div id="message">
					<?php echo $message; ?>
					<?php echo validation_errors(); ?>
				</div>
			
				
				<?php echo form_open(current_url());	?>  	
					<fieldset>
						<legend>Privilege Details</legend>
						<ul>
							<li class="info_req">
								<label for="privilege">Privilege Name:</label>
								<input type="text" id="privilege" name="upriv_name" value="<?php echo set_value('upriv_name', $privilege->upriv_name);?>" class="tooltip_trigger"
									title="The name of the privilege."
								<?php echo form_hidden('upriv_id',$privilege->upriv_id)?>	
							</li>
							<li>
								<label for="description">Privilege Description:</label>
								<textarea id="description" name="upriv_desc" class="width_400 tooltip_trigger"
									title="A short description of the purpose of the privilege."><?php echo set_value('upriv_desc', $privilege->upriv_desc);?></textarea>
							</li>
						</ul>
					</fieldset>
									
					<fieldset>
						<legend>Update Privilege Details</legend>
						<ul>
							<li>
								<label for="submit">Update Privilege:</label>
								<input type="submit" name="update_privilege" id="submit" value="Submit" class="link_button large"/>
							</li>
						</ul>
					</fieldset>
				<?php echo form_close();?>
			</div>
		</div>
	</div>	
