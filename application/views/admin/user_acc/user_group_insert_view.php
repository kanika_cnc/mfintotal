	<!-- Main Content -->
	<div class="content_wrap main_content_bg">
		<div class="content clearfix">
			<div class="col100">
				<h2>Insert New User Group</h2>
				<a href="<?php echo $base_url;?>/manage_groups">Manage User Groups</a>
				<div id="message">
					<?php echo $message; ?>
					<?php echo validation_errors(); ?>
				</div>
				
				<?php echo form_open(current_url());	?>  	
					<fieldset>
						<legend>Group Details</legend>
						<ul>
							<li class="info_req">
								<label for="group">Group Name:</label>
								<input type="text" id="group" name="group_name" value="<?php echo set_value('group_name');?>" class="tooltip_trigger"
									title="The name of the user group."/>
							</li>
							<li>
								<label for="description">Group Description:</label>
								<textarea id="description" name="group_description" class="width_400 tooltip_trigger"
									title="A short description of the purpose of the user group."><?php echo set_value('group_description');?></textarea>
							</li>
							<li>
								<label for="admin">Is Admin Group:</label>
								<input type="checkbox" id="admin" name="group_admin" value="1" <?php echo set_checkbox('group_admin',1);?> class="tooltip_trigger"
									title="If checked, the user group is set as an 'Admin' group."/>
							</li>
						</ul>
					</fieldset>

					<fieldset>
						<legend>Insert New Group</legend>
						<ul>
							<li>
								<label for="submit">Insert Group:</label>
								<input type="submit" name="insert_user_group" id="submit" value="Submit" class="link_button large"/>
							</li>
						</ul>
					</fieldset>
				<?php echo form_close();?>
			</div>
		</div>
	</div>	
