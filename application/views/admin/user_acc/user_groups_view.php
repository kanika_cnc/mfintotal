<!-- Main Content -->
<div class="content_wrap main_content_bg">
<div class="content clearfix">
<div class="col100">
<h2>Manage User Groups</h2>
<a href="<?php echo $base_url;?>/add_group">Insert New User Group</a> <?php if (! empty($message)) { ?>
<div id="message"><?php echo $message; ?></div>
<?php } ?> <?php echo form_open(current_url());	?>
	<div class="userlist">
<table table width="100%" border="1" cellspacing="0" cellpadding="0" id="usrlst">
	<thead>
		<tr>
			<th class="spacer_150 tooltip_trigger" title="The user group name.">
			Group Name</th>
			<th class="tooltip_trigger"
				title="A short description of the purpose of the user group.">
			Description</th>
			<th class="spacer_100 align_ctr tooltip_trigger"
				title="Indicates whether the group is considered an 'Admin' group.<br/> Note: Privileges can still be set seperately.">
			Is Admin Group</th>
			<th class="spacer_100 align_ctr tooltip_trigger"
				title="Manage the access privileges of user groups.">User Group
			Privileges</th>
			<!-- <th class="spacer_100 align_ctr tooltip_trigger"
				title="If checked, the row will be deleted upon the form being updated.">
			Delete</th> -->
		</tr>
	</thead>
	<tbody>
	<?php foreach ($user_groups as $group) { ?>
		<tr>
			<td><a
				href="<?php echo $base_url?>/edit_group/<?php echo $group->ugrp_id;?>">
				<?php echo $group->ugrp_name;?>
			</a></td>
			<td><?php echo $group->ugrp_desc;?></td>
			<td class="align_ctr"><?php echo ($group->ugrp_admin == 1) ? "Yes" : "No";?></td>
			<td class="align_ctr"><a
				href="<?php echo $base_url.'/edit_group_privileges/'.$group->ugrp_id;?>">Manage</a>
			</td>
			<!-- <td class="align_ctr"><?php if ($this->useracl->is_privileged('Delete User Groups')) { ?>
			<input type="checkbox"
				name="delete_group[<?php echo $group->ugrp_id;?>]"
				value="1" /> <?php } else { ?> <input type="checkbox"
				disabled="disabled" /> <small>Not Privileged</small> <input
				type="hidden"
				name="delete_group[<?php echo $group->ugrp_id;?>]"
				value="0" /> <?php } ?></td> -->
		</tr>
		<?php } ?>
	</tbody>
	<tfoot>
		<td colspan="5">
		<?php $disable = (! $this->useracl->is_privileged('Update User Groups') && ! $this->useracl->is_privileged('Delete User Groups')) ? 'disabled="disabled"' : NULL;?>
		<input type="submit" name="submit" value="Delete Checked User Groups"
			class="link_button large" <?php echo $disable; ?> /></td>
	</tfoot>
</table>
	<?php echo form_close();?>
	</div>
	</div>
</div>
</div>
