<script language="javascript">
$(document).ready(function(e){
   $('#update_users_account').click(function(event) {
	   event.preventDefault();
	   var edit_id ='<?php echo $user->uacc_id ?>';
	   var url = '<?php echo $base_url?>' + '/edit_user/'+ edit_id;
		// Get the form data.
		var $form_inputs = $('form').find(':input');
		//var $form_inputs = $(this).find(':select');
		var form_data = {};
		$form_inputs.each(function() 
		{
			form_data[this.name] = $(this).val();
		});
	   $.ajax(
		{
			url: url,
			type: 'POST',
			data: form_data,
			success:function(data)
			{
				if (data)
				{
					$("#edtusr").html(data);
				}
				else
				{
					window.location.href = '<?php echo $base_url?>';
				}
			}
		});
	   //togglePopup('edtusr');
	});
});
</script>
<div class="boxheader">Edit User Details
	<span class="cancelbtn"><button onmousedown="togglePopup('edtusr')"></button></span>
</div>
<div class="boxbody">
	<div id="message">
		<?php echo $message; ?>
	</div>
	<form action="" name="registeruser" method="post" id="edtusrfrm" class="edtusrfrm">
		<?php echo form_label("Name"); 
			  echo form_input("uacc_username",$user->uacc_username,"");
	  	?> 
		<div class="clear"></div>
		<?php echo form_label("ID"); 
			  $arremail =  split('@',$user->uacc_email);	
			  echo form_input("uacc_email",$arremail[0],"");
			  echo form_label("@".$arremail[1]); 
	  	?> 
		<div class="clear"></div>
		<?php echo form_label("Rights"); 
			 echo form_dropdown("uacc_group_fk",$groups,$user->uacc_group_fk ,"");
	  	?>
		<div class="clear"></div>
		<?php echo form_label("Map"); 
			echo form_dropdown("uacc_manager_acc_id",$groups_users,$user->uacc_manager_acc_id ,"");
	  	?>
		<div class="clear"></div>
		<?php echo form_submit("update_users_account","Update","class='submitbtn' id='update_users_account'" );?>
		<div class="clear"></div>
	</form>
</div>
<div class="boxfooter"></div>