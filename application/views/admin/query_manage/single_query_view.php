 
<?php
	$form_id_single ='frmquery_'.$index;  
	echo form_open($form_url,"'id='".$form_id_single."'name='".$form_id_single."'"); 
?>				
				<div class="query">
				<?php
					$current_time =  strtotime(date("d-M-Y H:i:s"));
					$created_time =  strtotime(date("d-M-Y H:i:s",strtotime($message->qm_created_date)));
					if(($current_time - $created_time)/60 > 12)
					{
						echo '<img src="<?php echo asset_url()?>images/badge.gif" />';
					} 
				?>
				<?php echo form_hidden('hdn_message_id',$message->qm_id);?>
           		<?php echo form_hidden('hdn_member_id',$message->qm_uacct_id);?>
           		<?php
           			//This value to setup by popups 
           			echo form_hidden('hdn_selected_attr_id');
           			echo form_hidden('hdn_selected_article_suggestion_id');
           			echo form_hidden('hdn_archive_details');
           		?>
           		<?php echo form_hidden('hdn_action');?>
           		<p><?php echo $message->qm_description; ?></p>
				<ul class="querydetails">
                    <li>-----<?php echo $message->qm_uacct_name;?></li>
                    <li><?php echo date ("d-M-Y",strtotime($message->qm_created_date));?></li>
                    <li class="last"><?php echo date('H:i:s',strtotime($message->qm_created_date));?></li>
                </ul>
              <!--Query menu here--> 
              <ul class="querymenu">
              	<li><a href="conversation.html" target="_blank">Conversation</a></li>
              	<?php ?>
                <li><a href="#" id="queryans_<?php echo $index;?>" class="queryans">Answer</a></li>
                <li><a href="#" id="edtqueryans2">Edit Answer</a></li>
                <?php ?>
                <li><a href="#">User detail</a></li>
                <li><a href="#" onclick="javascript:togglePopup('transfer', '<?php echo $message->qm_id ?>','<?php echo $form_id_single?>')">Transfer</a></li>
                <?php if($this->useracl->is_privileged('Approve Message')) {?>
                <li><?php echo form_submit("btnSubmit".$index,"Approve")?></li>
                <?php }?>
                <li><a href="#">Ignore</a></li>              
              </ul>
              <!--Query menu here--> 
              <div class="hdnanssectn" id="hdnanssectn_<?php echo $index;?>">
                    <textarea id="txtquerymessage" name="area"></textarea>
                    <input name="archive" type="checkbox" value="" class="archiveans" />Archive
              <!--Hidden form menu here--> 
              <ul class="querymenu">
              	<li><a href="#" onclick="javascript:togglePopup('seekattr','<?php echo $message->qm_id ?>','<?php echo $form_id_single?>')">Seek Attributes</a></li>
                <li><a href="#" onclick="javascript:togglePopup('artclesuggn','<?php echo $message->qm_id ?>','<?php echo $form_id_single?>')">Article Suggestion</a></li>
                <li><a href="#" onclick="javascript:togglePopup('importquery','<?php echo $message->qm_id ?>','<?php echo $form_id_single?>')">Import</a></li>
                <li><?php echo form_submit("btnSubmit".$index,"Submit")?></li>
              </ul>
               <!--Hidden form menu here--> 
               </div>
			</div>
         <!--Query ends here-->
<?php echo form_close();?>
<script language="javascript">
$(document).ready(function(e){
	$('form[name="<?php echo $form_id_single?>"]').submit(function(event){
	   event.preventDefault();
	   var url = '<?php echo $form_url?>';
		// Get the form data.
		var $form_inputs = $('form[name="<?php echo $form_id_single?>"]').find(':input');
		var $form_textarea = $('form[name="<?php echo $form_id_single?>"]').find('textarea');
		var form_data = {};
		$form_inputs.each(function() 
		{
			form_data[this.name] = $(this).val();
		});
		$form_textarea.each(function() 
		{
			form_data[this.name] = $(this).val();
		});
	   $.ajax(
		{
			url: url,
			type: 'POST',
			data: form_data,
			success:function(data)
			{
				alert(data);
				var obj = jQuery.parseJSON(data);
				if(obj)
				{
					if(obj.fail)
					{
						alert(obj.fail);
					}
					else
					{
						alert(this);
					}
				}
			}
		});
	});
	$('input[type="submit"]').unbind().click(function() {
		$('input[name=hdn_action]').val(this.value);
	});
});
</script>        
<script language="javascript">
		$(document).ready(function(e){
		   $('.queryans').unbind().click(function() {
			   	var _id = this.id;
			  	index =  _id.split("_")[1];
		    	$('#hdnanssectn_'+index).slideToggle('fast', function() {
				 $('#' + _id).toggleClass('hightlight', $(this).is(':visible'));
			 
		  });
		});
	   $('.archiveans').unbind().click(function(){
	       if($('.archiveans').is(':checked')) {
		       var $p =$(this).closest('form'); 
	    	   togglePopup('archivequery',$('form[name="'+ $p.attr('name')+ '"] input[name=hdn_action]').val(),"'" + $p.attr('name') +"'");
	       } else {
	       }
	   });
		});
</script>
