<?php
/*
 * @todo :
 * List all the module  (on load)
 * on select on module List the related attributes in list box  (ajax)
 * on select of attributes the attribute will be shown with Remove button (jquery/javascript)
 * on submit the attribute list will be added to main array (jquery/javascript)
 * on delete it will be deleted from main array (jquery/javascript)
 */
?>
<!--seek attributes popup box-->
<div class="popupBox" id="seekattr">
<div class="boxheader">Seek Attributes 
	<span class="cancelbtn">
		<button onmousedown="togglePopup('seekattr')"></button>
	</span>
</div>

<div class="boxbody">
	<span>Select Module:</span> 
	<?php echo form_dropdown('popupattrmodules',$master['attr_modules'],'','class="cmpnyNames" id="popupattrmodules"');?>
	<br />
	<span>Select Attributes:</span> 
	<?php echo form_multiselect('popupattrnames',array(""=>"Select Attributes"),'','class="cmpnyNames" id="popupattrnames"');?>
	<span class="addcmpny"><a href="#" id="addattr" onclick="">Add Attribute</a></span>
	<ul class="cmpnylst" id="selected_attr_list">
	</ul>
	<input name="" type="button" id="btnseekattrsubmit" class="submitbtn" value="Submit" /></div>
	<?php echo form_hidden('hdn_popup_parent_id');?>
	<?php echo form_hidden('hdn_message_id');?>
	
</div>
<div class="boxfooter"></div>
</div>

<!--Seek attributes popup box-->
<script>
var arr = new Array;
$(document).ready(function(e){
	$('#addattr').click(function(event){
	  // $("#selected_attr_list").empty();
       $("#popupattrnames option").each ( function() {
    	   if(this.selected)
    	   {
    		   arr.push($(this).val() +  "|" + $(this).text());
        	   $("#selected_attr_list").append('<li id="seek_attr_'+ $(this).val() +'">' + $(this).text()  + '<span class="delbtn"><a href="#" id="seek_attr_href_'+ $(this).val() +'" class="removeAttr">Remove</a></span></li>');
    	   }
       });
	});
	$("#selected_attr_list").on("click",'.removeAttr',function(event){
			$(this).parent().parent().remove();
			//popup deleted one
	});
	$('#btnseekattrsubmit').click(function(event){
		var parentid = 	 $("#seekattr").find("input[name=hdn_popup_parent_id]").val();
		$("form[name=" + parentid + "] input[name=hdn_selected_attr_id]").val(arr);
		//clear arr
		arr = new Array;
		//clear list
		$("#selected_attr_list").empty();
		togglePopup('seekattr','','');
		});
});

function load_attr_data(input_data)
{
	$("#seekattr #selected_attr_list").empty();
	$('#seekattr #popupattrmodules').val('');
	$('#seekattr #popupattrnames').empty();
	arr = new Array;
	if(input_data != "")
	{
		var selected_items =  input_data.split(',');
		$.each(selected_items, function( key, value ) {
			arr.push(value);
			var item_arr =  value.split('|');
			$("#seekattr #selected_attr_list").append('<li id="seek_attr_'+ item_arr[0] +'">' + item_arr[1]  + '<span class="delbtn"><a href="#" id="seek_attr_href_'+ item_arr[0] +'" class="removeAttr">Remove</a></span></li>');	
		});
	}
}
</script>
