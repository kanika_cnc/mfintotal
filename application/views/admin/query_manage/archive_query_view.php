<!--archive popup box starts-->
    <div class="popupBox" id="archivequery">
    <div class="boxheader">Archive Query<span class="cancelbtn"><button onmousedown="togglePopup('archivequery')"></button></span></div>
    <div class="boxbody">
                <label>Title</label>                
                <input name="popuptxtarchiveTitle" id="popuptxtarchiveTitle" type="text" class="archquerytitle"/>
                <div class="clear"></div>
                <label>Category</label>   
                <?php echo form_dropdown('popuparchivecategory',$master['archive_cat'],'','class="cmpnyNames" id="popuparchivecategory"');?>             
                 <div class="clear"></div>
                 <?php echo form_hidden('hdn_popup_parent_id');?>
				<?php echo form_hidden('hdn_message_id');?>
                <input name="" type="submit" id="btnarchivemessagesubmit" class="addarchivebtn" value="Add" />
                <div class="clear"></div>
  </div>
  <div class="boxfooter"></div>
</div>
 <!--archive popup box ends-->
<script>
$(document).ready(function(e){
	$('#btnarchivemessagesubmit').click(function(event){
		var parentid = 	 $("#archivequery").find("input[name=hdn_popup_parent_id]").val();
		$("form[name=" + parentid + "] input[name=hdn_archive_details]").val($("#popuparchivecategory").val() + "|" + $("#popuptxtarchiveTitle").val());
		$("#popuptxtarchiveTitle").val('');
		$("#popuparchivecategory").val('');
		togglePopup('archivequery','','');
		});
});

function load_archive_popup_data()
{
	$("#popuptxtarchiveTitle").val('');
	$("#popuparchivecategory").val('');
	var parentid = 	 $("#archivequery").find("input[name=hdn_popup_parent_id]").val();
	input_data =  $("form[name=" + parentid + "] input[name=hdn_archive_details]").val();
	if(input_data != "")
	{
		var item_arr =  input_data.split('|');
		$("#popuptxtarchiveTitle").val(item_arr[1]);
		$("#popuparchivecategory").val(item_arr[0]);
	}
}
</script>