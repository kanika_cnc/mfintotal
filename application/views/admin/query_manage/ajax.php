<script language="javascript">
<?php
######################################################################################## 
// Query Manage : Seek Attributes
?> 
$(document).ready(function(e){
	$('form[name="transquery"]').submit(function(event){
	   event.preventDefault();
	   $form = $('form[name="transquery"]');
	   var url = $form.attr('action');
		// Get the form data.
		var $form_inputs = $('form[name="transquery"]').find(':input');
		var $form_textarea = $('form[name="transquery"]').find('textarea');
		var form_data = {};
		$form_inputs.each(function() 
		{
			form_data[this.name] = $(this).val();
		});
		$form_textarea.each(function() 
		{
			form_data[this.name] = $(this).val();
		});
	    $.ajax(
		{
			url: url,
			type: 'POST',
			data: form_data,
			success:function(data)
			{
				alert(data);
				togglePopup('transfer','','');
			}
		});
	});
});
</script>  
  
<?php
######################################################################################## 
// Query Manage : Seek Attributes
?>  
<script language="javascript">

$(document).ready(function(e){
	$('#popupattrmodules').change(function(event){
	   event.preventDefault();
	   var selected_id =  $(this).val();
	   $.ajax(
		{
			url: '<?php echo base_url()?>core/list_/get_attributes/' + selected_id ,
			type: 'POST',
			dataType: "json",
			success:function(data)
			{
				$('select#popupattrnames').empty();
	            var toAppend='';
	        	$.each(data, function(i, optionHtml){
	        		toAppend += '<option value ="' + i +'">'+optionHtml+'</option>';
	             });
	        	$('#popupattrnames').append(toAppend);
			}
		});
	});
});
</script>  
  
<?php
######################################################################################## 
// Query Manage : Import Archive
?>  
<script language="javascript">

$(document).ready(function(e){
	$('#popupimportcategory').change(function(event){
	   event.preventDefault();
	   var selected_id =  $(this).val();
	   $.ajax(
		{
			url: '<?php echo base_url()?>core/list_/get_archived_titles/' + selected_id ,
			type: 'POST',
			dataType: "json",
			success:function(data)
			{
				$('select#popupimporttitle').empty();
	            var toAppend='<option value ="">Please select</option>';
	        	$.each(data, function(i, optionHtml){
	        		toAppend += '<option value ="' + i +'">'+optionHtml+'</option>';
	             });
	        	$('#popupimporttitle').append(toAppend);
			}
		});
	});
	$('#popupimporttitle').change(function(event){
		   event.preventDefault();
		   var selected_id =  $(this).val();
		   $.ajax(
			{
				url: '<?php echo base_url()?>admin/query_management/get_archived_messages/' +  selected_id  ,
				type: 'POST',
				dataType: "json",
				success:function(data)
				{
					$.each(data, function(index,item) {
						json_imported_message_object = item;
						$("#importquery #txtArchivedMessageText").val(item.qa_message.qm_description); 
					});
				}
			});
		});
});
</script>    