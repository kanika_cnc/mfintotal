<!--import popup box starts-->
    <div class="popupBox" id="importquery">
    <div class="boxheader">Import Answers<span class="cancelbtn"><button onmousedown="togglePopup('importquery')"></button></span></div>
    <div class="boxbody">
                <label>Category</label>                
				<?php echo form_dropdown('popupimportcategory',$master['archive_cat'],'','class="cmpnyNames" id="popupimportcategory"');?>
                 <div class="clear"></div>
                  <label>Title</label>
                  <?php echo form_dropdown('popupimporttitle',array(""=>"Please Select"),'','class="cmpnyNames" id="popupimporttitle"');?>
                <div class="clear"></div> 
                <textarea  name="area"  id="txtArchivedMessageText"></textarea>
    			<div class="clear"></div> 
    			<?php echo form_hidden('hdn_popup_parent_id');?>
				<?php echo form_hidden('hdn_message_id');?>
                <input name="" type="button" id="btnaddimportbtn" class="addimportbtn" value="Add" />
                <div class="clear"></div>
  </div>
  <div class="boxfooter"></div>
</div>
 <!--import popup box ends-->
 
 <script>
 var json_imported_message_object = [];
$(document).ready(function(e){
	$('#btnaddimportbtn').click(function(event){
		var parentid = 	 $("#importquery").find("input[name=hdn_popup_parent_id]").val();
			item = json_imported_message_object;
			attrs = item.qa_message.qm_attributes;
			var attrs_string = "";
			$.each(attrs, function(index,attr_item) {
				if(attrs_string == "")
				{
					attrs_string = attr_item.qa_attr.mfa_id + "|" + attr_item.qa_attr.mfa_name;
				}
				else
				{
					attrs_string = attrs_string + "," + attr_item.qa_attr.mfa_id + "|" + attr_item.qa_attr.mfa_name;
				}
			});
			$("form[name=" + parentid + "] input[name=hdn_selected_attr_id]").val(attrs_string);
		
			articles = item.qa_message.qm_related_articles;
			var article_string = "";
			$.each(articles, function(index,attr_item) {
				if(article_string == "")
				{
					article_string = attr_item.mar_title + "|" + attr_item.mar_url;
				}
				else
				{
					article_string = article_string + "," + attr_item.mar_title + "|" + attr_item.mar_url;
				}
			});
			alert(article_string);
			$("form[name=" + parentid + "] input[name=hdn_selected_article_suggestion_id]").val(article_string);
			$("form[name=" + parentid + "] #txtquerymessage").val($("#importquery #txtArchivedMessageText").val());
		
		togglePopup('importquery','','');
		});
});
function load_import_message_popup()
{
	$("#importquery #popupimportcategory").val('');
	$("#importquery #popupimporttitle").empty();
	$('#txtArchivedMessageText').val('');
}
</script>
