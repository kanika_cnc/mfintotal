<?php include_once( APPPATH . 'views/admin/query_manage/ajax' . EXT );?>
<!--Tabs function--> 
 <script>
$(function() {
$( "#tabs" ).tabs();
});
</script>
<!--Tabs function--> 

<!--Tabs section starts-->
<div id="tabs">
	<!--Tabs menu-->
	<ul> 
		<li><a href="#tabs-1">Alert</a></li>
		<li><a href="<?php echo base_url()?>admin/query_management/unanswered_queries">Unanswered queries</a></li>
		<li><a href="<?php echo base_url()?>admin/query_management/answered_queries">Answered queries</a></li>
		<li><a href="#tabs-4">Mass message</a></li>
	</ul>
	<!--Tabs menu ends-->
	
	<div class="fltr"><a href="#">View all</a> / <a href="#">1-Jan-2012 to 1-Jan-2014</a></div>
    <!--Tabs section ends here-->
</div>

<!--  Load all popup views -->
<?php $this->load->view("admin/query_manage/archive_query_view")?>
<?php $this->load->view("admin/query_manage/article_sugesstion_view")?>
<?php $this->load->view("admin/query_manage/import_query_view")?>
<?php $this->load->view("admin/query_manage/seek_attributes_view")?>
<?php $this->load->view("admin/query_manage/transfer_query_view")?>

<!--Editor function-->
<script type="text/javascript" src="js/nicEdit.js"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas(); });
</script>
<!--Editor function-->

<!--Popup function--> 
<script type="text/javascript">
function togglePopup(box, msg_id,form_id){
	var popupBox = document.getElementById(box);
	if(popupBox.style.display == "block"){
		popupBox.style.display = "none";
	} else {
		popupBox.style.display = "block";
		$(popupBox).find("input[name=hdn_message_id]").val(msg_id);
		$(popupBox).find("input[name=hdn_popup_parent_id]").val(form_id);
		switch(box)
		{
			case 'seekattr':
				input_data = $("form[name=" + form_id + "] input[name=hdn_selected_attr_id]").val();
				load_attr_data(input_data);
			break;
			case 'artclesuggn':
				input_data = $("form[name=" + form_id + "] input[name=hdn_selected_article_suggestion_id]").val();
				load_articleSuggestion_data(input_data);
				break;
			case 'archivequery' :
				load_archive_popup_data();
				break;
			case 'importquery':
				load_import_message_popup();	
				break;
			case 'transfer':
				load_transfer_message_popup();
				break;	
		}
	}
	
}
</script>

 <!--Popup function--> 
<!--Date picker function-->  
<script>
  $(function() {
    $( "#datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true
    });
	var dateFormat = $( "#datepicker" ).datepicker( "option", "dateFormat" );
	// setter
	$( "#datepicker" ).datepicker( "option", "dateFormat", "dd-mm-yy" );
	var yearRange = $( "#datepicker" ).datepicker( "option", "yearRange" );
	// setter
	$( "#datepicker" ).datepicker( "option", "yearRange", "1920:2013" );
  });
  </script>
<!--Date picker function-->    
