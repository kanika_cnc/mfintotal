<!--login section starts-->
<div class="loginsect"><a href="<?php echo base_url()?>admin/profile"><?php echo $user->uacc_username;?></a> | <a href="<?php echo base_url()?>auth/acl/logout">Logout</a></div>
<!--login section ends-->
<!--menu starts-->
<div class="menu">
<ul>
	<li><a href="<?php echo base_url()?>admin/dashboard">Dashboard</a></li>
	<?php if($this->useracl->is_privileged('Manage Admin Users')) {?>
	<li  class="dropdwn"><a href="<?php echo base_url()?>admin/user_management" class="selected">Admin Users</a>
		<ul class="sub_menu">
			<li><a href="<?php echo base_url()?>admin/user_management/manage_groups">Manage Groups</a></li>
			<li><a href="<?php echo base_url()?>admin/user_management/manage_privilages">Manage Privileges</a></li>
			<li><a href="#">Manage group Privilages</a></li>
		</ul>
	</li>
	<?php }?>
	<?php if($this->useracl->is_privileged('Manage Attributes')) {?>
	<li class="dropdwn"><a href="#">Attributes</a>
		<ul class="sub_menu">
			<li><a href="attributes.html">Standard</a></li>
			<li><a href="">Query</a></li>
			<li><a href="#">EM</a></li>
			<li><a href="insurance.html">Insurance</a></li>
		</ul>
	</li>
	<?php }?>
	<?php if($this->useracl->is_privileged('Manage MIS')) {?>
	<li><a href="#">MIS</a></li>
	<?php }?>
	<?php if($this->useracl->is_privileged('Manage Clients')) {?>
	<li><a href="clients">Clients</a></li>
	<?php }?>
	<li class="dropdwn"><a href="#" class="last">Module</a>
	<ul class="sub_menu">
		<?php if($this->useracl->is_privileged('Manage Queries')) {?>
		<li><a href="<?php echo base_url()?>admin/query_management">Query</a></li>
		<?php }?>
		<?php if($this->useracl->is_privileged('Manage EM')) {?>
		<li><a href="#">EM</a></li>
		<?php }?>
		<?php if($this->useracl->is_privileged('Manage Insurance')) {?>
		<li><a href="#">Insurance</a></li>
		<?php }?>
	</ul>
	</li>
</ul>
</div>
<!--menu ends-->
