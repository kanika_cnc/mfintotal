<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once( APPPATH . 'models/user_model' . EXT );
include_once( APPPATH . 'models/entity/mfin_attributes_entity' . EXT );


/**
 * 
 * CRUD Class for Attributes
 * @author Fintotal
 *
 */
class Attribute_Model extends Base_Model
{
	protected $tbl_mfin_attributes ='mfin_attributes';
	
	const STD = 3;
	const QM = 4;
	const GEN = 7;
	const ExpM = 8;
	 
	public function __construct()
	{
		parent::__construct();
	}
	
	public function fetch($module_id ='')
	{
		if($module_id !== '')
		{
			$this->db->where('mfa_module_id',$module_id);			
		}
		$this->db->select($this->tbl_mfin_attributes.'.*');
		$this->db->order_by('mfa_name');
		$query = $this->db->get($this->tbl_mfin_attributes);
		return $query->result();
	}

	public function save_attribute_collection($user_id,$attribute_array)
	{
		$attributes_id = array();
		$attributes = new stdClass();
		$profile_array = array();
		foreach($attribute_array as $key=>$value)
		{
			$attributes_id[]  = $key;
		}
		
		//get attributes defintaions		
		if(!empty($attributes_id))
		{
			$this->db->where_in('mfa_id',$attributes_id);			
			$this->db->select($this->tbl_mfin_attributes.'.*');
			$this->db->order_by('mfa_name');
			$query = $this->db->get($this->tbl_mfin_attributes);
			$attributes =  $query->result();
		}
		//create array or object for relevant functions
		foreach ($attributes as $attr)
		{
			switch($attr->mfa_module_id){
				case Attribute_Model::GEN :
					 if(trim($attribute_array[$attr->mfa_id])!= "")
						$profile_array[$attr->mfa_stored_table_field] =$attribute_array[$attr->mfa_id];  
				break;
			}
		}
		if(!empty($profile_array))
		{
			$this->load->model('User_Model','user_model');
			return $this->user_model->update_user_profile($user_id, $profile_array);
		}
	}
}