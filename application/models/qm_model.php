<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once( APPPATH . 'models/base_model' . EXT );
include_once( APPPATH . 'models/article_repos_model' . EXT );
include_once( APPPATH . 'models/entity/qmessage_entity' . EXT );
include_once( APPPATH . 'models/entity/mfin_attributes_entity' . EXT );


/**
 * 
 * Enter description here ...
 * @author Fintotal
 *
 */
class QM_Model extends Base_Model
{
	private $tbl_query_message =  'query_message';
	private $tbl_query_attributes = 'query_attributes';
	protected $tbl_user_account = 'user_accounts';
	protected $tbl_mfin_attributes ='mfin_attributes';
	private $tbl_query_related_article = 'query_related_article';
	private $tbl_mfin_article_repository ='mfin_article_repository';
	private $tbl_query_archival ='query_archival';
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function insert_message($qm_entity)
	{
		$this->db->set('qm_type_id',$qm_entity->qm_type_id);
		$this->db->set('qm_status_id',$qm_entity->qm_status_id);
		$this->db->set('qm_description',$qm_entity->qm_description);
		$this->db->set('qm_has_attachment',$qm_entity->qm_has_attachment);
		$this->db->set('qm_uacct_id',$qm_entity->qm_uacct_id);
		$this->db->set('qm_sm_uacct_id',$qm_entity->qm_sm_uacct_id);
		$this->db->set('qm_created_by_id',$qm_entity->qm_created_by_id);
		$this->db->set('qm_approved_by_id',$qm_entity->qm_approved_by_id);
		$this->db->set('qm_created_date','now()',false);
		$this->db->set('qm_modified_date','now()',false);
		$this->db->set('qm_modified_by_id',$qm_entity->qm_modified_by_id);
		$this->db->set('qm_is_transferred',$qm_entity->qm_is_transferred);
		$this->db->set('qm_is_archived',$qm_entity->qm_is_archived);
		$id = $this->db->insert($this->tbl_query_message);
		$id = $this->db->insert_id();
		if($id > 0)
			return $id;
		return false;
		
	}
	
	public function update_message($qm_entity)
	{
		if(isset($qm_entity->qm_approved_by_id))
		{
			$this->db->set("qm_approved_by_id",$qm_entity->qm_approved_by_id);
			$this->db->set("qm_approved_date",'now()',false);
		}
		if(isset($qm_entity->qm_is_archived))
			$this->db->set("qm_is_archived",$qm_entity->qm_is_archived);
		if(isset($qm_entity->qm_description))
			$this->db->set("qm_description",$qm_entity->qm_description);
		if(isset($qm_entity->qm_is_transferred))
			$this->db->set("qm_is_transferred",$qm_entity->qm_is_transferred);
		if(isset($qm_entity->qm_sm_uacct_id))			
			$this->db->set("qm_sm_uacct_id",$qm_entity->qm_sm_uacct_id);
		if(isset($qm_entity->qm_status_id))	
			$this->db->set("qm_status_id",$qm_entity->qm_status_id);
		if(isset($qm_entity->qm_type_id))	
			$this->db->set("qm_type_id",$qm_entity->qm_type_id);
		$this->db->set("qm_modified_date",'now()',false);
		$this->db->set("qm_modified_by_id",$qm_entity->qm_modified_by_id);
		$this->db->where("qm_id",$qm_entity->qm_id)	;
		return $this->db->update($this->tbl_query_message);	
	}
	
	/**
	 * 
	 * Function returns all message for the given users
	 * @param int $user_id
	 * @param bool $approved  '' for all, true for only approved, false for not approved;
	 */
	public function get_user_messages($user_id,$approved=true)
	{
		if(!is_numeric($user_id))
			return false;
		$this->db->select($this->tbl_query_message.'.*');
		$this->db->select($this->tbl_user_account.'.uacc_username As qm_created_by_name');	
		$this->db->where('qm_uacct_id',$user_id);
		//check for not approved
		if($approved)
			$this->db->where ('(1= (case when qm_uacct_id <> qm_created_by_id and qm_approved_by_id is null then 0 else 1 end))');
		$this->db->join($this->tbl_user_account,'uacc_id=qm_created_by_id','left');
		$this->db->order_by('qm_modified_date','desc');
		$query = $this->db->get($this->tbl_query_message);
		return $query->result();
	}
	
	public function insert_message_attributes($qattributes)
	{
		if(!$this->db->insert_batch($this->tbl_query_attributes,$qattributes))
			return false;
		return true;	
	}
	public function get_user_message_attributes($user_id)
	{
		if(!is_numeric($user_id))
			return false;
		$this->db->select($this->tbl_query_attributes.'.*');
		$this->db->select($this->tbl_mfin_attributes.'.*');
		$this->db->where('qm_uacct_id',$user_id);
		$this->db->join($this->tbl_mfin_attributes,'qa_attr_id = mfa_id','left');
		$this->db->join($this->tbl_query_message,'qm_id = qa_qm_id','left');
		$query = $this->db->get($this->tbl_query_attributes);
		return $query->result();
	}
		
	public function get_user_messages_related_articles($user_id)
	{
		if(!is_numeric($user_id))
			return false;
		$this->db->select($this->tbl_query_related_article.'.*');
		$this->db->select($this->tbl_mfin_article_repository.'.*');
		$this->db->where('qm_uacct_id',$user_id);
		$this->db->join($this->tbl_query_message,'qm_id = qa_qm_id','left');
		$this->db->join($this->tbl_mfin_article_repository,'qa_article_id = mar_id','left');
		$query = $this->db->get($this->tbl_query_related_article);
		return $query->result();
	}
	
	public function get_message_by_id($message_id)
	{
		if(!is_numeric($message_id))
			return false;
		$this->db->select($this->tbl_query_message. '.*');
		$this->db->select($this->tbl_user_account.'.uacc_username As qm_created_by_name');	
		$this->db->where('qm_id',$message_id);
		$this->db->join($this->tbl_user_account,'uacc_id=qm_created_by_id','left');
		$query = $this->db->get($this->tbl_query_message);
		$count = $query->num_rows();
		$result = $query->result();
		if($count == 1)
			return $query->row();
		return false;
	}
	public function insert_message_articles($qrelatedarticle,$message_id)
	{
		debug($qrelatedarticle);
		if(!empty($qrelatedarticle))
		{
			$qarticles = array();
			$this->db->trans_start();
			foreach ($qrelatedarticle as $articletitle=>$articleurl)
			{
				//call the model to insert artle in repos
				$article_model = new Article_Repos_Model();
				$article_entity = new  Mfin_Article_Repos_Entity();
				$article_entity->mar_url = $articleurl;
				$article_entity->mar_title = $articletitle; 
				$id = $article_model->save_article($article_entity);
				$qarticles[]=array("qa_article_id"=>$id,"qa_qm_id"=>$message_id);
 			}
			//insert then prepera the array and insert it in database
			$this->db->insert_batch($this->tbl_query_related_article,$qarticles);
			$this->db->trans_complete();
			unset($article_entity);
			unset($article_model);
			if ($this->db->trans_status() === FALSE)
			{
		    	return false;
			}
		}
		return true;
	}
	
	public function get_message_articles($message_id)
	{
		//if(!is_numeric($message_id))
			//return false;
		$this->db->select($this->tbl_query_related_article.'.*');
		$this->db->select($this->tbl_mfin_article_repository.'.*');
		$this->db->where_in('qm_id',$message_id);
		$this->db->join($this->tbl_query_message,'qm_id = qa_qm_id','left');
		$this->db->join($this->tbl_mfin_article_repository,'qa_article_id = mar_id','left');
		$query = $this->db->get($this->tbl_query_related_article);
		return $query->result();
	}
	
	/**
	 * 
	 * Enter description here ...
	 * @param $message_id : id or array of ids
	 */
	public function get_message_attributes($message_id)
	{
		$this->db->select($this->tbl_query_attributes.'.*');
		$this->db->select($this->tbl_mfin_attributes.'.*');
		$this->db->where_in('qm_id',$message_id);
		$this->db->join($this->tbl_mfin_attributes,'qa_attr_id = mfa_id','left');
		$this->db->join($this->tbl_query_message,'qm_id = qa_qm_id','left');
		$query = $this->db->get($this->tbl_query_attributes);
		return $query->result();
	}
	
	public function fetch_messages($sm_id,$status_id)
	{
	}
	
	/**
	 * 
	 * Function returns the last message for each user assigned to this given sm
	 * and if the last message is from user then it will be returned in list.
	 * @param unknown_type $sm_id
	 */
	public function get_unanswered_messages($user_id,$user_type)
	{
		$this->db->select($this->tbl_query_message.'.*');
		$this->db->select($this->tbl_user_account.'.uacc_username As qm_uacct_name');
		$this->db->join($this->tbl_query_message,'latestmessage.qm_id = query_message.qm_id','left');
		$this->db->join($this->tbl_user_account,'uacc_id=qm_created_by_id','left');
    	$this->db->where('(query_message.qm_created_by_id = query_message.qm_uacct_id)');
    	$this->db->where('query_message.qm_status_id <> ', QStatus::IGNORED);
    	$this->db->order_by('qm_created_date');
    	$subquery = '(select max(qm_id) As qm_id from ' .$this->tbl_query_message;
    	switch($user_type)
    	{
    		case UserAcl::USERGROUP_SM :
    			$subquery = $subquery. ' where qm_sm_uacct_id  = '. $user_id;
    			break;
    		case UserAcl::USERGROUP_SMM :
    			$subquery = $subquery. ' inner join user_accounts on uacc_id = qm_sm_uacct_id ';
    			$subquery = $subquery. ' where uacc_manager_acc_id  = '. $user_id;
    			break;	
    		case UserAcl::USERGROUP_ADMIN :
    			break;		
    	}
    	$subquery = $subquery . ' group by qm_uacct_id) latestmessage';
    	$query = $this->db->get($subquery);
    	return $query->result();
	}
	
	/**
	 * 
	 * Function returns the last message for each user assigned to this given sm
	 * and if the last message is from user then it will be returned in list.
	 * @param unknown_type $sm_id
	 */
	public function get_unapproved_messages($user_id,$user_type)
	{
		$this->db->select('qm.*');
		$this->db->select('uacc.uacc_username As qm_uacct_name');
		$this->db->join($this->tbl_user_account . " As uacc" ,'uacc.uacc_id = qm.qm_uacct_id');
		$this->db->join($this->tbl_user_account . " As usm" ,'usm.uacc_id = qm.qm_sm_uacct_id');
		switch($user_type)
    	{
    		case UserAcl::USERGROUP_SM :
    			throw new Exception("You are not entitled to use this feature",9999);
    			break;
    		case UserAcl::USERGROUP_SMM :
    			$this->db->where('usm.uacc_manager_acc_id',$user_id);
    			break;	
    		case UserAcl::USERGROUP_ADMIN :
    			break;		
    	}
		$this->db->where('qm.qm_approved_by_id is null');
		$this->db->where('qm.qm_created_by_id <> qm.qm_uacct_id');
    	$this->db->order_by('qm.qm_created_date');
		$query = $this->db->get($this->tbl_query_message . " As qm");
    	return $query->result();
	}
	public function get_approved_messages($user_id,$user_type)
	{
		$this->db->select($this->tbl_query_message.'.*');
		$this->db->select($this->tbl_user_account.'.uacc_username As qm_uacct_name');
		$this->db->join($this->tbl_query_message,'latestmessage.qm_id = query_message.qm_id','left');
		$this->db->join($this->tbl_user_account,'uacc_id=qm_uacct_id','left');
    	$this->db->where('(query_message.qm_created_by_id = query_message.qm_sm_uacct_id)');
    	//$this->db->where('query_message.qm_status_id <> ', QStatus::IGNORED);
    	$this->db->order_by('qm_created_date');
    	$subquery = '(select max(qm_id) As qm_id from ' .$this->tbl_query_message;
    	switch($user_type)
    	{
    		case UserAcl::USERGROUP_SM :
    			throw new Exception("You are not entitled to use this feature",9999);
    			break;
    		case UserAcl::USERGROUP_SMM :
    			$subquery = $subquery. ' where qm_approved_by_id  = '. $user_id;
    			break;	
    		case UserAcl::USERGROUP_ADMIN :
    			break;		
    	}
    	$subquery = $subquery . ' group by qm_uacct_id ) latestmessage';
    	$query = $this->db->get($subquery);
    	return $query->result();
	}
	public function  get_answered_messages($user_id,$user_type)
	{
		$this->db->select($this->tbl_query_message.'.*');
		$this->db->select($this->tbl_user_account.'.uacc_username As qm_uacct_name');
		$this->db->join($this->tbl_query_message,'latestmessage.qm_id = query_message.qm_id','left');
		$this->db->join($this->tbl_user_account,'uacc_id=qm_uacct_id','left');
    	$this->db->where('(query_message.qm_created_by_id = query_message.qm_sm_uacct_id)');
    	//$this->db->where('query_message.qm_status_id <> ', QStatus::IGNORED);
    	$this->db->order_by('qm_created_date');
    	$subquery = '(select max(qm_id) As qm_id from ' .$this->tbl_query_message;
    	switch($user_type)
    	{
    		case UserAcl::USERGROUP_SM :
    			$subquery = $subquery. ' where qm_sm_uacct_id  = '. $user_id;
    			break;
    		case UserAcl::USERGROUP_SMM :
    			$subquery = $subquery. ' inner join user_accounts on uacc_id = qm_sm_uacct_id ';
    			$subquery = $subquery. ' where uacc_manager_acc_id  = '. $user_id;
    			break;	
    		case UserAcl::USERGROUP_ADMIN :
    			break;		
    	}
    	$subquery = $subquery . ' group by qm_uacct_id ) latestmessage';
    	$query = $this->db->get($subquery);
    	return $query->result();
	}
	
	public function get_archived_messages($category_id='',$title='',$message_id='')
	{
		if(is_numeric($category_id))
			$this->db->where('qa_category_id',$category_id);
		if(!empty($title))
			$this->db->where('qa_title',$title);
		if(is_numeric($message_id))
			$this->db->where('qa_qm_id',$message_id);
		 $query = $this->db->get($this->tbl_query_archival);
		 return $query->result();
		  
	}
	public function archive_message($title,$category,$message_id,$logged_user_id)
	{
			$this->db->trans_start();
			$this->db->set("qa_title",$title);
			$this->db->set("qa_category_id",$category);
			$this->db->set("qa_archived_by",$logged_user_id);
			$this->db->set("qa_archived_date",'now()',false);
			$this->db->set("qa_qm_id",$message_id);
			$this->db->insert($this->tbl_query_archival);

			//update flag
			$entity =  new stdClass();
			$entity->qm_is_archived = 1;
			$entity->qm_id =  $message_id;
			$entity->qm_modified_by_id =  $logged_user_id;
			$this->update_message($entity);
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE)
			{
		    	return false;
			}
		return true;	
	}
	
	public function get_archived_messages_title_list($category_id ,$unique=false)
	{
		if(is_numeric($category_id))
			$this->db->where('qa_category_id',$category_id);
		if($unique)
			$this->db->distinct();
		$this->db->select('qa_title');
		$this->db->select('qa_qm_id');		
		$query = $this->db->get($this->tbl_query_archival);
		return $query->result();
	}
}	

