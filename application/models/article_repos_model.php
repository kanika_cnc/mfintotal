<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once( APPPATH . 'models/entity/mfin_article_repos_entity' . EXT );

class Article_Repos_Model extends Base_Model
{
	public $tbl_mfin_article_repository ='mfin_article_repository';
	public function __construct()
	{
		parent::__construct();
	}
	
	public function save_article($mfin_article_repos_entity)
	{
		if(is_object($mfin_article_repos_entity))
		{
			$this->db->set("mar_text",$mfin_article_repos_entity->mar_text);
			$this->db->set("mar_url",$mfin_article_repos_entity->mar_url);
			$this->db->set("mar_title",$mfin_article_repos_entity->mar_title);  
			if(!$this->db->insert($this->tbl_mfin_article_repository))
				return false;
			return $this->db->insert_id();
		}
	}
	
	public function fetch()
	{
		$this->db->select($this->tbl_mfin_article_repository.'.*');
		$this->db->order_by('mar_id');
		$query = $this->db->get($this->tbl_mfin_article_repository);
		return $query->result();
	}
}