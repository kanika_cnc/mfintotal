<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once( APPPATH . 'models/base_model' . EXT );

class List_Model extends Base_Model
{
	private $tbl_list_item ="list_item";
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_list_items_by_list_type_name($list_type='')
	{
		if($list_type == '')
			return false;
		return true;	
	}
	/**
	 * 
	 * Method to return the List items for the given listid
	 * @param unknown_type $list_type_id
	 */
	public function get_list_items_by_list_type_id($list_type_id)
	{
		if(!is_numeric($list_type_id))
			return false;
		
		$this->db->where('li_lt_id',$list_type_id);			
		$this->db->select($this->tbl_list_item.'.*');
		$this->db->order_by('li_sort_order');
		$query = $this->db->get($this->tbl_list_item);
		return $query->result();	
	}
} 