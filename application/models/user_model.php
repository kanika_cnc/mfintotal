<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once( APPPATH . 'models/base_model' . EXT );
/**
 * 
 * USER MANAGEMENT / CRUD METHODS
 * @author Fintotal
 *
 */
class User_Model extends Base_Model
{
	protected $tbl_user_account = 'user_accounts'; 
	protected $tbl_user_profile = 'user_profile';
	protected $static_salt = 'change-me!'	;
	
	/**
	 * 
	 * Function to return the user data
	 * @param unknown_type $user_id
	 * @param bool $is_profile : False : will not return the user profile data
	 */
	public function get_user_by_user_id($user_id,$is_profile=true)
	{
		$this->db->where('users.uacc_id',$user_id);
		$this->db->select('users.*');
		$this->db->select('map.uacc_username AS uacc_manager_name');
		$this->db->select('user_groups.*');
		$this->db->join('user_groups','ugrp_id = users.uacc_group_fk','left');
		$this->db->join($this->tbl_user_account .' As map','map.uacc_id = users.uacc_manager_acc_id','left');
		if($is_profile)
		{
			$this->db->select('user_profile.*');
			$this->db->join('user_profile','profile_uacc_id_fk = users.uacc_id','left');
		}
		$query = $this->db->get($this->tbl_user_account . ' As users');
	    return (object) $query->row();
	}
	public function get_user_by_email($email)
	{
		$this->db->where('users.uacc_email',$email);
		$this->db->select('users.*');
		$this->db->select('map.uacc_username AS uacc_manager_name');
		$this->db->select('user_groups.*');
		$this->db->join('user_groups','ugrp_id = users.uacc_group_fk','left');
		$this->db->join($this->tbl_user_account .' As map','map.uacc_id = users.uacc_manager_acc_id','left');
		$query = $this->db->get($this->tbl_user_account . ' As users');
	    return (object) $query->row();
	}
	
	public function get_users_list_array_by_group_id($group_id)
	{
		$this->db->select('uacc_id');
		$this->db->select('uacc_username');
		$this->db->where('uacc_group_fk',$group_id);
		$query = $this->db->get($this->tbl_user_account);
	    $list_item  =  $query->result();
	    $strSelect = "Please select";
	    switch ($group_id)
	    {
	    	case UserAcl::USERGROUP_ADMIN : 
	    		$strSelect = "Select Admin";
	    		break;
	    	case UserAcl::USERGROUP_SMM : 
	    		$strSelect = "Select SMM";
	    		break;
	    	case UserAcl::USERGROUP_SM  : 
	    		$strSelect ="Select SM";
	    		break;		
	    }
	    $data =  array(""=>$strSelect);
	    foreach ($list_item as $item)
	    {
	    	$data[$item->uacc_id] = $item->uacc_username;
	    }
	    return $data;
	    
	}
	/**
	 * 
	 * Return the list of all users
	 * @param $group_id int, array of int: returns the users of that group
	 */
	public function get_users($admin_group=-1,$group_id='')
	{
		$this->db->select('users.*');
		$this->db->select('map.uacc_username AS uacc_manager_name');
		$this->db->select('user_groups.*');
		$this->db->join('user_groups','ugrp_id = users.uacc_group_fk','left');
		$this->db->join($this->tbl_user_account .' As map','map.uacc_id = users.uacc_manager_acc_id','left');
		if($group_id != '')
		{
			$this->db->where_in('users.uacc_group_fk',$group_id);
		}
		if($admin_group != -1)
		{
			$this->db->where('ugrp_admin',$admin_group);
		}
		$query = $this->db->get($this->tbl_user_account . ' As users');
		return $query->result();
	    // Left Join user group table to user account table.
	    /*$this->db->join(
			$this->auth->tbl_user_group, 
			$this->auth->tbl_col_user_account['group_id'].' = '.$this->auth->tbl_join_user_group, 'left'
		);

		// Left Join user custom data table(s) to user account table.
		foreach ($this->auth->tbl_custom_data as $table)
		{
			$this->db->join($table['table'], $this->auth->tbl_join_user_account.' = '.$table['join'], 'left');
		}

		// Group by users id to prevent multiple custom data rows to be listed per user.
		if ($sql_group_by === TRUE)
		{
			$this->db->group_by($this->auth->tbl_col_user_account['id']);
		}
		// Else, if a specific column is defined, group by that column.
		else if ($sql_group_by)
		{
			$this->db->group_by($sql_group_by);
		}
		
		// Set any custom defined SQL statements.
		$this->set_custom_sql_to_db($sql_select, $sql_where);

		return $this->db->get($this->auth->tbl_user_account);*/
	}
	
		
	/**
	 * insert_user
	 * Inserts user account and profile data, returning the users new id.
	 *
	 * @return bool
	 */
	public function insert_user($email, $username, $password,$manager_id, $profile_data = FALSE, $group_id = FALSE)
	{
		// Check that an email address and password have been set.
		// If a username is defined as an identity column, ensure it is also set.
		if (empty($email) || empty($password))
		{
			$this->set_error_message('account_creation_insufficient_data', 'config');
			return FALSE;
		}
				
		// Check email is unique.
		if (!$this->email_available($email))
		{
			$this->set_error_message('account_creation_duplicate_email', 'config');
			return FALSE;
		}
		// Get group ID if it was passed in additional data array.
	    if (!is_numeric($group_id))
	    {
			$group_id = 0;
	    }

	    $ip_address = $this->input->ip_address();
	    $database_salt = $this->generate_token(10);
		$hash_password = $this->generate_hash_token($password, $database_salt, TRUE);
		$activation_token = sha1($this->generate_token(20));
		$suspend_account = false;
		
		###+++++++++++++++++++++++++++++++++###

		// Start SQL transaction.
		$this->db->trans_start();
		
		// Main user account table.	
	    $sql_insert = array(
			"uacc_group_fk" => $group_id,
			"uacc_email" => $email,
			"uacc_username" => ($username) ? $username : '',
			"uacc_password" => $hash_password,
			"uacc_ip_address" => $ip_address,
			"uacc_date_last_login" => $this->database_date_time(),
			"uacc_date_added" => $this->database_date_time(),
			"uacc_activation_token" => $activation_token,
			"uacc_active" => 0,		
			"uacc_suspend" => $suspend_account,		
	    	"uacc_salt" => $database_salt,
	    	"uacc_manager_acc_id" => $manager_id
		);
	    // Create main user account.
		$this->db->insert($this->tbl_user_account, $sql_insert);
		
		###+++++++++++++++++++++++++++++++++###

		// Custom user data table(s).
		// Get newly created User Account id for join with custom user data table(s).
	    $user_id = $this->db->insert_id();
		if(is_array($profile_data))
		{
	    	$this->update_user_profile($user_id,$profile_data);
		}
		// Complete SQL transaction.
		$this->db->trans_complete();
	    return is_numeric($user_id) ? $user_id : FALSE;
	}


	/**
	 * update_user
	 * Updates the main user account table and any linked custom user tables.
	 *
	 * @return bool
	 * @author Rob Hussey
	 * @author Phil Sturgeon
	 */
	public function update_user($user_id, $user_data)
	{
		if (!is_array($user_data))
		{
			return FALSE;
		}
		
		if (isset($user_data['uacc_email']))
		{
			// Get identity value from $user_data and check if identity is available.
			if (!$this->email_available($user_data['uacc_email'], $user_id))
			{
				// Identity already exists
				// Note: If using an identity other than email or username, this needs to be added to the language file.
				$this->set_error_message('account_creation_duplicate_email', 'config');
				return FALSE;
			}
			//$this->update_session_identifier($user_id, $user_data['uacc_email']);
		}
		 // Start SQL transaction.
		$this->db->trans_start();
		$user = $this->get_user_by_user_id($user_id);
		
		
		###+++++++++++++++++++++++++++++++++###
		if (isset($user_data['uacc_email']))
			$this->db->set('uacc_email',$user_data['uacc_email']);
		
		if (isset($user_data['uacc_password']))
		{
			$password = $user_data['uacc_password'];
			$database_salt = $user->uacc_salt;
			$hash_password = $this->generate_hash_token($password, $database_salt, TRUE);
			$this->db->set('uacc_password',$hash_password);
		}
		$this->db->set('uacc_username',$user_data['uacc_username']);
		if (isset($user_data['uacc_group_fk']))
			$this->db->set('uacc_group_fk',$user_data['uacc_group_fk']);
		if (isset($user_data['uacc_manager_acc_id']))	
			$this->db->set('uacc_manager_acc_id',$user_data['uacc_manager_acc_id']);
		$this->db->where('uacc_id',$user_id);
		$this->db->update($this->tbl_user_account);
		
		
		//insert/update data in profile table
		###+++++++++++++++++++++++++++++++++###
		$this->update_user_profile($user_id,$user_data);
		
		// Complete SQL transaction.
		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}


	/**
	 * delete_user
	 * Deletes a user account and all linked custom user tables from the database.
	 *
	 * @return bool
	 * @author Rob Hussey
	 * @author Phil Sturgeon
	 */
	public function delete_user($user_id)
	{
		// Start SQL transaction.
		$this->db->trans_start();

		// Delete any user 'Remember me' sessions.
		//@todo: later
		//$this->delete_database_login_session($user_id);

		// Delete user data.
		$this->db->delete($this->tbl_user_account, array("uacc_id" => $user_id));
		
		// Delete user privilege data.
		//$this->db->delete($this->auth->tbl_user_privilege_users, array($this->auth->tbl_col_user_privilege_users['user_id'] => $user_id));

		// Complete SQL transaction
		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}

	/**
	 * 
	 * @param unknown_type $user_id
	 * @param unknown_type $user_profile_entity
	 */
	public function update_user_profile($user_id,$user_profile_entity)
	{
		//check if profile exists
		$this->db->where('profile_uacc_id_fk',$user_id);
		$query = $this->db->get($this->tbl_user_profile);
		$profile = $query->row();
		
		$this->db->set('profile_uacc_id_fk',$user_id);
		if (isset($user_profile_entity['profile_dob']))	
			$this->db->set('profile_dob',$this->database_date_time(0,$user_profile_entity['profile_dob']));
		if (isset($user_profile_entity['profile_location']))	
			$this->db->set('profile_location',$user_profile_entity['profile_location']);
		if (isset($user_profile_entity['profile_gender']))	
			$this->db->set('profile_gender',$user_profile_entity['profile_gender']);
		if (isset($user_profile_entity['profile_photo']))	
			$this->db->set('profile_photo',$user_profile_entity['profile_photo']);
		if (isset($user_profile_entity['profile_age']))	
			$this->db->set('profile_age',$user_profile_entity['profile_age']);
		if (isset($user_profile_entity['profile_life_stage']))	
			$this->db->set('profile_life_stage',$user_profile_entity['profile_life_stage']);
		if (isset($user_profile_entity['profile_monthly_income']))	
			$this->db->set('profile_monthly_income',$user_profile_entity['profile_monthly_income']);
		if (isset($user_profile_entity['profile_home_stage']))	
			$this->db->set('profile_home_stage',$user_profile_entity['profile_home_stage']);
		if (isset($user_profile_entity['profile_has_loans']))	
			$this->db->set('profile_has_loans',$user_profile_entity['profile_has_loans']);
		if(is_object($profile))
		{
			
			$this->db->where('profile_uacc_id_fk',$user_id);
			if(!$this->db->update($this->tbl_user_profile))
			{
				$this->set_error_message('account_creation_duplicate_email', 'config');
			}
		}
		else
		{
			if(!$this->db->insert($this->tbl_user_profile))
				$this->set_error_message('account_creation_duplicate_email', 'config');
		}
	}
	
	
	
	/**
		/**
	 * get_primary_identity
	 * Looks-up database identity columns and return users primary identifier.
	 *
	 * @return string
	 * @author fintotal
	 */
	public function get_primary_identity($identity)
	{
		
		if (empty($identity) || !is_string($identity))
		{
			return FALSE;
		}
		$this->db->where('uacc_email',$identity);	
		$query = $this->db->select('uacc_email')
			->get($this->tbl_user_account);
		
		// Return users primary identity.
		if ($query->num_rows() == 1)
		{
			return $query->row()->uacc_email;
		}
		return FALSE;
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	

	/**
	 * search_users
	 * Compare a search query against user columns defined in the config file.
	 *
	 * @return object
	 * @author Rob Hussey
	 */
	public function search_users($search_query = FALSE, $exact_match = FALSE, $sql_select = FALSE, $sql_where = FALSE, $sql_group_by = TRUE)
	{
		if (!empty($search_query))
		{
			// Get user table columns to be searched, set via config file.
			$user_cols = $this->auth->db_settings['search_user_cols'];

			// Create a concatenated string of user columns to search against.
			$concat_cols = "(CONCAT_WS(' '";
			foreach ($user_cols as $column)
			{
				$concat_cols .= ', '.$column;
			}

			// Convert search query to array if it isn't already.
			$query_terms = (is_array($search_query)) ? $search_query : explode(' ', $search_query);	
			
			// Define whether to use 'AND' or 'OR' condition.
			$sql_condition = ($exact_match) ? ' AND ' : ' OR ';
			
			// Create array of user column data and each search query term.
			$i = 0;
			$sql_like = "(";
			foreach ($query_terms as $term)
			{
				$sql_like .= $concat_cols.", ".$i++.") LIKE '%".$this->db->escape_like_str($term)."%')".$sql_condition;
			}
			$sql_like = rtrim($sql_like, $sql_condition).")";
			
			// Set SQL WHERE LIKE statement to be passed to get_users() function.
			$this->db->where($sql_like, NULL, FALSE);
		}
		
		return $this->get_users($sql_select, $sql_where, $sql_group_by);
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	

	/**
	 * get_unactivated_users
	 * Cleanup function to get unactivated users from database within expiry period.
	 * These can then be deleted using the library delete_unactivated_users() function.
	 *
	 * @return bool
	 * @author Rob Hussey
	 */
	public function get_unactivated_users($inactive_days = 28, $sql_select = FALSE, $sql_where = FALSE)
	{
		if (!is_numeric($inactive_days))
		{
			return FALSE;
		}

		// SQL SELECT columns.
		if (!empty($sql_select))
		{
			$this->db->select($sql_select);
		}
		
		// SQL WHERE columns.
		if (!empty($sql_where))
		{
			$this->db->where($sql_where);
		}
		
		// Do not delete accounts added within set $inactive_days.
		$inactive_days = (60 * 60 * $inactive_days);
		$expire_date = $this->database_date_time(-$inactive_days);
		
		$sql_where = array(
			$this->auth->tbl_col_user_account['active'] => 0,
			$this->auth->tbl_col_user_account['date_added'].' < ' => $expire_date
		);
				
		$this->db->where($sql_where);
		
		return $this->get_users($sql_select);
	}
	
	/**
	 * email_available
	 * Check email does not exist in database.
	 * NOTE: This should not be used if the email field is defined in the 'identity_cols' set via the config file.
	 * In this case, use the identity_available() function instead.
	 *
	 * @return bool
	 * @author Rob Hussey
	 */
	public function email_available($email = FALSE, $user_id = FALSE)
	{
	    if (empty($email))
	    {
			return FALSE;
	    }
	    // If $user_id is set, remove user from query so their current email is not found during the duplicate email check.
		if (is_numeric($user_id))
		{
			$this->db->where('uacc_id != ',$user_id);
		}
		$this->db->where('uacc_email', $email);
		$this->db->from($this->tbl_user_account);
		return $this->db->count_all_results() == 0;
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// USER TASK METHODS
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	
	/**
	 * activate_user
	 * Activates a users account allowing them to login to their account. 
	 * If $verify_token = TRUE, a valid $activation_token must also be submitted.
	 *
	 * @return void
	 * @author Rob Hussey
	 * @author Mathew Davies
	 */
	public function activate_user($user_id, $activation_token = FALSE, $verify_token = TRUE, $clear_token = TRUE)
	{
		if ($activation_token)
		{
			// Confirm activation token is 40 characters long (length of sha1).
			if ($verify_token && strlen($activation_token) != 40)
			{
				return FALSE;
			}
			// Verify that $activation_token matches database record.
			else if ($verify_token && strlen($activation_token) == 40)
			{
				$sql_where = array(
					"uacc_id" => $user_id,
					"uacc_activation_token" => $activation_token
				);

				$query = $this->db->where($sql_where)
					->get($this->tbl_user_account);

				if ($query->num_rows() !== 1)
				{
					return FALSE;
				}
			}
		}
		if ($clear_token)
		{
			$sql_update['uacc_activation_token'] = '';
		}
		$sql_update['uacc_active'] = 1;

		$this->db->update($this->tbl_user_account, $sql_update, array('uacc_id' => $user_id));
							
	    return $this->db->affected_rows() == 1;
	}
	
	/**
	 * deactivate_user
	 * Deactivates a users account, preventing them from logging in.
	 *
	 * @return void
	 * @author Rob Hussey
	 * @author Mathew Davies
	 */
	public function deactivate_user($user_id)
	{
	    if (empty($user_id))
	    {
			return FALSE;
	    }
	
	    $activation_token = sha1($this->generate_token(20));

	    $sql_update = array(
			'uacc_activation_token'=> $activation_token,
			'uacc_active' => 0
	    );

		$this->db->update($this->tbl_user_account, $sql_update, array('uacc_id' => $user_id));

	    return $this->db->affected_rows() == 1;
	}
	
	/**
	 * change_password
	 * Validates a submitted 'Current' password against the database, if valid, the database is updated with the 'New' password. 
	 *
	 * @return bool
	 * @author Rob Hussey
	 */
	public function change_password($identity, $current_password, $new_password)
	{		
		// Verify current password matches
	    if ($this->verify_password($identity, $current_password))
	    {
			// Remove 'Remember me' database sessions so all remembered instances have to re-login, whilst maintaining current login session
			//@todo with session
			//$user_id = $this->auth->session_data[$this->auth->session_name['user_id']];
			
			/*if ($session_token = $this->auth->session_data[$this->auth->session_name['login_session_token']])
			{
				$this->db->where($this->auth->tbl_col_user_session['token'].' != ', $session_token);
			}
			$this->db->where($this->auth->tbl_col_user_session['user_id'],$user_id);			
			$this->db->delete($this->auth->tbl_user_session);
			*/
			// Get users salt.
			$sql_select = 'uacc_salt';

			$sql_where = array(
				'uacc_email' => $identity
			);
		
			$user = $this->get_user_by_email($identity);
			
			// Create hash of password and store.
			$hash_new_password = $this->generate_hash_token($new_password, $user->uacc_salt, TRUE);
			
			$sql_update['uacc_password'] = $hash_new_password;

			$sql_where[uacc_email] = $identity;

			$this->db->update($this->tbl_user_account, $sql_update, $sql_where);

			return $this->db->affected_rows() == 1;
	    }

	    return FALSE;
	}
	/**
	 * verify_password
	 * Verify that a submitted password matches a user database record.
	 *
	 * @return bool
	 * @author Rob Hussey
	 */
	public function verify_password($identity, $verify_password)
	{
		
		if (empty($identity) || empty($verify_password))
		{
			return FALSE;
		}
				
		$sql_select = array(
			'uacc_password',
			'uacc_salt'
		);
		
		$query = $this->db->select($sql_select)
			->where('uacc_email', $identity)
			->limit(1)
			->get($this->tbl_user_account);
				 
	    $result = $query->row();
		
	    if ($query->num_rows() !== 1)
	    {
			return FALSE;
	    }
				
		$database_password = $result->uacc_password;
		$database_salt = $result->uacc_salt;
		$static_salt = $this->static_salt;
		
		require_once(APPPATH.'libraries/phpass/PasswordHash.php');				
		$hash_token = new PasswordHash(8, FALSE);
		return $hash_token->CheckPassword($database_salt . $verify_password . $static_salt, $database_password);
	}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// TOKEN GENERATION
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	
	/**
	 * generate_token
	 * Generates a random unhashed password / token / salt.
	 * Includes a safe guard to ensure vowels are removed to avoid offensive words when used for password generation.
	 * Additionally, 0, 1 removed to avoid confusion with o, i, l.
	 *
	 * @return string
	 */
	public function generate_token($length = 8) 
	{
		$characters = '23456789BbCcDdFfGgHhJjKkMmNnPpQqRrSsTtVvWwXxYyZz';
		$count = mb_strlen($characters);

		for ($i = 0, $token = ''; $i < $length; $i++) 
		{
			$index = rand(0, $count - 1);
			$token .= mb_substr($characters, $index, 1);
		}
		return $token;
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	
	/**
	 * generate_hash_token
	 * Generates a new hashed password / token.
	 *
	 * @return string
	 * @author Rob Hussey
	 */
	public function generate_hash_token($token, $database_salt = FALSE, $is_password = FALSE)
	{
	    if (empty($token))
	    {
	    	return FALSE;
	    }
		
		// Get static salt if set via config file.
		$static_salt = 'change-me!';
		
		if ($is_password)
		{
			require_once(APPPATH.'libraries/phpass/PasswordHash.php');				
			$phpass = new PasswordHash(8, FALSE);
			
			return $phpass->HashPassword($database_salt . $token . $static_salt);
		}
		else
		{
			return sha1($database_salt . $token . $static_salt);
		}
	}
	
	public function get_next_manager()
	{
		$this->db->select('manager.uacc_id As manager_id');
		$this->db->select('count(users.uacc_id) As user_count');
		$this->db->join('user_accounts As users','manager.uacc_id = users.uacc_manager_acc_id and users.uacc_manager_acc_id is not null ','left');
		$this->db->join('user_groups','user_groups.ugrp_id =  users.uacc_group_fk and user_groups.ugrp_admin = 0 ','left');
		 $this->db->where('manager.uacc_group_fk', 3); // 0 not admin
		$this->db->group_by('manager.uacc_id');
		$this->db->order_by('user_count','asc');
		$query = $this->db->get($this->tbl_user_account .' As manager',1);
		if(count($query->result()) > 0)
		{
			foreach ($query->result() as $row)
				return $row->manager_id;
		}
		return false;
	}
}