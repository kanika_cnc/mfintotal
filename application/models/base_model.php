<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Base_Model extends CI_Model
{
	public $model_state;
	
	public function __construct()
	{
		parent::__construct();
		$this->model_state = new stdClass;
		$this->load->config('flexi_auth', TRUE);
		// Status and error messages.
		$this->model_state->message_settings = $this->config->item('messages', 'flexi_auth');
		$this->model_state->status_messages = array('public' => array(), 'admin' => array());
		$this->model_state->error_messages = array('public' => array(), 'admin' => array());
		$this->lang->load('fintotal');
		
	}
	public function objectToObject($instance, $className) {
	    return unserialize(sprintf(
	        'O:%d:"%s"%s',
	        strlen($className),
	        $className,
	        strstr(strstr(serialize($instance), '"'), ':')
	    ));
	}
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// MESSAGES AND ERRORS
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	
	/**
	 * set_message
	 * Set a status or error message to be displayed.
	 *
	 * @return void
	 * @author Rob Hussey
	 */
	private function set_message($message_type = FALSE, $message = FALSE, $target_user = 'public', $overwrite_existing = FALSE)
	{
		if (in_array($message_type, array('status', 'error')) && $message)
		{
			// Convert the target user to lowercase to ensure whether comparison values are matched. 
			$target_user = strtolower($target_user);
			// Check whether to use the target user set via the config file.
			if ($target_user === 'config' && isset($this->model_state->message_settings['target_user'][$message]))
			{
				$target_user = $this->model_state->message_settings['target_user'][$message];
			}
			// If $target_user exactly equals TRUE, set the target user as public.
			$target_user = ($target_user === TRUE) ? 'public' : $target_user;

			// Check whether a message should be set, if FALSE is defined, do not set the message.
			if (in_array($target_user, array('public', 'admin')))
			{
				$message_alias = ($message_type == 'status') ? 'status_messages' : 'error_messages';
				
				// Check whether to overwrite existing messages.
				if ($overwrite_existing)
				{
					$this->model_state->{$message_alias} = array('public' => array(), 'admin' => array());
				}

				// Check message is not already in array to avoid displaying duplicates.
				if (! in_array($message, $this->model_state->{$message_alias}[$target_user]))
				{
					$this->model_state->{$message_alias}[$target_user][] = $message;
				}
			}
		}
			
		return $message;
	}

	/**
	 * set_status_message
	 * Set a status message to be displayed.
	 *
	 * @return void
	 * @author Rob Hussey
	 */
	public function set_status_message($status_message = FALSE, $target_user = 'public', $overwrite_existing = FALSE)
	{
		return $this->set_message('status', $status_message, $target_user, $overwrite_existing);
	}

	/**
	 * set_error_message
	 * Set an error message to be displayed.
	 *
	 * @return void
	 * @author Rob Hussey
	 */
	public function set_error_message($error_message = FALSE, $target_user = 'public', $overwrite_existing = FALSE)
	{
		return $this->set_message('error', $error_message, $target_user, $overwrite_existing);
	}

	###+++++++++++++++++++++++++++++++++###

	/**
	 * get_messages
	 * Get any status or error message(s) that may have been set by recently run functions. 
	 */
	public function get_messages($message_type = FALSE, $target_user = 'public', $prefix_delimiter = FALSE, $suffix_delimiter = FALSE)
	{	
		if (in_array($message_type, array('status', 'error')))
		{
			// If $target_user exactly equals TRUE, set the target user as public.
			$target_user = ($target_user === TRUE) ? 'public' : $target_user;

			// Convert the target user to lowercase to ensure whether comparison values are matched. 
			$target_user = strtolower($target_user);

			// Set message delimiters, by checking they do not exactly equal FALSE, we can allow NULL or empty '' delimiter values. 
			if (! $prefix_delimiter)
			{
				$prefix_delimiter = ($message_type == 'status') ? '<p class="status_msg">' :'<p class="error_msg">';
			}
			if (! $suffix_delimiter)
			{
				$suffix_delimiter = '</p>';
			}
			
			$message_alias = ($message_type == 'status') ? 'status_messages' : 'error_messages';

			// Get all messages for public users, or both public AND admin users.
			if ($target_user === 'public')
			{
				$messages = $this->model_state->{$message_alias}['public'];
			}
			else
			{
				$messages = array_merge($this->model_state->{$message_alias}['public'], $this->model_state->{$message_alias}['admin']);
			}
			
			$statuses = FALSE;
			foreach ($messages as $message)
			{
				$message = ($this->lang->line($message)) ? $this->lang->line($message) : $message;
				$statuses .= $prefix_delimiter . $message . $suffix_delimiter;
			}
			return $statuses;
		}

		return FALSE;
	}

	/**
	 * status_messages
	 * Get any status message(s) that may have been set by recently run functions.
	 *
	 * @return void
	 * @author Rob Hussey
	 */
	public function status_messages($target_user = 'public', $prefix_delimiter = FALSE, $suffix_delimiter = FALSE)
	{
		return $this->get_messages('status', $target_user, $prefix_delimiter, $suffix_delimiter);
	}

	/**
	 * error_messages
	 * Get any error message(s) that may have been set by recently run functions.
	 *
	 * @return void
	 * @author Rob Hussey
	 */
	public function error_messages($target_user = 'public', $prefix_delimiter = FALSE, $suffix_delimiter = FALSE)
	{
		return $this->get_messages('error', $target_user, $prefix_delimiter, $suffix_delimiter);
	}	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// ACTIVE RECORD FUNCTIONS
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	
	/**
	 * set_sql_to_var
	 * Used internally by flexi auth to set an SQL statement to CI's Active Record.
	 *
	 * @return null
	 * @author Rob Hussey
	 */
	public function set_sql_to_var($sql_clause, $key = FALSE, $value = FALSE, $param = FALSE, $overwrite_existing = FALSE)
	{
		// Validate submitted data.
		if ($key === FALSE || (! is_array($key) && in_array($key, array('select', 'order_by', 'group_by', 'limit')) && $value === FALSE))
		{
			return FALSE;
		}
	
		// Check whether to overwrite any existing clause data.
		if ($overwrite_existing)
		{
			// If '$key' is an SQL WHERE clause of some kind, then remove all SQL WHERE statements.
			if (! in_array($key, array('select', 'join', 'order_by', 'group_by', 'limit')))
			{
				$this->auth->where = $this->auth->or_where = $this->auth->where_in = array();
				$this->auth->or_where_in = $this->auth->where_not_in = $this->auth->or_where_not_in = array();
				$this->auth->like = $this->auth->or_like = $this->auth->not_like = $this->auth->or_not_like = array();
			}
			// Else, just remove the specific '$key' SQL statement.
			else
			{
				$this->auth->$key = array();
			}
		}
		
		// Key, value and parameter method, used for LIKE and JOIN clauses.
		if (! is_array($key) && $value && $param) 
		{
			array_push($this->auth->$sql_clause, array('key_value_param_method' => array('key' => $key, 'value' => $value, 'param' => $param)));
		}
		// Key and value method.
		else if (! is_array($key) && $value) 
		{
			array_push($this->auth->$sql_clause, array('key_value_method' => array('key' => $key, 'value' => $value)));
		}
		// String or associative array method.
		else 
		{
			array_push($this->auth->$sql_clause, $key);
		}
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	
	/**
	 * set_custom_sql_to_db
	 * Used internally by flexi auth to call any custom user defined SQL Active Record functions at the correct point during a function.
	 *
	 * @return null
	 * @author Rob Hussey
	 */
	public function set_custom_sql_to_db($sql_select = FALSE, $sql_where = FALSE) 
	{
		// Set directly submitted SELECT and WHERE clauses.
		if (!empty($sql_select))
		{
			$this->db->select($sql_select);
		}

		if (!empty($sql_where))
		{
			$this->db->where($sql_where);
		}
	
		### ++++++++++++++++++++ ###
	
		// Set SQL clauses defined via flexi auth SQL Active Record functions.

		// Set array of all SQL clause types.
		$clause_types = array(
			'select', 'where', 'or_where', 'where_in', 'or_where_in', 'where_not_in', 'or_where_not_in', 
			'like', 'or_like', 'not_like', 'or_not_like', 'join', 'order_by', 'group_by', 'limit'
		);
		
		// Loop through clause types.
		foreach($clause_types as $sql_clause)
		{
			// If a clause is set.
			if (! empty($this->auth->$sql_clause))
			{
				// Loop through the clause array setting values using active record.
				foreach($this->auth->$sql_clause as $value)
				{
					// Key, value and parameter method.
					if (is_array($value) && key($value) === 'key_value_param_method') 
					{
						$data = current($value);					
						$this->db->$sql_clause($data['key'], $data['value'], $data['param']);
					}
					// Key and value method.
					else if (is_array($value) && key($value) === 'key_value_method') 
					{
						$data = current($value);
						$this->db->$sql_clause($data['key'], $data['value']);
					}
					// String or Associative array method.
					else 
					{
						$this->db->$sql_clause($value);
					}
				}
			}
		}
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	
	/**
	 * clear_arg_sql
	 * Clears any custom user defined SQL Active Record functions.
	 *
	 * @return null
	 * @author Rob Hussey
	 */
	public function clear_arg_sql() 
	{
		$this->auth->select = $this->auth->join = $this->auth->order_by = $this->auth->group_by = $this->auth->limit = array();
		$this->auth->where = $this->auth->or_where = $this->auth->where_in = array();
		$this->auth->or_where_in = $this->auth->where_not_in = $this->auth->or_where_not_in = array();
		$this->auth->like = $this->auth->or_like = $this->auth->not_like = $this->auth->or_not_like = array();
	}	
	
	/**
	 * database_date_time
	 * Format the current or a submitted date and time (in seconds). 
	 * Additional time can be added / subtracted.
	 *
	 * @return void
	 */
	public function database_date_time($apply_time = 0, $time = FALSE, $force_unix = FALSE)
	{
		// Get timestamp of either submitted time or current time.
		if ($time)
		{
			$time = (is_numeric($time) && strlen($time) == 10) ? $time : strtotime($time);
		}
		else
		{
			$time = time();
		}
		
		// Add or subtract any submitted apply time.
		$time += $apply_time;
		return date('Y-m-d H:i:s', $time);
	}
	

}