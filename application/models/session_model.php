<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Session_Model extends CI_Model
{
	/**
	 * insert_database_login_session
	 * Used in conjunction with $config['validate_login_onload'] set via the config file.
	 * The function inserts a login session token into the database and browser session. 
	 * These two tokens are then compared on every page load to ensure the users session is still valid.
	 *
	 * This method offers more control over login security as you can logout users immediately (By removing their database session or
	 * suspending / deactivating them), rather than having to wait for the users CodeIgniter session to expire.
	 * However, it requires more database calls per page load.
	 *
	 * @return bool
	 * @author Rob Hussey
	 */
	private function insert_database_login_session($user_id)
	{
	    if (!is_numeric($user_id))
	    {
			return FALSE;
	    }
		
		// Generate session token.
		$session_token = sha1($this->generate_token(20));
		
		$sql_insert = array(
			$this->auth->tbl_col_user_session['user_id'] => $user_id,
			$this->auth->tbl_col_user_session['token'] => $session_token,
			$this->auth->tbl_col_user_session['date'] => $this->database_date_time()
		);

		$this->db->insert($this->auth->tbl_user_session, $sql_insert);
		
	    if ($this->db->affected_rows() > 0)
	    {
			// Create session.
			$this->auth->session_data[$this->auth->session_name['login_session_token']] = $session_token;
			$this->session->set_userdata(array($this->auth->session_name['name'] => $this->auth->session_data));

			// Hash database session token as it will be visible via cookie.
			$hash_session_token = $this->hash_cookie_token($session_token);
							
			// Create cookies to detect if user closes their browser (Defined by config file).
			if ($this->auth->auth_security['logout_user_onclose'])
			{
				set_cookie(array(
					'name' => $this->auth->cookie_name['login_session_token'],
					'value' => $hash_session_token,
					'expire' => 0 // Set to 0 so it expires on browser close.
				));
			}
			// Create a cookie to detect when a user has closed their browser since logging in via password (Defined by config file).
			// If the cookie is not set/valid, a users 'logged in via password' status will be unset.
			else if ($this->auth->auth_security['unset_password_status_onclose'])
			{
				set_cookie(array(
					'name' => $this->auth->cookie_name['login_via_password_token'],
					'value' => $hash_session_token,
					'expire' => 0 // Set to 0 so it expires on browser close.
				));
			}
			
			return TRUE;
		}
		return FALSE;
	}
	
	 /** remember_user
	 * Creates a set of 'Remember me' cookies and inserts a database row containing the cookie session data.
	 *
	 * @return bool
	 * @author Rob Hussey
	 * @author Ben Edmunds
	 */
	private function remember_user($user_id)
	{
	    if (!is_numeric($user_id))
	    {
			return FALSE;
	    }

		// Use existing 'Remember me' series token if it exists.
	    if (get_cookie($this->auth->cookie_name['remember_series']))
		{
			$remember_series = get_cookie($this->auth->cookie_name['remember_series']);
		}
		else
		{
			$remember_series = $this->generate_token(40);
		}
		
	    // Set new 'Remember me' unique token.
		$remember_token = $this->generate_token(40);
		
		// Hash the database session tokens with user-agent to help invalidate hijacked cookies used from different browser.
		$sql_insert = array(
			$this->auth->tbl_col_user_session['user_id'] => $user_id,
			$this->auth->tbl_col_user_session['series'] => $this->hash_cookie_token($remember_series),
			$this->auth->tbl_col_user_session['token'] => $this->hash_cookie_token($remember_token),
			$this->auth->tbl_col_user_session['date'] => $this->database_date_time()
		);

		$this->db->insert($this->auth->tbl_user_session, $sql_insert);
		
		###+++++++++++++++++++++++++++++++++###
		
		// Cleanup and remove the now used 'Remember me' database session if they exist.
		if (get_cookie($this->auth->cookie_name['remember_series']) && get_cookie($this->auth->cookie_name['remember_token']))
		{
			$sql_where = array(
				$this->auth->tbl_col_user_session['user_id'] => $user_id,
				$this->auth->tbl_col_user_session['series'] => 
					$this->hash_cookie_token(get_cookie($this->auth->cookie_name['remember_series'])),
				$this->auth->tbl_col_user_session['token'] => 
					$this->hash_cookie_token(get_cookie($this->auth->cookie_name['remember_token']))
			);

			$this->db->delete($this->auth->tbl_user_session, $sql_where);
		}

		###+++++++++++++++++++++++++++++++++###
		
		// Set new 'Remember me' cookies.
	    if ($this->db->affected_rows() > 0)
	    {
			set_cookie(array(
				'name' => $this->auth->cookie_name['user_id'],
				'value' => $user_id,
				'expire' => $this->auth->auth_security['user_cookie_expire'],
			));

			set_cookie(array(
				'name' => $this->auth->cookie_name['remember_series'],
				'value' => $remember_series,
				'expire' => $this->auth->auth_security['user_cookie_expire'],
			));			

			set_cookie(array(
				'name' => $this->auth->cookie_name['remember_token'],
				'value' => $remember_token,
				'expire' => $this->auth->auth_security['user_cookie_expire'],
			));			
			
			return TRUE;
	    }

		// 'Remember me' has been unsuccessful, for security, remove any existing cookies and database sessions.
		$this->delete_database_login_session($user_id);

	    return FALSE;
	}
	
	
	/**
	 * delete_database_login_session
	 * Delete database login sessions and delete 'Remember me' cookies.
	 *
	 * @return bool
	 * @author Rob Hussey
	 */
	public function delete_database_login_session($user_id, $all_sessions = TRUE)
	{
		if (!is_numeric($user_id))
		{
			return FALSE;
		}

		// Get 'Remember me' cookie values before they are deleted.
		$remember_token = get_cookie($this->auth->cookie_name['remember_token']);
		$remember_series = get_cookie($this->auth->cookie_name['remember_series']);

		// Delete 'Remember me' cookies if they exist.
		$this->delete_remember_me_cookies();

		###+++++++++++++++++++++++++++++++++###
		
		// Check whether to delete all sessions for user on all browers they may be logged in on, or just this session.
		if (!$all_sessions && isset($remember_token))
		{				
			// If deleting a login session not associated to a 'Remember me' cookie.
			if (!isset($remember_series))
			{
				$remember_series = '';
			}
					
			$sql_where = '('.
				$this->auth->tbl_col_user_session['user_id'].' = '.$this->db->escape($user_id).' AND '.
				$this->auth->tbl_col_user_session['series'].' = '.$this->db->escape($this->hash_cookie_token($remember_series)).' AND '.
				$this->auth->tbl_col_user_session['token'].' = '.$this->db->escape($this->hash_cookie_token($remember_token)).
			')';
			$this->db->where($sql_where, NULL, FALSE); 
			
			// Delete the login session token if it is set.
			if ($session_token = $this->auth->session_data[$this->auth->session_name['login_session_token']])
			{
				$sql_where = '('.
					$this->auth->tbl_col_user_session['user_id'].' = '.$this->db->escape($user_id).' AND '.
					$this->auth->tbl_col_user_session['token'].' = '.$this->db->escape($session_token).
				')';
				$this->db->or_where($sql_where, NULL, FALSE);
			}
		}
		else
		{
			$this->db->where($this->auth->tbl_col_user_session['user_id'], $user_id);
		}

		// Delete database session records.
		$this->db->delete($this->auth->tbl_user_session);

	    return $this->db->affected_rows() > 0;
	}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###

	/**
	 * validate_database_login_session
	 * Used in conjunction with $config['validate_login_onload'] set via the config file.
	 * Validates a browser login session token with a stored database login token.
	 *
	 * @return bool
	 * @author Rob Hussey
	 */
	public function validate_database_login_session()
	{
		$user_id = $this->auth->session_data[$this->auth->session_name['user_id']];
		$session_token = $this->auth->session_data[$this->auth->session_name['login_session_token']];
		
		$sql_where = array(
			$this->auth->tbl_col_user_account['id'] => $user_id,
			$this->auth->tbl_col_user_account['active'] => 1,
			$this->auth->tbl_col_user_account['suspend'] => 0,
			$this->auth->tbl_col_user_session['token'] => $session_token
		);

		// If a session expire time is defined, check its valid. 
		if ($this->auth->auth_security['login_session_expire'] > 0)
		{
			$sql_where[$this->auth->tbl_col_user_session['date'].' > '] = $this->database_date_time(-$this->auth->auth_security['login_session_expire']);
		}

	    $query = $this->db->from($this->auth->tbl_user_account)
			->join($this->auth->tbl_user_session, $this->auth->tbl_join_user_account.' = '.$this->auth->tbl_join_user_session)
			->where($sql_where)
			->get();
		
		###+++++++++++++++++++++++++++++++++###

	    // User login credentials are valid, continue as normal.
	    if ($query->num_rows() == 1)
	    {
			// Get database session token and hash it to try and match hashed cookie token if required for the 'logout_user_onclose' or 'login_via_password_token' features.
			$session_token = $query->row()->{$this->auth->database_config['user_sess']['columns']['token']};
			$hash_session_token = $this->hash_cookie_token($session_token);

			// Validate if user has closed their browser since login (Defined by config file).
			if ($this->auth->auth_security['logout_user_onclose'])
			{
				if (get_cookie($this->auth->cookie_name['login_session_token']) != $hash_session_token)
				{
					$this->set_error_message('login_session_expired', 'config');
					$this->logout(FALSE);
					return FALSE;
				}
			}
			// Check whether to unset the users 'Logged in via password' status if they closed their browser since login (Defined by config file). 
			else if ($this->auth->auth_security['unset_password_status_onclose'])
			{
				if (get_cookie($this->auth->cookie_name['login_via_password_token']) != $hash_session_token)
				{
					$this->delete_logged_in_via_password_session();
					return FALSE;
				}
			}
		
			// Extend users login time if defined by config file.
			if ($this->auth->auth_security['extend_login_session'])
			{
				// Set extension time.
				$sql_update[$this->auth->tbl_col_user_session['date']] = $this->database_date_time();
				
				$sql_where = array(
					$this->auth->tbl_col_user_session['user_id'] => $user_id,
					$this->auth->tbl_col_user_session['token'] => $session_token
				);
				
				$this->db->update($this->auth->tbl_user_session, $sql_update, $sql_where);
			}
			
			// If loading the 'complete' library, it extends the 'lite' library with additional functions, 
			// however, this would also runs the __construct twice, causing the user to wastefully be verified twice.
			// To counter this, the 'auth_verified' var is set to indicate the user has already been verified for this page load.
			return $this->auth_verified = TRUE;
		}
		// The users login session token has either expired, is invalid (Not found in database), or their account has been deactivated since login.
		// Attempt to log the user in via any defined 'Remember Me' cookies. 
		// If the "Remember me' cookies are valid, the user will have 'logged_in' credentials, but will have no 'logged_in_via_password' credentials.
	 	// If the user cannot be logged in via a 'Remember me' cookie, the user will be stripped of any login session credentials.
		// Note: If the user is also logged in on another computer using the same identity, those sessions are not deleted as they will be authenticated when they next login.
		else
		{
			$this->delete_logged_in_via_password_session();
			return FALSE;
		}
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
		
	/**
	 * delete_logged_in_via_password_session
	 * Attempt to log the user in via any defined 'Remember me' cookies.
	 * If the user cannot be logged in via a 'Remember me' cookie, then remove any login credentials assigned to the users session.
	 *
	 * @return bool
	 * @author Rob Hussey
	 */
	private function delete_logged_in_via_password_session()
	{
		$this->load->model('flexi_auth_model');
		if (! $this->flexi_auth_model->login_remembered_user())
		{
			$this->set_error_message('login_session_expired', 'config');
			$this->session->set_userdata(array($this->auth->session_name['name'] => $this->set_auth_defaults()));
		}
		
		return TRUE;
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
		
	/**
	 * delete_expired_remember_users
	 * Cleanup function to delete all expired 'Remember me' sessions from database.
	 *
	 * @return bool
	 * @author Rob Hussey
	 */
	public function delete_expired_remember_users($expire_time = FALSE)
	{
		if (!$expire_time)
		{
			$expire_time = $this->auth->auth_security['user_cookie_expire'];
		}
		
		// Create expire date.
		$expire_date = $this->database_date_time(-$expire_time);

	    $this->db->delete($this->auth->tbl_user_session, array($this->auth->tbl_col_user_session['date'].' < ' => $expire_date));
		
	    return $this->db->affected_rows() > 0;
	}
	
	/**
	 * delete_remember_me_cookies
	 * Delete any defined 'Remember me' cookies.
	 *
	 * @return bool
	 * @author Rob Hussey
	 */
	public function delete_remember_me_cookies()
	{
		if (get_cookie($this->auth->cookie_name['user_id']))
		{
			delete_cookie($this->auth->cookie_name['user_id']);
		}
		if ($remember_series = get_cookie($this->auth->cookie_name['remember_series']))
		{
			delete_cookie($this->auth->cookie_name['remember_series']);
		}
		if ($remember_token = get_cookie($this->auth->cookie_name['remember_token']))
		{
			delete_cookie($this->auth->cookie_name['remember_token']);
		}

		return TRUE;
	}
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	
	/**
	 * hash_cookie_token
	 * Create a hash of users database session token, users browser details and a static salt.
	 * This can help invalidate hijacked cookies used from a different browser.
	 *
	 * @return bool
	 * @author Rob Hussey
	 */
	public function hash_cookie_token($data)
	{
		if (empty($data))
		{
			return FALSE;
		}
		
		$browser = $this->auth->auth_security['static_salt'].$this->input->server('HTTP_USER_AGENT');
		
		return sha1($data.$browser);
	}
	
}