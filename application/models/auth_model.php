<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once( APPPATH . 'models/user_model' . EXT );

class Auth_Model extends User_Model
{
	public $auth;
	private $primary_identity_col = 'uacc_email';
	private $login_attempt_limit = 5;
	private $login_attempt_time_ban =  10;
	protected $failed_login_attempts_limit_by_ip = 10;
	protected $expire_forgotten_password = 730 ;//30 days
	public function __construct()
	{
		parent::__construct();
		$this->auth = new stdClass;
	}
			
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// LOGIN 
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	
	/**
	 * ip_login_attempts_exceeded
	 * Validates whether the number of failed login attempts from a unique IP address has exceeded a defined limit.
	 * The function can be used in conjunction with showing a Captcha for users repeatedly failing login attempts.
	 *
	 * @return bool
	 * @author fintotal
	 */
	public function ip_login_attempts_exceeded($ip_address)
	{
		// Compare users IP address against any failed login IP addresses.
		$sql_where = array(
			'uacc_fail_login_ip_address' => $ip_address,
			'uacc_fail_login_attempts'.' >= ' => $this->failed_login_attempts_limit_by_ip
		);	
	    $query = $this->db->where($sql_where)
			->get($this->tbl_user_account);
		return $query->num_rows() > 0;
	}
	
	/**
	 * login_attempts_exceeded 
	 * Check whether user has made too many failed login attempts within a set time limit.
	 *
	 * @return bool
	 * @author fintotal
	 */
	private function login_attempts_exceeded($identity)
	{
		if (empty($identity))
		{
			return TRUE;
		}

		$sql_select = array(
			"uacc_fail_login_attempts", 
			"uacc_date_fail_login_ban"
		);
		
	    $query = $this->db->select($sql_select)
			->where($this->primary_identity_col, $identity)
			->get($this->tbl_user_account);

		if ($query->num_rows() == 1)
		{
			$user = $query->row();
			
			$attempts = $user->uacc_fail_login_attempts;
			$failed_login_date = $user->uacc_date_fail_login_ban;
			
			// Check if login attempts are acceptable.
			if ($attempts < $this->login_attempt_limit)
			{
				return FALSE;
			}
			// Login attempts exceed limit, now check if user has waited beyond time ban limit to attempt another login.
			else if ($this->database_date_time($this->login_attempt_time_ban, $failed_login_date, TRUE) 
				< $this->database_date_time(FALSE, FALSE, TRUE))
			{
				return FALSE;
			}
		}
		return TRUE;
	}
	
		/**
	 * reset_login_attempts 
	 * This function is called when a user successfully logs in, it's used to remove any previously logged failed login attempts.
	 * This prevents a user accumulating a login time ban for every failed attempt they make.
	 *
	 * @return bool
	 * @author fintotal
	 */
	private function reset_login_attempts($identity)
	{
		if (empty($identity))
		{
			return FALSE;
		}
		
		$login_data = array(
			'uacc_fail_login_ip_address' => '',
			'uacc_fail_login_attempts' => 0,
			'uacc_date_fail_login_ban' => 0
		);
		
		$this->db->update($this->tbl_user_account, $login_data, array($this->primary_identity_col => $identity));
	    return $this->db->affected_rows() == 1;
	}
	
	/**
	 * increment_login_attempts
	 * This function is called to log details of when a user has failed a login attempt.
	 *
	 * @return bool
	 * @author fintotal
	 */
	private function increment_login_attempts($identity, $attempts)
	{
		if (empty($identity) || !is_numeric($attempts))
		{
			return FALSE;
		}
		
		$attempts++;
		$time_ban = 0;
		
		// Length of time ban in seconds.
		if ($attempts >= $this->login_attempt_limit)
		{
			// Set time ban message.
			$this->set_error_message('login_attempts_exceeded', 'config');
			
			$time_ban = $this->login_attempt_time_ban;
		
			// If failed attempts continue for more than 3 times the limit, increase the time ban by a factor of 2.
			if ($attempts >= ($this->login_attempt_limit * 3))
			{
				$time_ban = ($time_ban * 2);
			}

			// Set time ban as a date.
			$time_ban = $this->database_date_time($time_ban);
		}
		
		// Record users ip address to compare future login attempts.
		$login_data = array(
			'uacc_fail_login_ip_address' => $this->input->ip_address(),
			'uacc_fail_login_attempts' => $attempts,
			'uacc_date_fail_login_ban' => $time_ban
		);
		
		$this->db->update($this->tbl_user_account, $login_data, array($this->primary_identity_col => $identity));
	    return $this->db->affected_rows() == 1;
	}
	
	/**
	 * update_last_login
	 * Updates the main user account table with the last time a user logged in and their IP address. 
	 * The data type of the date can be formatted via the config file.
	 *
	 * @return bool
	 * @author fintotal
	 * //@todo : also insert entry in log history table
	 */
	public function update_last_login($user_id)
	{
		// Update user IP address and last login date.
		$login_data = array(
			'uacc_ip_address' => $this->input->ip_address(),
			'uacc_date_last_login' => $this->database_date_time()
		);
		
		$this->db->update($this->tbl_user_account, $login_data, array('uacc_id' => $user_id));
	    return $this->db->affected_rows() == 1;
	}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// LOGOUT 
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	

	/**
	 * logout
	 * Note: $all_sessions variable allows you to define whether to delete all database session or just the current session.
	 * When set to FALSE, this can be used to logout a user off of one computer (Internet Cafe) but not another (Home).
	 *
	 * @return bool
	 * @author Rob Hussey
	 */
	public function logout($all_sessions = TRUE)
	{
		$user_id = $this->auth->session_data[$this->auth->session_name['user_id']];
				
		// Delete database login sessions and 'Remember me' cookies.
		$this->delete_database_login_session($user_id, $all_sessions);

		// Delete session login data.
		$this->auth->session_data = $this->set_auth_defaults();
		$this->session->unset_userdata($this->auth->session_name['name']);

		// Run database maintenance function to clean up any expired login sessions.
		$this->delete_expired_remember_users();

		return TRUE;
	}
	
	/**
	 * logout_specific_user
	 * Logs a specific user out of all of their logged in sessions.
	 *
	 * @return bool
	 * @author Rob Hussey
	 */
	public function logout_specific_user($user_id = FALSE)
	{
		if (is_numeric($user_id))
		{
			// Delete database login sessions and 'Remember me' cookies.
			$this->delete_database_login_session($user_id, TRUE);
		}
		
		// Run database maintenance function to clean up any expired login sessions.
		$this->delete_expired_remember_users();

		return TRUE;
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###

	/**
	 * set_auth_defaults
	 * Sets the default auth session values
	 *
	 * @return void
	 * @author Rob Hussey
	 */
	public function set_auth_defaults()
	{
		foreach($this->auth->session_name as $session_name => $session_alias)
		{
			if (!in_array($session_name,array('name','math_captcha')))
			{
				$this->auth->session_data[$session_alias] = FALSE;
			}
		}

		$this->session->set_userdata(array($this->auth->session_name['name'] => $this->auth->session_data));
	}
	
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// USER TASK METHODS
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	
	/**
	 * forgotten_password
	 * Inserts a forgotten password token.
	 *
	 * @return bool
	 * @author fintotal
	 */
	public function forgotten_password($identity)
	{
		
	    if (empty($identity))
	    {
			return FALSE;
	    }
	
		// Generate forgotten password token.
		$hash_token = $this->generate_hash_token($this->generate_token());
		
		// Set forgotten password token expiry time if defined by config file.
		if ($this->expire_forgotten_password > 0)
		{
			$expire_time = (60 * $this->expire_forgotten_password); // 60 Secs * expire minutes.
			$this->db->set('uacc_forgotten_password_expire', 
				$this->database_date_time($expire_time));
		}

		$this->db->set('uacc_forgotten_password_token', $hash_token)
			->where('uacc_email', $identity)
			->update($this->tbl_user_account);
	    return $this->db->affected_rows() == 1;
	}
	

	/**
	 * validate_forgotten_password_token
	 * Validates that a submitted Forgotten Password Token matches the database record and has not expired.
	 *
	 * @return bool
	 * @author by fintotal
	 */
	public function validate_forgotten_password_token($user_id, $token)
	{
	    // Confirm token is 40 characters long (length of sha1).
		if (!is_numeric($user_id) || strlen($token) !== 40)
	    {
			return FALSE;
	    }
	
		$sql_where = array(
			'uacc_id' => $user_id,
			'uacc_forgotten_password_token' => $token
		);
		
		// Check Forgotten Password Token hasn't expired, defined via config file.
		if ($this->expire_forgotten_password > 0)
		{
			$sql_where['uacc_forgotten_password_expire > '] = $this->database_date_time();
		}
		
	    $this->db->where($sql_where);
		
		return $this->db->count_all_results($this->tbl_user_account) > 0;
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###
	
	/**
	 * change_forgotten_password
	 * Changes a forgotten password to either a new submitted password, or it generates a new one.
	 *
	 * @return string
	 * @author fintotal
	 */
	public function change_forgotten_password($user_id, $token, $new_password = FALSE, $database_salt = FALSE)
	{
	    // Confirm token is 40 characters long (length of sha1)
		if (!is_numeric($user_id) || strlen($token) !== 40) 
	    {
			return FALSE;
	    }
		
		// If forgotten password token matches and has not expired (expiry set via config)
	    if ($this->validate_forgotten_password_token($user_id, $token))
	    {
			// Delete any user 'Remember me' sessions
			//@todo: session
			//$this->delete_database_login_session($user_id);
		
			// Create new password if not set
			if (!$new_password)
			{
				$new_password = $this->generate_token();
			}
						
			$hashed_new_password = $this->generate_hash_token($new_password, $database_salt, TRUE);
			
			$sql_update = array(
				'uacc_password' => $hashed_new_password,
				'uacc_forgotten_password_token' => '',
				'uacc_forgotten_password_expire' => 0
			);
			
			$sql_where = array(
				'uacc_id' => $user_id,
				'uacc_forgotten_password_token' => $token
			);
			
			$this->db->update($this->tbl_user_account, $sql_update, $sql_where);

			if ($this->db->affected_rows() == 1)
			{
				return $new_password;
			}		
	    }

	    return FALSE;
	}

	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	
	/**
	 * set_update_email_token
	 * Inserts a Update Email Token and stores the new email address pending activation.
	 *
	 * @return bool
	 * @author Rob Hussey
	 */
	public function set_update_email_token($user_id, $new_email)
	{
	    if (empty($user_id) || (empty($new_email)))
	    {
			return FALSE;
	    }
		
		$sql_update = array(
			$this->auth->tbl_col_user_account['update_email_token'] => $this->generate_hash_token($this->generate_token()),
			$this->auth->tbl_col_user_account['update_email'] => $new_email	
		);
		
		$sql_where[$this->auth->tbl_col_user_account['id']] = $user_id;

		$this->db->update($this->auth->tbl_user_account, $sql_update, $sql_where);
	
	    return $this->db->affected_rows() == 1;
	}
	
	/**
	 * verify_updated_email
	 * Verifies a submitted $update_email_token and updates their account with the new email address.
	 *
	 * @return bool
	 * @author Rob Hussey
	 */
	public function verify_updated_email($user_id, $update_email_token)
	{
		if (empty($user_id) || empty($update_email_token))
		{
			$this->flexi_auth_model->set_error_message('update_unsuccessful', 'config');
			return FALSE;
		}
		
		// Get user information.
		$sql_select = array(
			$this->auth->tbl_col_user_account['update_email_token'],
			$this->auth->tbl_col_user_account['update_email']
		);
		
		$sql_where[$this->auth->tbl_col_user_account['id']] = $user_id;
		
		$user = $this->flexi_auth_model->get_users($sql_select, $sql_where)->row();

		if (!is_object($user))
		{
			$this->flexi_auth_model->set_error_message('update_unsuccessful', 'config');
			return FALSE;
		}
		
		$database_email_token = $user->{$this->auth->database_config['user_acc']['columns']['update_email_token']};
		$new_email = $user->{$this->auth->database_config['user_acc']['columns']['update_email']};
		
		// Check if token in database matches the submitted token.
		if ($database_email_token == $update_email_token)
		{
			$sql_update = array(
				$this->auth->tbl_col_user_account['email'] => $new_email,
				$this->auth->tbl_col_user_account['update_email_token'] => '',
				$this->auth->tbl_col_user_account['update_email'] => ''
			);
			
			$this->db->update($this->auth->tbl_user_account, $sql_update, $sql_where);

			if ($this->db->affected_rows() == 1)
			{
				// Update users current session identifier if the primary identity column is the users email.
				if ($this->auth->primary_identity_col == $this->auth->tbl_col_user_account['email'])
				{
					$this->flexi_auth_model->update_session_identifier($user_id, $new_email);
				}
			}
			
			return TRUE;
		}
		
		return FALSE;
	}
	
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	// LOGIN / VALIDATION METHODS
	###++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++###	
	/**
	 * login
	 * Verifies a users identity and password,
	 *
	 * @return bool
	 * @author fintotal
	 */
	public function login($identity = FALSE, $password = FALSE)
	{	
		if (empty($identity) || empty($password))
	    {
			return FALSE;
	    }
		
		// Check if login attempts are being counted.
		if ($this->login_attempt_limit > 0)
		{
			// Check user has not exceeded login attempts.
			if ($this->login_attempts_exceeded($identity))
			{
				$this->set_error_message('login_attempts_exceeded', 'config');
				return FALSE;
			}
		}
		
		$user = $this->get_user_by_email($identity);
		
		###+++++++++++++++++++++++++++++++++###
	    // User exists, now validate credentials.
		if (is_object($user))
	    {	
			// Check whether account has been activated.
			if ($user->uacc_active == 0)
			{
				$this->set_error_message('account_requires_activation', 'config');
				return FALSE;
			}
			
			// Check if account has been suspended.
			if ($user->uacc_suspend == 1)
			{
				$this->set_error_message('account_suspended', 'config');
				return FALSE;
			}
			
			// Verify submitted password matches database.
			if ($this->verify_password($identity, $password))
			{
				// Reset failed login attempts.
				if ($user->uacc_fail_login_attempts > 0)
				{
					$this->reset_login_attempts($identity);
				}
				$this->update_last_login($user->uacc_id);
				return $user;
			}
			// Password does not match, log the failed login attempt if defined via the config file.
			else if ($this->login_attempt_limit > 0)
			{				
				$attempts = $user->uacc_fail_login_attempts;
				// Increment failed login attempts.
				$this->increment_login_attempts($identity, $attempts);
			}
	    }
	    return FALSE;
	}
	
	/**
	 * login_remembered_user
	 *
	 * @return bool
	 * @author Rob Hussey
	 * @author Ben Edmunds
	 */
	public function login_remembered_user()
	{
	    if (!get_cookie($this->auth->cookie_name['user_id']) || !get_cookie($this->auth->cookie_name['remember_series']) || 
			!get_cookie($this->auth->cookie_name['remember_token']))
	    {
		    return FALSE;
	    }

		$user_id = get_cookie($this->auth->cookie_name['user_id']);
		$remember_series = get_cookie($this->auth->cookie_name['remember_series']);
		$remember_token = get_cookie($this->auth->cookie_name['remember_token']);
		
		$sql_select = array(
			$this->auth->primary_identity_col,
			$this->auth->tbl_col_user_account['id'], 
			$this->auth->tbl_col_user_account['group_id'], 
			$this->auth->tbl_col_user_account['activation_token'], 
			$this->auth->tbl_col_user_account['last_login_date']
		);

		// Database session tokens are hashed with user-agent to 'help' invalidate hijacked cookies used from different browser.
		$sql_where = array(
			$this->auth->tbl_col_user_account['id'] => $user_id,
			$this->auth->tbl_col_user_account['active'] => 1,
			$this->auth->tbl_col_user_account['suspend'] => 0,
			$this->auth->tbl_col_user_session['series'] => $this->hash_cookie_token($remember_series),
			$this->auth->tbl_col_user_session['token'] => $this->hash_cookie_token($remember_token),
			$this->auth->tbl_col_user_session['date'].' > ' => $this->database_date_time(-$this->auth->auth_security['user_cookie_expire'])
		);

		$query = $this->db->select($sql_select)
			->from($this->auth->tbl_user_account)
			->join($this->auth->tbl_user_session, $this->auth->tbl_join_user_account.' = '.$this->auth->tbl_join_user_session)
			->where($sql_where)
			->get();
			
		###+++++++++++++++++++++++++++++++++###

	    // If user exists.
	    if ($query->num_rows() == 1)
	    {
			$user = $query->row();
		
			// If an activation time limit is defined by config file and account hasn't been activated by email.
			if ($this->auth->auth_settings['account_activation_time_limit'] > 0 && 
				!empty($user->{$this->auth->database_config['user_acc']['columns']['activation_token']}))
			{
				if (!$this->validate_activation_time_limit($user->{$this->auth->database_config['user_acc']['columns']['last_login_date']}))
				{
					$this->set_error_message('account_requires_activation', 'config');
					return FALSE;
				}
			}

			// Set user login sessions.
			if ($this->set_login_sessions($user))
			{
				// Extend 'Remember me' if defined by config file.
				if ($this->auth->auth_security['extend_cookies_on_login'])
				{
					$this->remember_user($user->{$this->auth->database_config['user_acc']['columns']['id']});
				}
				return TRUE;
			}
	    }
		
		// 'Remember me' has been unsuccessful, for security, remove any existing cookies and database sessions.
		$this->delete_database_login_session($user_id);

	    return FALSE;
	}

	
	
	/**
	 * update_session_identifier
	 * Updates a users current session identifier if they update the database record of their identity (i.e. Change their email).
	 *
	 * @return bool
	 * @author Rob Hussey
	 */
	public function update_session_identifier($user_id, $identity)
	{
		if ($user_id == $this->auth->session_data[$this->auth->session_name['user_id']])
		{
			$this->auth->session_data[$this->auth->session_name['user_identifier']] = $identity;
			$this->session->set_userdata(array($this->auth->session_name['name'] => $this->auth->session_data));
			
			return TRUE;
		}
		return FALSE;
	}
	/**
	 * validate_activation_time_limit 
	 * Validate whether a non-activatated account is within the activation time limit set via config.
	 *
	 * @return bool
	 * @author Rob Hussey
	 */
	private function validate_activation_time_limit($last_login)
	{
		if (empty($last_login))
		{
			return FALSE;
		}
	
		// Set account activation expiry time.
		$expire_time = (60 * $this->auth->auth_settings['account_activation_time_limit']); // 60 Secs * expire minutes.		
		
		// Convert if using MySQL time.
		if (strtotime($last_login)) 
		{
			$last_login = strtotime($last_login);
		}
		
		// Account activation time has expired, user must now activate account via email.
		if (($last_login + $expire_time) < time())
		{
			return FALSE;
		}
		
		return TRUE;
	}
	
	
	
	
	
	/**
	 * recaptcha
	 * Generates the html for Google reCAPTCHA.
	 * Note: If the reCAPTCHA is located on an SSL secured page (https), set $ssl = TRUE.
	 *
	 * @return string
	 * @author Rob Hussey
	 */
	public function recaptcha($ssl = FALSE)
	{
		$this->load->helper('recaptcha');

		// Get config settings.
		$captcha_theme = $this->auth->auth_security['recaptcha_theme'];
		$captcha_lang = $this->auth->auth_security['recaptcha_language'];
		
		// Set defaults.
		$theme = "theme:'clean',";
		$language = "lang:'en'";
		
		// Set reCAPTCHA theme.
		if (!empty($captcha_theme))
		{
			if ($captcha_theme == 'custom')
			{
				$theme = "theme:'custom', custom_theme_widget:'recaptcha_widget',";
			}
			else
			{
				$theme = "theme:'".$captcha_theme."',";
			}
		}
		
		// Set reCAPTCHA language.
		if (!empty($captcha_lang))
		{
			$language = "lang:'".$captcha_lang."'";
		}
		
		$theme_html = "<script>var RecaptchaOptions = { $theme $language };</script>\n";
		$captcha_html = recaptcha_get_html($this->auth->auth_security['recaptcha_public_key'], NULL, $ssl);

		return $theme_html.$captcha_html;
	}
	
	/**
	 * validate_recaptcha
	 * Validates if a Google reCAPTCHA answer submitted via http POST data is correct.
	 *
	 * @return bool
	 * @author Rob Hussey
	 */
	public function validate_recaptcha()
	{
		$this->load->helper('recaptcha');

		$response = recaptcha_check_answer(
			$this->auth->auth_security['recaptcha_private_key'],
			$this->input->ip_address(),
			$this->input->post('recaptcha_challenge_field'),
			$this->input->post('recaptcha_response_field')
		);

		return $response->is_valid;
	}
	
	/**
	 * math_captcha
	 * Basic captcha reliant on maths questions rather than text images.
	 *
	 * @return array
	 * @author Rob Hussey
	 */
	public function math_captcha()
	{
		$min_operand_val = 1;
		$max_operand_val = 20;
		$total_operands = 2;
		$operators = array('+'=>' + ', '-'=>' - ');
		$equation = '';
		
        for ($i = 1; $total_operands >= $i; $i++) 
		{
			$operand = rand($min_operand_val, $max_operand_val);
			$operator = ($i < $total_operands) ? array_rand($operators) : '';			
			$equation .= $operand.$operator;
		}

		// Convert equation symbols to written symbols.
		$captcha['equation'] = str_replace(array_keys($operators), array_values($operators), $equation);
		
		// Convert equation string.
		eval("\$captcha['answer'] = ".$equation.";");
	
		return $captcha;
	}	
	
	
	
	
}

/* End of file flexi_auth_lite_model.php */
/* Location: ./application/controllers/flexi_auth_lite_model.php */
