<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once( APPPATH . 'models/base_model' . EXT );
include_once( APPPATH . 'models/entity/user_group_entity' . EXT );
include_once( APPPATH . 'models/entity/privilege_entity' . EXT );
/**
 * 
 * Enter description here ...
 * @author fintotal
 *
 */
class Group_Privilage_Model extends Base_Model
{
	private $tbl_user_group = 'user_groups';
	private $tbl_user_privilege = 'user_privileges'	;
	private $tbl_user_privilege_users = 'user_privilege_users';
	private $tbl_user_privilege_groups = 'user_privilege_groups';
	
	/**
	 * insert_group
	 * Inserts a new user group to the database. If the group has admin privileges this can be 
	 * set using $is_admin = TRUE.
	 * @return bool
	 */
	public function insert_group($name, $description = NULL, $is_admin = FALSE, $custom_data = array())
  	{
		if (empty($name))
		{
			return FALSE;
		}
		
		// Set any custom data that may have been submitted.
		$sql_insert = (is_array($custom_data)) ? $custom_data : array();
		
		// Set standard group data.
		$sql_insert['ugrp_name'] = $name;
		$sql_insert['ugrp_desc'] = $description;
		$sql_insert['ugrp_admin'] = (int)$is_admin;

		$this->db->insert($this->tbl_user_group, $sql_insert);
		
		return ($this->db->affected_rows() == 1) ? $this->db->insert_id() : FALSE;
	}

	/**
	 * update_group
	 * Updates a user group with any submitted data.
	 *
	 * @return bool
	 * @author fintotal
	 */
	public function update_group($group_id, $group_data)
  	{
		if (!is_numeric($group_id) || empty($group_data))
		{
			return FALSE;
		}
		$this->db->set("ugrp_name",$group_data->ugrp_name);
		$this->db->set("ugrp_desc",$group_data->ugrp_desc);
		$this->db->set("ugrp_admin",$group_data->ugrp_admin);
		$this->db->where("ugrp_id",$group_id);
		$this->db->update($this->tbl_user_group);
		return $this->db->affected_rows() == 1;	
	}


	/**
	 * delete_group
	 * Deletes a group from the user group table.
	 *
	 * @return bool
	 * @author fintotal
	 */
	public function delete_group($group_id)
  	{
		if (is_numeric($group_id))
			$this->db->where("ugrp_id",$group_id);
		
		if(is_array($group_id))
			$this->db->where_in("ugrp_id",$group_id);

		$this->db->delete($this->tbl_user_group);
		return $this->db->affected_rows();	
	}

		
	/**
	 * insert_privilege
	 * Inserts a new user privilege to the database.
	 *
	 * @return bool
	 * @author fintotal
	 */
	public function insert_privilege($name, $description = NULL, $custom_data = array())
  	{
		if (empty($name))
		{
			return FALSE;
		}
		
		// Set any custom data that may have been submitted.
		$sql_insert = (is_array($custom_data)) ? $custom_data : array();
		
		// Set standard privilege data.
		$sql_insert['upriv_name'] = $name;
		$sql_insert['upriv_desc'] = $description;

		$this->db->insert($this->tbl_user_privilege, $sql_insert);
		return ($this->db->affected_rows() == 1) ? $this->db->insert_id() : FALSE;
	}

	/**
	 * update_privilege
	 * Updates a user privilege with any submitted data.
	 *
	 * @return bool
	 * @author fintotal
	 */
	public function update_privilege($privilege_id, $privilege_data)
  	{
		if (!is_numeric($privilege_id) || empty($privilege_data))
		{
			return FALSE;
		}
		$this->db->set('upriv_name',$privilege_data->upriv_name);
		$this->db->set('upriv_desc',$privilege_data->upriv_desc);
		$this->db->where("upriv_id",$privilege_id);
		$this->db->update($this->tbl_user_privilege);
		
		return $this->db->affected_rows() == 1;	
	}


	/**
	 * delete_privilege
	 * Deletes a privilege from the user privilege table.
	 *
	 * @return bool
	 * @author fintotal
	 */
	public function delete_privilege($privilege_ids)
  	{
		if (is_numeric($privilege_ids))
			$this->db->where('upriv_id',$privilege_ids);
		if(is_array($privilege_ids))
			$this->db->where_in('upriv_id',$privilege_ids);
			
			// Delete privileges.
			$this->db->delete($this->tbl_user_privilege);
			
			// Loop through deleted privilege ids and then deleted related user privileges.
			$this->db->where_in('upriv_users_id',$privilege_ids);
			$this->db->delete($this->tbl_user_privilege_users);
			return TRUE;	
	}

	/**
	 * insert_privilege_user
	 * Inserts a new user privilege user to the database.
	 *
	 * @return bool
	 * @author fintotal
	 */
	public function insert_privilege_user($user_id, $privilege_id)
  	{
		if (!is_numeric($user_id) || !is_numeric($privilege_id))
		{
			return FALSE;
		}
		
		// Set standard privilege data.
		$sql_insert = array(
			$this->auth->tbl_col_user_privilege_users['user_id'] => $user_id,
			$this->auth->tbl_col_user_privilege_users['privilege_id'] => $privilege_id
		);

		$this->db->insert($this->auth->tbl_user_privilege_users, $sql_insert);
		
		return ($this->db->affected_rows() == 1) ? $this->db->insert_id() : FALSE;
	}
        
        
	/**
	 * delete_privilege_user
	 * Deletes a privilege user from the privilege user table.
	 *
	 * @return bool
	 */
	public function delete_privilege_user($sql_where)
  	{
		if (is_numeric($sql_where))
		{
			$sql_where = array($this->auth->tbl_col_user_privilege_users['id'] => $sql_where);
		}
		
		$this->db->delete($this->auth->tbl_user_privilege_users, $sql_where);

		return $this->db->affected_rows() == 1;	
	}
        
        
	/**
	 * insert_user_group_privilege
	 * Inserts a new user group privilege to the database.
	 *
	 * @return void
	 */
	public function insert_user_group_privilege($group_id, $privilege_id)
  	{
		if (!is_numeric($group_id) || !is_numeric($privilege_id))
		{
			return FALSE;
		}
		
		// Set standard privilege data.
		$sql_insert = array(
			'upriv_groups_ugrp_fk' => $group_id,
			'upriv_groups_upriv_fk' => $privilege_id
		);
		$this->db->insert($this->tbl_user_privilege_groups, $sql_insert);
		return ($this->db->affected_rows() == 1) ? $this->db->insert_id() : FALSE;
	}

	/**
	 * delete_user_group_privilege
	 * Deletes a user group privilege from the user privilege group table.
	 *
	 * @return bool
	 */
	public function delete_user_group_privilege($group_id, $privilege_id)
  	{
		if (is_numeric($group_id))
		{
			$this->db->where('upriv_groups_ugrp_fk',$group_id);
			$this->db->where('upriv_groups_upriv_fk',$privilege_id);
			$this->db->delete($this->tbl_user_privilege_groups);
			return $this->db->affected_rows() == 1;
		}
		return false;	
	}
	
	/**
	 * get_groups
	 * Returns a list of user groups matching the $sql_where condition.
	 *
	 * @return object
	 */
	public function get_groups($sql_select = FALSE, $sql_where = FALSE)
  	{
	    $query =  $this->db->get($this->tbl_user_group);
	    return (object)$query->result();
  	}
	
	/**
	 * get_groups
	 * Returns a list of user groups matching the $sql_where condition.
	 *
	 * @return object
	 */
	public function get_groups_list_array($admin_groups= false)
  	{
  		$this->db->select('ugrp_id');
  		$this->db->select('ugrp_name');
  		if($admin_groups)
	  		$this->db->where('ugrp_admin',1);
	    $query =  $this->db->get($this->tbl_user_group);
	    $list_item  =  $query->result();
	    $data =  array(""=>"Select Group");
	    foreach ($list_item as $item)
	    {
	    	$data[$item->ugrp_id] = $item->ugrp_name;
	    }
	    return $data;
  	}
	/**
	 * get_group
	 * Returns a user groups matching the group_id
	 *
	 * @return object
	 */
	public function get_group($group_id)
  	{
	    $query =  $this->db->get_where($this->tbl_user_group,array("ugrp_id"=>$group_id));
	    return $this->objectToObject($query->result(),'User_Group_Entity');
  	}

	/**
	 * get_privileges
	 * Returns a list of all privileges matching the $sql_where condition.
	 */
	public function get_privileges()
	{
		// Set any custom defined SQL statements.
		$query =  $this->db->get($this->tbl_user_privilege);  
		return (object)$query->result();     
	}
	
	/**
	 * get_privileges
	 * Returns a list of all privileges matching the $sql_where condition.
	 */
	public function get_privilege($privilege_id)
	{
		// Set any custom defined SQL statements.
		$query =  $this->db->get_where($this->tbl_user_privilege,array("upriv_id"=>$privilege_id));  
		return $this->objectToObject($query->result(),'Privilege_Entity');     
	}
	/**
	 * get_user_privileges
	 * Returns a list of user privileges matching the $sql_where condition.
	 */
	public function get_user_privileges($sql_select, $sql_where)
	{
		// Set any custom defined SQL statements.
		$this->flexi_auth_lite_model->set_custom_sql_to_db($sql_select, $sql_where);
		
		return $this->db->from($this->auth->tbl_user_privilege)
			->join($this->auth->tbl_user_privilege_users, $this->auth->tbl_col_user_privilege['id'].' = '.$this->auth->tbl_col_user_privilege_users['privilege_id'])
			->get();
	}
       
	/**
	 * get_user_group_privileges
	 * Returns a list of user group privileges matching the $sql_where condition.
	 *
	 */
	public function get_user_group_privileges($group_id)
	{
		// Set any custom defined SQL statements.
		$this->db->join($this->tbl_user_privilege_groups, 'upriv_groups_upriv_fk'.' = '.'upriv_id');
		$this->db->where('upriv_groups_ugrp_fk'.' = '.$group_id);
		$query =  $this->db->get($this->tbl_user_privilege);  
		return (object)$query->result(); 
	}

	
}