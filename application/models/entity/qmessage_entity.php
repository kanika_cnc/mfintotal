<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 * Enter description here ...
 * @author Fintotal
 *
 */
class QMessage_Enity
{
	Public $qm_id = null;
  	Public $qm_type_id= 1; // This is for future use, not sure of the use as of now.
  	Public $qm_status_id = null;
  	Public $qm_description='';
  	Public $qm_has_attachment=false;
  	Public $qm_uacct_id=null;
  	public $qm_uacct_name = "";
  	Public $qm_sm_uacct_id=null;
  	public $qm_sm_uacct_name = "";
  	Public $qm_created_by_id=null;
  	public $qm_created_by_name = "";
  	Public $qm_approved_by_id=null;
  	public $qm_approved_by_name = "";
  	public $qm_modified_date = null;
  	public $qm_modified_id = "";
  	Public $qm_created_date=null;
  	Public $qm_approved_date=null;
  	Public $qm_is_transferred=false;
  	public $qm_is_archived = false;
  	public $qm_attributes = array();
  	public $qm_attached_files = array();
  	public $qm_related_articles = array();
  	
  	public $qm_sm='';
}

class QRelatedArticle
{
	Public $qa_id=null;
  	Public $qa_qm_id=null;
  	Public $qa_article_id=0;
  	Public $qa_article= '';
  	
	
  	public function to_object($instance)
  	{
  		$temp =  new QRelatedArticle();
  		$temp_details= new Mfin_Attributes_Entity();
  		$temp->qa_id=$instance->qa_id;
  		$temp->qa_qm_id=$instance->qa_qm_id;
  		$temp->qa_article_id=$instance->qa_article_id;
  		$temp_details->mar_id = $instance->mar_id;
  		$temp_details->mar_url =$instance->mar_url;
  		$temp_details->mar_title = $instance->mar_title	;
  		$temp_details->mar_text = $instance->mar_text;
  		$temp->qa_article =  $temp_details;
  		return $temp;
  	}
  	
}

class QAttributes
{
	Public $qa_id=null;
  	Public $qa_qm_id=null;
  	Public $qa_attr_id=null;
  	Public $qa_attr_value='';
  	public $qa_attr ='';
  	
  	public function to_object($instance)
  	{
  		$temp =  new QAttributes();
  		$temp_details= new Mfin_Attributes_Entity();
  		$temp->qa_id=$instance->qa_id;
  		$temp->qa_qm_id=$instance->qa_qm_id;
  		$temp->qa_attr_id=$instance->qa_attr_id;
  		$temp->qa_attr_value=$instance->qa_attr_value;
  		$temp_details->mfa_id = $instance->mfa_id;
  		$temp_details->mfa_module_id =$instance->mfa_module_id;
  		$temp_details->mfa_name = $instance->mfa_name;
  		$temp_details->mfa_type = $instance->mfa_type;
  		$temp_details->mfa_control_type = $instance->mfa_control_type;
  		$temp_details->mfa_list_type_id = $instance->mfa_list_type_id;
  		$temp_details->mfa_stored_table_name = $instance->mfa_stored_table_name;
  		$temp_details->mfa_stored_table_field = $instance->mfa_stored_table_field;
  		$temp->qa_attr =  $temp_details;
  		return $temp;
  	}
}

class QArchival
{
	public $qa_id =null;
  	public $qa_title = '';
  	public $qa_qm_id='';
  	public $qa_category_id='';
  	public $qa_archived_by='';
  	public $qa_archived_date='';
  	public $qa_message ;
  	
	public function __construct()
	{
		$this->qa_message = new QMessage_Enity();
	}
}
class QStatus
{
	const OPEN = 1;
	const ANSWERED = 5;
	const APPROVED = 2;
	const IGNORED = 6;
	
}