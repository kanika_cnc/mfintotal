<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * Enter description here ...
 * @author fintotal
 *
 */
class Mfin_Attributes_Entity
{
	public $mfa_id = null;
  	public $mfa_module_id =0;
  	public $mfa_name = null;
  	public $mfa_type = null;
  	public $mfa_control_type = null;
  	public $mfa_list_type_id = null;
  	public $mfa_stored_table_name = null;
  	public $mfa_stored_table_field = null;
}