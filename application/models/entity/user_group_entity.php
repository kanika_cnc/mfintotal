<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_Group_Entity
{
	public $ugrp_id;
	public $ugrp_name;
	public $ugrp_desc;
	public $ugrp_admin;
}